<?php

namespace App;

use App\Paciente;
use Illuminate\Database\Eloquent\Model;
use \Carbon\Carbon;

class Cita extends Model
{

    /**
     * Cita belongs to Paciente.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function paciente_r()
    {
        // belongsTo(RelatedModel, foreignKey = paciente_id, keyOnRelatedModel = id)
        return $this->belongsTo(Paciente::class, 'paciente_id', 'id');
    }

    public function getStatusNombreAttribute()
    {
        switch ($this->estatus) {
            case '0':
                return 'Pendiente';
                break;
            case '1':
                return 'En Proceso';
                break;
            case '2':
                return 'Cancelada';
                break;
            case '3':
                return 'Finalizada';
                break;
            case '4':
                return 'Reprogramada';
                break;

            default:
                return "N/A";
                break;
        }
    }

    public function getCreadoAttribute()
    {
        return Carbon::parse($this->created_at)->diffForHumans();
    }

    public function getCitaAttribute()
    {
        return Carbon::parse($this->fecha)->isoFormat('LL');
    }

}
