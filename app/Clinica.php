<?php

namespace App;

use App\Dentista;
use Illuminate\Database\Eloquent\Model;

class Clinica extends Model
{
    protected $table = 'clinicas';

    public function dentista()
    {
        return $this->belongsTo(Dentista::class, 'dentista_id', 'id');
    }

    public function codigop()
    {
        return $this->hasOne(CodigoPostal::class, 'id', 'codigo_postal_id');
    }
}
