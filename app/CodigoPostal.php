<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CodigoPostal extends Model
{
    protected $table = 'codigos_postales';

    public function getDireccionAttribute()
    {
        return 'Col. '.$this->id_asenta . ', C.P. ' . $this->id_codigo. ', '.$this->id_municipio. ', '.$this->id_estado;
    }
}
