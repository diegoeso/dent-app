<?php

namespace App;

use App\Clinica;
use App\Paciente;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Dentista extends Model
{
    /**
     * Fields that can be mass assigned.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'cedula',
        'escuela',
        'estatus',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function clinicas()
    {
        return $this->hasMany(Clinica::class);
    }

    public function paciente()
    {
        return $this->hasMany(Paciente::class);
    }
}
