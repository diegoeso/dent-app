<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\PAciente;
use App\User;
use Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
     */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = RouteServiceProvider::HOME;
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {

        $user = User::where('email', $request->email)->first();
        if (!$user) {
            $user = Paciente::where('email', $request->email)->first();
            if ($user) {
                $gard  = 'pacient';
                $route = 'home_paciente';
            } else {
                return redirect()->back()->with(['estatus' => 'Usuario no valido!'])->withInput($request->only('email', 'remember'));
            }
        } else if ($user) {
            $gard  = 'web';
            $route = 'home';
        } else {
            return redirect()->back()->with(['estatus' => 'Usuario no valido!'])->withInput($request->only('email', 'remember'));
        }

        // Inicio de sesion con el usuario
        if (Auth::guard($gard)->attempt(['email' => $request->email, 'password' => $request->password])) {
            return redirect()->route($route);
        }

        return redirect()->back()->with(['estatus' => 'Usuario no valido!'])->withInput($request->only('email', 'remember'));
        // return redirect()->back()->withInput($request->only('email', 'remember'));
    }
}
