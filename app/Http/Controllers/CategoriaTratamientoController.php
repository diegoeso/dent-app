<?php

namespace App\Http\Controllers;

use App\CategoriaTratamiento;
use Illuminate\Http\Request;

class CategoriaTratamientoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CategoriaTratamiento  $categoriaTratamiento
     * @return \Illuminate\Http\Response
     */
    public function show(CategoriaTratamiento $categoriaTratamiento)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CategoriaTratamiento  $categoriaTratamiento
     * @return \Illuminate\Http\Response
     */
    public function edit(CategoriaTratamiento $categoriaTratamiento)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CategoriaTratamiento  $categoriaTratamiento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CategoriaTratamiento $categoriaTratamiento)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CategoriaTratamiento  $categoriaTratamiento
     * @return \Illuminate\Http\Response
     */
    public function destroy(CategoriaTratamiento $categoriaTratamiento)
    {
        //
    }
}
