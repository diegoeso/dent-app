<?php

namespace App\Http\Controllers;

use App\Cita;
use App\Paciente;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CitaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('citas.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if (Auth::user()->rol == 2) {
            $dentista = Auth::user()->dentista->id;
        } else if (Auth::user()->rol == 3) {
            $dentista = Auth::user()->dentista_id;
        }

        $cita               = new Cita;
        $cita->paciente_id  = ($request->nuevo) ? null : $request->paciente_id;
        $cita->paciente     = ($request->nuevo) ? $request->paciente_new : Paciente::findOrFail($request->paciente_id)->fullName;
        $cita->fecha        = $request->fecha_inicio;
        $cita->hora_inicio  = $request->hora_inicio;
        $cita->hora_fin     = $request->hora_fin;
        $cita->status       = $request->status;
        $cita->comentarios  = $request->comentarios;
        $cita->dentista_id  = $dentista;
        $cita->registrado   = Auth::user()->id;
        $cita->con_registro = ($request->paciente_id != null) ? 1 : 0;
        $cita->aprobada     = 1;
        if ($cita->save()) {
            return response()->json(['success' => true, 'data' => $cita]);
        }
        return response()->json(['success' => false]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cita  $cita
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Cita::with('paciente_r')->find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cita  $cita
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if (Auth::user()->rol == 2) {
            $dentista = Auth::user()->dentista->id;
        } else if (Auth::user()->rol == 3) {
            $dentista = Auth::user()->dentista_id;
        }

        $cita               = Cita::find($id);
        $cita->paciente_id  = ($request->nuevo) ? null : $request->paciente_id;
        $cita->paciente     = ($request->nuevo) ? $request->paciente_new : Paciente::findOrFail($request->paciente_id)->fullName;
        $cita->fecha        = $request->fecha_inicio;
        $cita->hora_inicio  = $request->hora_inicio;
        $cita->hora_fin     = $request->hora_fin;
        $cita->status       = $request->status;
        $cita->comentarios  = $request->comentarios;
        $cita->dentista_id  = $dentista;
        $cita->registrado   = Auth::user()->id;
        $cita->con_registro = ($request->paciente_id != null) ? 1 : 0;
        if ($cita->save()) {
            return response()->json(['success' => true, 'data' => $cita]);
        }
        return response()->json(['success' => false]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cita  $cita
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cita = Cita::find($id);
        if ($request->fecha) {
            $cita->fecha = $request->fecha;
        }
        if ($request->hora_inicio) {
            $cita->hora_inicio = $request->hora_inicio;
        }
        $cita->aprobada = 1;
        if ($cita->save()) {
            return response()->json(['success' => true, 'cita' => $cita]);
        }
        return response()->json(['success' => false]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cita  $cita
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cita $cita)
    {
        //
    }

    public function get_pacientes()
    {

        if (Auth::user()->rol == 2) {
            // dd(Auth::user()->dentista);
            $pacientes = Paciente::where('dentista_id', Auth::user()->dentista->id)->get();
        } else if (Auth::user()->rol == 3) {
            // dd(Auth::user()->dentista_asistente);
            $pacientes = Paciente::where('dentista_id', Auth::user()->dentista_id)->get();
        }

        return response()->json($pacientes);
    }

    public function listar_citas($dentista = null)
    {
        $rol = Auth::user()->rol;
        if ($rol == 2) {
            $dentista = Auth::user()->dentista->id;
        } else if ($rol == 3) {
            $dentista = Auth::user()->dentista_id;
        }

        $now             = Carbon::now();
        $fechaActual     = $now->format('Y-m-d');
        $calendar        = Cita::where('dentista_id', $dentista)->get();
        $data            = [];
        $backgroundColor = '';
        $borderColor     = '';
        foreach ($calendar as $event) {
            // colores para cada estado de la solicitud
            switch ($event->status) {
                case '1':
                    $backgroundColor = '#00a65a';
                    $borderColor     = '#00a65a';
                    break;
                case '1':
                    $backgroundColor = '#f39c12';
                    $borderColor     = '#f39c12';
                    break;
                case '3':
                    $backgroundColor = '#f56954';
                    $borderColor     = '#f56954';
                    break;
                case '4':
                    $backgroundColor = '#d2d6de';
                    $borderColor     = '#d2d6de';
                    break;
                default:
                    $backgroundColor = '#00c0ef';
                    $borderColor     = '#00c0ef';
                    break;
            }
            $subArr = [
                'id'              => $event->id,
                'title'           => $event->paciente,
                'start'           => $event->fecha . ' ' . $event->hora_inicio,
                'end'             => $event->fecha . ' ' . $event->hora_fin,
                'backgroundColor' => $backgroundColor,
                'borderColor'     => $borderColor,
            ];
            array_push($data, $subArr);
        }

        return $data;

    }

    public function show_cita($id)
    {
        $cita = Cita::findOrFail($id);
    }

}
