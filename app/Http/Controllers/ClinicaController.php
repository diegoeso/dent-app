<?php

namespace App\Http\Controllers;

use App\Clinica;
use App\Http\Requests\StoreClinicaRequest;
use Auth;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class ClinicaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('can:clinica.index')->only('index');
        $this->middleware('can:clinica.show')->only('show');
        $this->middleware('can:clinica.create')->only(['create', 'store']);
        $this->middleware('can:clinica.edit')->only(['edit', 'update']);
        $this->middleware('can:clinica.destroy')->only('destroy');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('clinicas.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreClinicaRequest $request)
    {
        $clinica                   = new Clinica;
        $clinica->direccion        = $request->direccion;
        $clinica->nombre           = $request->nombre;
        $clinica->codigo_postal_id = $request->codigo_postal_id;
        $clinica->telefono         = $request->telefono;
        $clinica->telefono_alt     = $request->telefono_alt;
        if (Auth::user()->rol == 1) {
            $clinica->dentista_id = $request->dentista_id;
        } else {
            $clinica->dentista_id = Auth::user()->dentista->id;
        }
        if ($clinica->save()) {
            return response()->json(['success' => true]);
        }
        return response()->json(['success' => false]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Clinica  $clinica
     * @return \Illuminate\Http\Response
     */
    public function show(Clinica $clinica)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Clinica  $clinica
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $clinica = Clinica::with('codigop')->where('id', $id)->first();
        return response()->json(['success' => true, 'res' => $clinica]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Clinica  $clinica
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $clinica                   = Clinica::findOrFail($id);
        $clinica->nombre           = $request->nombre;
        $clinica->direccion        = $request->direccion;
        $clinica->codigo_postal_id = $request->codigo_postal_id;
        $clinica->telefono         = $request->telefono;
        $clinica->telefono_alt     = $request->telefono_alt;
        if (Auth::user()->rol == 1) {
            $clinica->dentista_id = $request->dentista_id;
        } else {
            $clinica->dentista_id = Auth::user()->dentista->id;
        }
        if ($clinica->save()) {
            return response()->json(['success' => true]);
        }
        return response()->json(['success' => false]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Clinica  $clinica
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $clinica          = Clinica::findOrFail($id);
        $clinica->deleted = 1;
        if ($clinica->save()) {
            return response()->json(['success' => 'true']);
        } else {
            return response()->json(['success' => 'false']);
        }
    }

    public function listar_clinicas()
    {

        if (Auth::user()->rol == 2) {
            $clinicas = Clinica::whereHas('dentista', function ($q) {
                $q->where('user_id', Auth::user()->id);
            })->get();
        } else {

            $clinicas = Clinica::where('deleted', 0)->get();
        }

        return Datatables::of($clinicas)
            ->addColumn('action', function ($clinicas) {
                return
                '<a id="btnMostrar" class="text-info mr-2" href="javascript:;" data-id="' . $clinicas->id . '"><i class="text-15 nav-icon i-Eye font-weight-bold"></i></a>' .
                '<a id="btnEditar" class="text-success mr-2" href="javascript:;" data-id="' . $clinicas->id . '"><i class="text-15 nav-icon i-Pen-2 font-weight-bold"></i></a>' .
                '<a class="text-danger mr-2" data-id="' . $clinicas->id . '" href="javascript:;" id="btnEliminar"><i class="text-15 nav-icon i-Close-Window font-weight-bold"></i></a>';

            })
            ->make(true);

    }
}
