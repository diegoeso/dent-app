<?php

namespace App\Http\Controllers;

use App\CodigoPostal;
use DB;
use Illuminate\Http\Request;

class CodigoPostalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CodigoPostal  $codigoPostal
     * @return \Illuminate\Http\Response
     */
    public function show($cp)
    {

        $info = CodigoPostal::select('id', 'id_municipio', 'id_estado')
            ->where('id_codigo', $cp)
            ->groupBy('id', 'id_municipio', 'id_estado')
            ->limit(1)
            ->get();
        $colonia = CodigoPostal::select(DB::raw("id,id_asenta AS colonia"))
            ->where('id_codigo', $cp)
            ->get();
        $rs['info']    = $info;
        $rs['colonia'] = $colonia;
        if (count($info) > 0) {
            $rs['success'] = true;
        } else {
            $rs['success'] = false;
        }
        return $rs;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CodigoPostal  $codigoPostal
     * @return \Illuminate\Http\Response
     */
    public function edit($cp)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CodigoPostal  $codigoPostal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CodigoPostal $codigoPostal)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CodigoPostal  $codigoPostal
     * @return \Illuminate\Http\Response
     */
    public function destroy(CodigoPostal $codigoPostal)
    {
        //
    }

    public function search_cp_2(Request $request){
        $cp = DB::table('codigos_postales')
        ->where('id_codigo', '=', $request->data)
        ->orderBy('id_asenta', 'asc')
        ->get();
        if(count($cp) == 0) return response()->json(['status_proccess' => 'error', 'message' => 'El codigo postal ingresado no es valido.']);
        $cp_list = "";
        foreach ($cp as $key => $value) 
            $cp_list .= "<option value= '".$value->id."'>".$value->id_asenta."</option>";
        return response()->json(['status_proccess' => 'success', 'cp' => $cp_list, 'del' => $cp[0]->id_municipio, 'edo' => $cp[0]->id_estado]);
    }
}
