<?php

namespace App\Http\Controllers;

use App\HistorialTratamiento;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Traits\Validacion;

class HistorialTratamientoController extends Controller
{
    use Validacion;

    public function ht_validate($data ,$type=''){
        return Validator::make($data, [
            '_pcnt_trtmnt' => $type = '' ? 'required':'',
            'medicamento' => 'required',
            'descripcion' => 'required',
            'monto' => 'nullable',
            'estatus' => 'required',
        ]
        );
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $id = $request->id;
        return response()->json([
            'success' => 1,
            'title' => 'Agregar a historial',
            'action' => "load_info_post('HistorialTratamiento/add', 'agrega_historial_frm', 'another_script', 'set_new_historial_tr')",
            'button_text' => 'Agregar',
            'content' => view('HistorialTratamiento.add', compact('id'))->render()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($this->get_sum_mount_by_client(16));
        $valida = $this->ht_validate($request->all());
        if($valida->fails()){
            $errors = $this->get_errors_validate($valida->getMessageBag()->toArray());
            return response()->json(['status_proccess' => 'error', 'message' => $errors]);
        }
        $ht = new HistorialTratamiento;
            $ht->descripcion = $request->descripcion;
            $ht->monto_a_pagar = $request->monto;
            $ht->medicamento = $request->medicamento;
            $ht->estatus = $request->estatus;
            $ht->paciente_tratamiento_id = $request->_pcnt_trtmnt;
        if(!$ht->save()) return response()->json(['status' => false, 'message' => 'Error al almacenar historial, por favor intenta mas tarde.']);
        return response()->json([
            'status' => 1, 
            'message' => 'El dato ha sido registrado',
            'extra' => [
                'id' => $ht->id,
                'estatus' => $ht->estatus,
                'cantidad' => $ht->monto_a_pagar,
                'medicacion' => nl2br($ht->medicamento),
                'observ' => nl2br($ht->descripcion),
                'created' => date('Y-m-d H:i:s'),
                "titulo" => substr($ht->descripcion, 0, 30),
                "cobrado" => $this->get_sum_mount_by_client($request->_pcnt_trtmnt),
            ]
        ]);
    }

    public function get_sum_mount_by_client($id){
        return HistorialTratamiento::where(['paciente_tratamiento_id' => $id, 'deleted' => 0, 'estatus' => 'Finalizado'])->get()->sum('monto_a_pagar');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\HistorialTratamiento  $historialTratamiento
     * @return \Illuminate\Http\Response
     */
    public function show(HistorialTratamiento $historialTratamiento)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HistorialTratamiento  $historialTratamiento
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $historial = HistorialTratamiento::findOrFail($request->id);
        return response()->json([
            'success' => 1,
            'title' => 'Editar Historial',
            'action' => "load_info_post('HistorialTratamiento/edit', 'editar_historial_frm', 'another_script', 'edit_historial_client')",
            'button_text' => 'Guardar',
            'content' => view('HistorialTratamiento.edit', compact('historial'))->render()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HistorialTratamiento  $historialTratamiento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $valida = $this->ht_validate($request->all(), 'update');
        if($valida->fails()){
            $errors = $this->get_errors_validate($valida->getMessageBag()->toArray());
            return response()->json(['status_proccess' => 'error', 'message' => $errors]);
        }
        $ht = HistorialTratamiento::findOrFail($request->_hstrl);
            $ht->descripcion = $request->descripcion;
            $ht->monto_a_pagar = $request->monto;
            $ht->medicamento = $request->medicamento;
            $ht->estatus = $request->estatus;
        if(!$ht->save()) return response()->json(['status' => false, 'message' => 'Error al almacenar historial, por favor intenta mas tarde.']);
        return response()->json([
            'status' => 1, 
            'message' => 'El dato de historial ha sido actualizado',
            'extra' => [
                'id' => $ht->id,
                'estatus' => $ht->estatus,
                'cantidad' => $ht->monto_a_pagar,
                'medicacion' => nl2br($ht->medicamento),
                'observ' => nl2br($ht->descripcion),
                'created' => $ht->created_at->format('Y-m-d H:i:s'),
                "titulo" => substr($ht->descripcion, 0, 30),
                "cobrado" => $this->get_sum_mount_by_client($ht->paciente_tratamiento_id),
            ]
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HistorialTratamiento  $historialTratamiento
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        // dd($request->all());
        $ht = HistorialTratamiento::findOrFail($request->data);
        $ht->deleted = 1;
        if(!$ht->save())
            return response()->json(['status' => 0, 'message' => 'Error al eliminar los datos del historial. Por favor intenta mas tarde']);

        return response()->json([
            'status' => 1, 
            'message' => 'El dato de historial ha sido eliminado',
            'extra' => [
                'id' => $ht->id,
                "cobrado" => $this->get_sum_mount_by_client($request->_pcnt_trtmnt),
            ]
        ]);
    }
}
