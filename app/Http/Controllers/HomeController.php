<?php

namespace App\Http\Controllers;

use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'view']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        /*
         * Cambiar switch por middlware para el manejo de las vistas dependiento el rol
         */
        $rol = Auth::user()->rol;
        switch ($rol) {
            case 1:
                return view('dashboard.admin');
                break;
            case 3:
                return view('dashboard.asistente');
                break;
            default:
                return view('dashboard.dentista');
                break;

        }

    }
}
