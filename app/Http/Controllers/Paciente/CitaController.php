<?php

namespace App\Http\Controllers\Paciente;

use App\Cita;
use App\Http\Controllers\Controller;
use App\Paciente;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CitaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:pacient');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('pacientesView.citas.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user      = Auth::user();
        $validator = Validator::make($request->all(), [
            'fecha'       => 'required | date',
            'hora_inicio' => 'required',
            // 'hora_fin'    => 'required',
            'comentarios' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'errors' => $validator->errors()]);
        }

        $cita              = new Cita;
        $cita->paciente_id = $user->id;
        $cita->paciente    = $user->fullName;
        $cita->fecha       = $request->fecha;
        $cita->hora_inicio = $request->hora_inicio;
        // $cita->hora_fin    = $request->hora_fin;
        $cita->status      = '0';
        $cita->comentarios = $request->comentarios;
        $cita->dentista_id = $user->dentista->id;
        $cita->registrado  = $user->id;
        $cita->aprobada    = 0;

        if ($cita->save()) {
            return response()->json(['success' => true, 'data' => $cita]);
        }
        return response()->json(['success' => false]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function listar_citas($bandera)
    {
        $data  = Cita::where('paciente_id', Auth::user()->id);
        $citas = $data->orderBy('fecha', 'DESC');
        if ($bandera = 0) {
            $res = $citas->limit(10)->get();
        } else {
            $res = $citas->get();
        }

        return response()->json(['success' => true, 'data' => $res, 'total' => count($res)]);
    }
}
