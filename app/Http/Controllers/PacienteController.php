<?php

namespace App\Http\Controllers;

use App\Cita;
use App\Image;
use App\Paciente;
use App\Traits\Validacion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class PacienteController extends Controller
{
    use Validacion;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('can:paciente.index')->only('index');
        $this->middleware('can:paciente.view')->only('view');
    }

    public function paciente_data_validate($request)
    {
        return Validator::make($request, [
            'nombre'        => 'required',
            'apellidos'     => 'required',
            'sexo'          => 'required',
            'direccion'     => 'required',
            //'cp_id' => 'required',
            'telefono'      => 'required|numeric|digits:10',
            'tel'           => 'nullable|numeric|digits:10',
            'email'         => 'nullable|email',
            'f_nac'         => 'required',
            'presion'       => 'nullable',
            'observaciones' => 'nullable|max:200',
        ]
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id = null)
    {
        return view('pacientes.index', compact('id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Paciente  $paciente
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $paciente = Paciente::findOrFail($request->id);
        $cp       = \App\CodigoPostal::where('id_codigo', $paciente->codigo_postal->id_codigo)->get();
        return response()->json([
            'success'     => 1,
            'title'       => 'Modificar Paciente',
            'action'      => "load_info_post('pacientes/update', 'edita_paciente_frm', 'another_script', 'refresh_paciente_info')",
            'button_text' => 'Registrar',
            'content'     => view('pacientes.edit', compact('paciente', 'cp'))->render(),
        ]);
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Paciente  $paciente
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $valida = $this->paciente_data_validate($request->all());
        if ($valida->fails()) {
            $errors = $this->get_errors_validate($valida->getMessageBag()->toArray());
            return response()->json(['status_proccess' => 'error', 'message' => $errors]);
        }

        $paciente                      = Paciente::findOrFail($request->_pcnt);
        $paciente->clinica_id          = 1;
        $paciente->codigo_postal_id    = $request->cp_id;
        $paciente->nombre              = $request->nombre;
        $paciente->apellidos           = $request->apellidos;
        $paciente->direccion           = $request->direccion;
        $paciente->sexo                = $request->sexo;
        $paciente->telefono            = $request->telefono;
        $paciente->telefono_alt        = $request->tel;
        $paciente->email               = $request->email;
        $paciente->fecha_nacimiento    = $request->f_nac;
        $paciente->enfermedades        = $request->enfermedades ? 'Si' : 'No';
        $paciente->alergias            = $request->alergias ? 'Si' : 'No';
        $paciente->presion             = $request->presion;
        $paciente->tratamiento_medico  = $request->t_medico;
        $paciente->tuberculosis        = $request->tuberculosis;
        $paciente->alergia_penicilina  = $request->penicilina;
        $paciente->alergia_medicamento = $request->a_medicamento;
        $paciente->cardiovascular      = $request->cardiovascular;
        $paciente->hemorragia          = $request->hemorragia;
        $paciente->diabetes            = $request->diabetes;
        $paciente->complicaciones      = $request->complicaciones;
        $paciente->anestecia_local     = $request->anestecia_local;
        $paciente->embarazada          = $request->embarazada;
        $paciente->medico_general      = $request->medico_general;
        $paciente->periodo_menstrual   = $request->periodo_menstrual;
        $paciente->observaciones       = $request->observaciones;
        if ($request->password) {
            $paciente->password = bcrypt($request->password);
        }
        if (!$paciente->save()) {
            return response()->json(['status' => false, 'message' => 'Error al almacenar paciente, por favor intenta mas tarde.']);
        }

        return response()->json([
            'status'  => true,
            'message' => 'El paciente ha sido almacenado',
            'extra'   => view('pacientes.info_paciente', compact('paciente'))->render(),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Paciente  $paciente
     * @return \Illuminate\Http\Response
     */
    public function destroy(Paciente $paciente)
    {
        //
    }

    public function listar($id = null)
    {
        // $dentista = ($id) ?: Auth::user()->dentista->id;
        $pacientes = $this->get_role();
        $btn       = '';
        return Datatables::of($pacientes)
            ->addColumn('action', function ($pacientes) use ($btn) {
                $btn .= '<a id="btnMostrar" class="text-info mr-2" href="' . route('pacientes.view.data', $pacientes->id) . '"><i class="text-15 nav-icon i-Eye font-weight-bold"></i></a>';
                // '<a id="btnEditar" class="text-success mr-2" href="javascript:;" data-id="' . $pacientes->id . '"><i class="text-15 nav-icon i-Pen-2 font-weight-bold"></i></a>' .
                $btn .= '<a class="text-danger mr-2" data-id="' . $pacientes->id . '" href="javascript:;" id="btnEliminar"><i class="text-15 nav-icon i-Close-Window font-weight-bold"></i></a>';
                return $btn;
            })
            ->make(true);
    }

    public function get_role()
    {

        switch (Auth::user()->rol) {
            case 1:
                $res = Paciente::all();
                break;
            case 2:
                $res = Paciente::where('dentista_id', Auth::user()->dentista->id)->get();
                break;
            case 3:
                $res = Paciente::where('dentista_id', Auth::user()->dentista_id)->get();
                break;
        }

        return $res;
    }

    public function get_to_edit(Request $request)
    {
        if (!$request->isAjax()) {
            abort(404);
        }

        dd($request->all());
    }

    public function add_view(Request $request)
    {
        $id         = $request->id;
        $complement = ($request->id_cita) ? "?id_cita={$request->id_cita}" : '';
        return response()->json([
            'success'     => 1,
            'title'       => 'Agregar Paciente',
            'action'      => "load_info_post('pacientes/add{$complement}', 'agrega_paciente_frm', 'another_script', 'redirect_new_paciente')",
            'button_text' => 'Registrar',
            'content'     => view('pacientes.add', compact('id'))->render(),
        ]);
    }

    public function add(Request $request)
    {
        $valida = $this->paciente_data_validate($request->all());
        if ($valida->fails()) {
            $errors = $this->get_errors_validate($valida->getMessageBag()->toArray());
            return response()->json(['status_proccess' => 'error', 'message' => $errors]);
        }

        $paciente                      = new Paciente;
        $paciente->dentista_id         = ($request->_dntst) ?: Auth::user()->dentista->id;
        $paciente->clinica_id          = 1;
        $paciente->codigo_postal_id    = $request->cp_id;
        $paciente->nombre              = $request->nombre;
        $paciente->apellidos           = $request->apellidos;
        $paciente->direccion           = $request->direccion;
        $paciente->sexo                = $request->sexo;
        $paciente->telefono            = $request->telefono;
        $paciente->telefono_alt        = $request->tel;
        $paciente->email               = $request->email;
        $paciente->fecha_nacimiento    = $request->f_nac;
        $paciente->enfermedades        = $request->enfermedades ? 'Si' : 'No';
        $paciente->alergias            = $request->alergias ? 'Si' : 'No';
        $paciente->presion             = $request->presion;
        $paciente->tratamiento_medico  = $request->t_medico;
        $paciente->tuberculosis        = $request->tuberculosis;
        $paciente->alergia_penicilina  = $request->penicilina;
        $paciente->alergia_medicamento = $request->a_medicamento;
        $paciente->cardiovascular      = $request->cardiovascular;
        $paciente->hemorragia          = $request->hemorragia;
        $paciente->diabetes            = $request->diabetes;
        $paciente->complicaciones      = $request->complicaciones;
        $paciente->anestecia_local     = $request->anestecia_local;
        $paciente->embarazada          = $request->embarazada;
        $paciente->medico_general      = $request->medico_general;
        $paciente->periodo_menstrual   = $request->periodo_menstrual;
        $paciente->observaciones       = $request->observaciones;
        $paciente->password            = bcrypt($request->password);
        if (!$paciente->save()) {
            return response()->json(['status' => false, 'message' => 'Error al almacenar paciente, por favor intenta mas tarde.']);
        }

        if ($request->id_cita) {
            $cita              = \App\Cita::findOrFail($request->id_cita);
            $cita->paciente_id = $paciente->id;
            $cita->save();
        }
        return response()->json(['status' => true, 'message' => 'El paciente ha sido almacenado', 'extra' => $paciente->id]);

    }

    public function view(Request $request, $id)
    {
        $paciente = Paciente::with(['tratamientos' => function ($q) {
            $q->where(['deleted' => 0])->orderBy('id', 'desc');
        }])->findOrFail($id);
        $imagenes     = Image::where('paciente_id', $id)->get();
        $active_modal = 0;
        if (isset($request->new)) {
            $active_modal = 1;
        }

        // dd($paciente->tratamientos);
        return view('pacientes.view', compact('paciente', 'active_modal', 'imagenes'));
    }

    public function add_cita(Request $request)
    {
        // dd($request->all());
        $id = $request->id;
        return response()->json([
            'success'     => 1,
            'title'       => 'Agregar cita',
            'action'      => "load_info_post('pacientes/add_new_cita', 'agrega_cita_paciente', 'another_script', 'add_new_cita')",
            'button_text' => 'Registrar',
            'content'     => view('pacientes.add_cita', compact('id'))->render(),
        ]);
    }

    public function add_new_cita(Request $request)
    {
        $cita              = new Cita;
        $cita->paciente_id = $request->_pcnt;
        $cita->fecha       = $request->fecha_inicio;
        $cita->hora_inicio = $request->hora_inicio;
        $cita->hora_fin    = $request->hora_fin;
        $cita->comentarios = $request->comentarios;
        $cita->status      = $request->status;
        $cita->dentista_id = Paciente::find($request->_pcnt)->dentista_id;
        if (!$cita->save()) {
            return response()->json(['status' => false, 'message' => 'Error al almacenar cita, por favor intenta mas tarde.']);
        }

        return response()->json([
            'status'  => true,
            'message' => 'La cita del paciente ha sido almacenada',
            'extra'   => [
                'fecha'  => $request->fecha_inicio,
                'inicio' => $request->hora_inicio,
                'fin'    => $request->hora_fin,
                'status' => $cita->status_nombre,
            ],
        ]);
    }
}
