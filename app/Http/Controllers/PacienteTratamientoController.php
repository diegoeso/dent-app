<?php

namespace App\Http\Controllers;

use App\PacienteTratamiento;
use App\Traits\Validacion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PacienteTratamientoController extends Controller
{
    use Validacion;

    public function pt_validate($data, $type = '')
    {
        return Validator::make($data, [
            '_pcnt'       => $type = '' ? 'required' : '',
            'categoria'   => 'required',
            'tratamiento' => 'required',
            'sintomas'    => 'required',
            'descripcion' => 'required',
        ]
        );
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $valida = $this->pt_validate($request->all());
        if ($valida->fails()) {
            $errors = $this->get_errors_validate($valida->getMessageBag()->toArray());
            return response()->json(['status_proccess' => 'error', 'message' => $errors]);
        }
        $pt                 = new PacienteTratamiento;
        $pt->tratamiento_id = $request->tratamiento;
        $pt->paciente_id    = $request->_pcnt;
        $pt->sintomas       = $request->sintomas;
        $pt->descripcion    = $request->descripcion;
        $pt->presupuesto    = $request->presupuesto;
        $pt->estatus        = "En Proceso";
        if (!$pt->save()) {
            return response()->json(['status' => false, 'message' => 'Error al almacenar tratamiento, por favor intenta mas tarde.']);
        }

        return response()->json([
            'status'  => 1,
            'message' => 'El tratamiento ha sido registrado',
            'extra'   => [
                'id'          => $pt->id,
                'titulo'      => $pt->tratamiento->categoria->nombre,
                'tratamiento' => $pt->tratamiento->nombre,
                'created'     => date('Y-m-d H:i:s'),
            ],
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PacienteTratamiento  $pacienteTratamiento
     * @return \Illuminate\Http\Response
     */
    public function show(PacienteTratamiento $pacienteTratamiento)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PacienteTratamiento  $pacienteTratamiento
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $tratamiento      = PacienteTratamiento::findOrFail($request->id);
        $tratamientos     = \App\Tratamiento::where(['categoria_id' => $tratamiento->tratamiento->categoria_id, 'deleted' => 0])->orderBy('nombre', 'asc')->get();
        $cat_tratamientos = \App\CategoriaTratamiento::where(['deleted' => 0])->orderBy('nombre', 'asc')->get();
        return response()->json([
            'success'     => 1,
            'title'       => 'Editar Tratamiento',
            'action'      => "load_info_post('PacienteTratamiento/edit', 'editar_tratamiento_frm', 'another_script', 'edit_tratamiento_client')",
            'button_text' => 'Guardar',
            'content'     => view('PacienteTratamiento.edit', compact('tratamiento', 'cat_tratamientos', 'tratamientos'))->render(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PacienteTratamiento  $pacienteTratamiento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        // dd($request->all());
        $valida = $this->pt_validate($request->all(), 'update');
        if ($valida->fails()) {
            $errors = $this->get_errors_validate($valida->getMessageBag()->toArray());
            return response()->json(['status_proccess' => 'error', 'message' => $errors]);
        }
        $pt                 = PacienteTratamiento::findOrFail($request->_trtmnt_pcnt);
        $pt->tratamiento_id = $request->tratamiento;
        $pt->sintomas       = $request->sintomas;
        $pt->descripcion    = $request->descripcion;
        $pt->presupuesto    = $request->presupuesto;
        $pt->estatus        = $request->estatus;
        if (!$pt->save()) {
            return response()->json(['status' => false, 'message' => 'Error al almacenar tratamiento, por favor intenta mas tarde.']);
        }

        return response()->json([
            'status'  => 1,
            'message' => 'El tratamiento ha sido modificado',
            'extra'   => [
                'id'      => $pt->id,
                'cat'     => $pt->tratamiento->categoria->nombre,
                'tr'      => $pt->tratamiento->nombre,
                'estatus' => $pt->estatus,
            ],
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PacienteTratamiento  $pacienteTratamiento
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $tratamiento          = PacienteTratamiento::findOrFail($request->data);
        $tratamiento->deleted = 1;
        if (!$tratamiento->save()) {
            return response()->json(['status' => 0, 'message' => 'Error al eliminar los datos del tratamiento. Por favor intenta mas tarde']);
        }

        return response()->json([
            'status'  => 1,
            'message' => 'Los datos del tratamiento han sido eliminados',
            'extra'   => $tratamiento->id,
        ]);
    }

    public function listar(Request $request)
    {
        $tratamiento = PacienteTratamiento::with(['historial' => function ($q) {
            $q->where(['deleted' => 0])->orderBy('id', 'desc');
        }])->findOrFail($request->id);
        $pagado = \App\HistorialTratamiento::where(['paciente_tratamiento_id' => $request->id, 'deleted' => 0, 'estatus' => 'Finalizado'])->get()->sum('monto_a_pagar');
        return response()->json([
            'success' => 1,
            'content' => view('PacienteTratamiento.tratamientos_client', compact('tratamiento', 'pagado'))->render(),
        ]);
    }

    public function add_from_ajax(Request $request)
    {
        $id               = $request->id;
        $cat_tratamientos = \App\CategoriaTratamiento::where(['deleted' => 0])->orderBy('nombre', 'asc')->get();
        // dd($cat_tratamientos);
        // $tratamientos = $cat_tratamientos->tratamientos;
        return response()->json([
            'success'     => 1,
            'title'       => 'Nuevo Tratamiento',
            'action'      => "load_info_post('PacienteTratamiento/add', 'agrega_tratamiento_frm', 'another_script', 'set_new_tratamiento')",
            'button_text' => 'Agregar',
            'content'     => view('PacienteTratamiento.add_tratamiento_paciente', compact('id', 'cat_tratamientos'))->render(),
        ]);
    }
}
