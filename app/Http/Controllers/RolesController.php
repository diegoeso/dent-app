<?php

namespace App\Http\Controllers;

use Caffeinated\Shinobi\Models\Permission;
use Caffeinated\Shinobi\Models\Role;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class RolesController extends Controller
{
    public function listar_roles($id = null)
    {
        $roles = Role::all();
        return response()->json($roles);
    }

    public function listar_permisos()
    {
        $permisos = Permission::all();
        return response()->json($permisos);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('roles.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rol              = new Role;
        $rol->name        = $request->name;
        $rol->slug        = $request->slug;
        $rol->special     = $request->special;
        $rol->description = $request->description;

        if ($rol->save()) {
            $rol->givePermissionTo($request->permisos);
            return response()->json(['success' => true]);
        }
        return response()->json(['success' => false]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Dentista  $dentista
     * @return \Illuminate\Http\Response
     */
    public function show(Dentista $dentista)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Dentista  $dentista
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rol = Role::with('permissions')->where('id', $id)->first();
        return response()->json($rol);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Dentista  $dentista
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $rol              = Role::findOrFail($id);
        $rol->name        = $request->name;
        $rol->slug        = $request->slug;
        $rol->special     = $request->special;
        $rol->description = $request->description;

        if ($rol->save()) {
            $rol->syncPermissions($request->permisos);
            return response()->json(['success' => true]);
        }
        return response()->json(['success' => false]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Dentista  $dentista
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $clinica = Role::findOrFail($id);
        if ($clinica->delete()) {
            return response()->json(['success' => 'true']);
        } else {
            return response()->json(['success' => 'false']);
        }
    }

    public function listar_roles_table()
    {

        $roles = Role::all();
        return Datatables::of($roles)
            ->addColumn('action', function ($roles) {
                return
                '<a id="btnMostrar" class="text-info mr-2" href="javascript:;" data-id="' . $roles->id . '"><i class="text-15 nav-icon i-Eye font-weight-bold"></i></a>' .
                '<a id="btnEditar" class="text-success mr-2" href="javascript:;" data-id="' . $roles->id . '"><i class="text-15 nav-icon i-Pen-2 font-weight-bold"></i></a>' .
                '<a class="text-danger mr-2" data-id="' . $roles->id . '" href="javascript:;" id="btnEliminar"><i class="text-15 nav-icon i-Close-Window font-weight-bold"></i></a>';

            })
            ->make(true);

    }

    public function listar_permisos_table()
    {

        $permisos = Permission::all();

        return Datatables::of($permisos)
        // ->addColumn('action', function ($permisos) {
        //     return
        //     '<a id="btnMostrar" class="text-info mr-2" href="javascript:;" data-id="' . $permisos->id . '"><i class="text-15 nav-icon i-Eye font-weight-bold"></i></a>' .
        //     '<a id="btnEditar" class="text-success mr-2" href="javascript:;" data-id="' . $permisos->id . '"><i class="text-15 nav-icon i-Pen-2 font-weight-bold"></i></a>' .
        //     '<a class="text-danger mr-2" data-id="' . $permisos->id . '" href="javascript:;" id="btnEliminar"><i class="text-15 nav-icon i-Close-Window font-weight-bold"></i></a>';

        // })
            ->make(true);

    }

    // Permisos
    public function permisos(Request $request)
    {
        $permiso              = new Permission;
        $permiso->name        = $request->name;
        $permiso->slug        = $request->slug;
        $permiso->description = $request->description;

        if ($permiso->save()) {
            return response()->json(['success' => true]);
        }
        return response()->json(['success' => false]);
    }
}
