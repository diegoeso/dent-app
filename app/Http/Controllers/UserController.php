<?php

namespace App\Http\Controllers;

use App\Dentista;
use App\Http\Requests\UserStoreRequest;
use App\Http\Requests\UserUpdateRequest;
use App\User;
use Auth;
use Caffeinated\Shinobi\Models\Role;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        // $this->middleware('can:asistentes.index')->only('index');
        // $this->middleware('can:asistentes.show')->only('show');
        // $this->middleware('can:asistentes.create')->only(['create', 'store']);
        // $this->middleware('can:asistentes.edit')->only(['edit']);
        // $this->middleware('can:asistentes.destroy')->only('destroy');
        // $this->middleware('can:dentista.index')->only('index');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($rol = null)
    {
        switch ($rol) {
            case 'administradores':
                $rol = 1;
                break;
            case 'dentistas':
                $rol = 2;
                break;
            case 'asistentes':
                $rol = 3;
                break;
            default:
                $rol = 2;
                break;
        }
        return view('usuarios.index', compact('rol'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserStoreRequest $request)
    {

        $rol                    = $request->tipo_rol;
        $user                   = new User;
        $user->nombre           = $request->nombre;
        $user->apellidos        = $request->apellidos;
        $user->codigo_postal_id = $request->codigo_postal_id;
        $user->direccion        = $request->direccion;
        $user->telefono         = $request->telefono;
        $user->telefono_alt     = $request->telefono_alt;
        $user->username         = $request->username;
        if ($rol == 3) {
            if (Auth::user()->rol == 2) {
                $user->dentista_id = Auth::user()->dentista->id;
            } else {
                $user->dentista_id = $request->dentista_id;
            }
        }
        $user->email    = $request->email;
        $user->password = bcrypt($request->password);
        $user->rol      = $rol;

        if ($user->save()) {
            if ($rol == 2) {
                $dentista          = new Dentista;
                $dentista->user_id = $user->id;
                $dentista->cedula  = $request->cedula;
                $dentista->escuela = $request->escuela;
                $dentista->save();
            }
            $user->assignRoles(Role::findOrFail($rol)->slug);
            return response()->json(['success' => true]);
        }
        return response()->json(['success' => false]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $res = User::with(
            'codigop',
            'dentista',
            'dentista.clinicas',
            'dentista_asistente',
            'dentista_asistente.user'
        )->where('id', $id)->first();
        return response()->json(['success' => true, 'res' => $res]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, $id)
    {

        $rol                    = $request->tipo_rol;
        $user                   = User::findOrFail($id);
        $user->nombre           = $request->nombre;
        $user->apellidos        = $request->apellidos;
        $user->codigo_postal_id = $request->codigo_postal_id;
        $user->direccion        = $request->direccion;
        $user->telefono         = $request->telefono;
        $user->telefono_alt     = $request->telefono_alt;
        $user->username         = $request->username;
        // if ($rol == 3) {
        //     $user->dentista_id = $request->dentista_id;
        // }

        if ($rol == 3) {
            if (Auth::user()->rol == 2) {
                $user->dentista_id = Auth::user()->dentista->id;
            } else {
                $user->dentista_id = $request->dentista_id;
            }
        }
        $user->email    = $request->email;
        $user->password = bcrypt($request->password);
        $user->rol      = $rol;

        if ($user->save()) {
            if ($rol == 2) {
                $dentista          = Dentista::findOrFail($user->dentista_id);
                $dentista->user_id = $user->id;
                $dentista->cedula  = $request->cedula;
                $dentista->escuela = $request->escuela;
                $dentista->save();
            }
            $user->assignRoles(Role::findOrFail($rol)->slug);
            return response()->json(['success' => true]);
        }
        return response()->json(['success' => false]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user          = User::FindOrFail($id);
        $user->deleted = 1;
        if ($user->save()) {
            return response()->json(['success' => 'true']);
        } else {
            return response()->json(['success' => 'false']);
        }
    }

    public function listar_usuarios($rol = null)
    {
        if (Auth::user()->rol == 2) {
            $dentista = Auth::user()->dentista->id;
            $usuarios = User::with('dentista')
                ->where('deleted', 0)
                ->where('dentista_id', $dentista)
                ->where('rol', 'like', '%' . $rol . '%')
                ->get();
        } else {
            $usuarios = User::with('dentista')
                ->where('deleted', 0)
                ->where('rol', 'like', '%' . $rol . '%')
                ->get();
        }

        return Datatables::of($usuarios)
            ->editColumn('nombre', function ($usuarios) {
                return $usuarios->FullName;
            })
            ->addColumn('action', function ($usuarios) {
                return
                '<a id="btnMostrar" class="text-info mr-2" href="javascript:;" data-id="' . $usuarios->id . '"><i class="text-15 nav-icon i-Eye font-weight-bold"></i></a>' .
                '<a id="btnEditar" class="text-success mr-2" href="javascript:;" data-id="' . $usuarios->id . '"><i class="text-15 nav-icon i-Pen-2 font-weight-bold"></i></a>' .
                '<a class="text-danger mr-2" data-id="' . $usuarios->id . '" href="javascript:;" id="btnEliminar"><i class="text-15 nav-icon i-Close-Window font-weight-bold"></i></a>';

            })
            ->make(true);
    }

    public function listar_dentistas()
    {
        $res = User::all();
        return response()->json($res);
    }
}
