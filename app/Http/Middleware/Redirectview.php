<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class Redirectview
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $rol = Auth::user()->rol;
        // dd($rol);
        // if ($rol == 2) {
        //     return view('dashboard.dentista');
        // }
        return $next($request);
    }
}
