<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        return [
            'nombre'           => 'required',
            'apellidos'        => 'required',
            'direccion'        => 'required',
            'codigo_postal_id' => 'required',
            'telefono'         => 'required|numeric|digits:10',
            'telefono_alt'     => ($request->telefono_alt) ? 'numeric|digits:10' : '',
            'username'         => 'required|unique:users,username,' . $request->segment(2),
            'cedula'           => ($request->tipo_rol == 2) ? 'required' : '',
            'escuela'          => ($request->tipo_rol == 2) ? 'required' : '',
            'dentista_id'      => ($request->tipo_rol == 3) ? 'required' : '',
            'email'            => 'required|email|unique:users,email,' . $request->segment(2),
        ];
    }
}
