<?php

namespace App;

use App\Paciente;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table = 'images';

    /**
     * Fields that can be mass assigned.
     *
     * @var array
     */
    protected $fillable = [
        'paciente_id',
        'titulo',
        'url',
    ];

    public function paciente()
    {
        return $this->belongsTo(Paciente::class);
    }
}
