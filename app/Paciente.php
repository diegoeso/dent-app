<?php

namespace App;

use App\Cita;
use App\CodigoPostal;
use App\Dentista;
use App\Image;
use App\PacienteTratamiento;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Paciente extends Authenticatable
{
    use Notifiable;

    public function tratamientos()
    {
        return $this->hasMany(PacienteTratamiento::class);
    }

    public function citas()
    {
        return $this->hasMany(Cita::class);
    }

    public function codigo_postal()
    {
        return $this->belongsTo(CodigoPostal::class, 'codigo_postal_id', 'id');
    }

    public function getFullNameAttribute()
    {
        return $this->nombre . ' ' . $this->apellidos;
    }

    public function imagenes()
    {
        return $this->hasMany(Image::class);
    }

    /**
     * Dentista belongs to .
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function dentista()
    {
        return $this->belongsTo(Dentista::class);
    }

}
