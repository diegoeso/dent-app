<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Tratamiento;
use App\HistorialTratamiento;
class PacienteTratamiento extends Model
{
    public function tratamiento(){
    	return $this->belongsTo(Tratamiento::class);
    }

    public function historial(){
    	return $this->hasMany(HistorialTratamiento::class);
    }
}
