<?php

namespace App\Providers;

use App\Cita;
use App\Paciente;
use Auth;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        /*
         * Notificaciones
         */
        view()->composer('layouts.app_dent', function ($view) {
            $view->with('notificaciones', Cita::where('dentista_id', Auth::user()->dentista->id)
                    ->where('aprobada', 0)
                    ->orderBy('id', 'DESC')
                    ->get()
            );
        });

        /*
         * Dashboard dentista
         */
        view()->composer('dashboard.dentista', function ($view) {
            $view->with('pacientes', Paciente::where('dentista_id', Auth::user()->dentista->id)->get()
            );
        });

        /*
         * Dashboard dentista
         */
        view()->composer('dashboard.dentista', function ($view) {
            $view->with('citas', Cita::where('dentista_id', Auth::user()->dentista->id)->get()
            );
        });
    }
}
