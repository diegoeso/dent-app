<?php

namespace App\Traits;

trait Validacion
{
    /**
     * Get errors and process to return in html format
     *
     * @param array $errors This is errors from validate
     * @author Gustavo Garcia
     * @return string $result Errors in html format
     */
    public function get_errors_validate($errors = array())
    {
        if (empty($errors)) {
            return "";
        }

        $result = "<ul>";
        foreach ($errors as $key => $value) {
            $result .= "<li style='text-align:left;'>" . $value[0] . "</li>";
        }

        return $result .= "</ul>";
    }
}
