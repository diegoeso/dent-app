<?php

namespace App;
use App\CategoriaTratamiento;
use Illuminate\Database\Eloquent\Model;

class Tratamiento extends Model
{
    public function categoria(){
    	return $this->belongsTo(CategoriaTratamiento::class);
    }
}
