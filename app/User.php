<?php

namespace App;

use App\Dentista;
use Caffeinated\Shinobi\Concerns\HasRolesAndPermissions;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable, HasRolesAndPermissions;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre',
        'apellidos',
        'direccion',
        'telefono',
        'username',
        'cedula',
        'dentista_id',
        'email',
        'telefono_alt',
        'codigo_postal_id',
        'estatus',
        'dentista_id',

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function dentista()
    {
        return $this->hasOne(Dentista::class);
    }

    public function codigop()
    {
        return $this->hasOne(CodigoPostal::class, 'id', 'codigo_postal_id');
    }

    public function dentista_asistente()
    {
        return $this->hasOne(Dentista::class, 'id', 'dentista_id');
    }

    public function getFullNameAttribute()
    {
        return $this->nombre . ' ' . $this->apellidos;
    }
}
