<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\CategoriaTratamiento;
use Faker\Generator as Faker;

$factory->define(CategoriaTratamiento::class, function (Faker $faker) {
    return [
        'nombre' => $faker->randomElement(['Dentición Temporal', 'Dentición Permanente']),
    ];
});
