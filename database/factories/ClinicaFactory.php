<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Clinica;
use Faker\Generator as Faker;

$factory->define(Clinica::class, function (Faker $faker) {
    return [
        'nombre'           => $faker->company,
        'direccion'        => $faker->address,
        'telefono'         => $faker->phoneNumber,
        'dentista_id'      => $faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]),
        'codigo_postal_id' => 64547,
    ];
});
