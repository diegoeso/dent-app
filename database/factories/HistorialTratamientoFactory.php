<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\HistorialTratamiento;
use Faker\Generator as Faker;

$factory->define(HistorialTratamiento::class, function (Faker $faker) {
    return [
        'descripcion'             => $faker->realText(300, 2),
        // 'monto_a_pagar'           => $faker->randomDigit(10000, 10000000),
        'monto_a_pagar'           => rand(100, 1000),
        'medicamento'             => $faker->word,
        'paciente_tratamiento_id' => $faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]),
    ];
});
