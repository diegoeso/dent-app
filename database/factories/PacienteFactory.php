<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Paciente;
use Faker\Generator as Faker;

$factory->define(Paciente::class, function (Faker $faker) {
    return [
        'nombre'            => $faker->name,
        'apellidos'         => $faker->lastName,
        'direccion'         => $faker->address,
        'telefono'          => $faker->phoneNumber,
        'fecha_nacimiento'  => $faker->date('Y-m-d', 'now'),
        'enfermedades'      => $faker->randomElement(['Si', 'No']),
        'alergias'          => $faker->randomElement(['Si', 'No']),
        'presion'           => $faker->randomDigit,
        'sexo'              => $faker->randomElement(['Masculino', 'Femenino']),
        'dentista_id'       => $faker->randomElement([1, 2, 3, 4, 5]),
        'clinica_id'        => $faker->randomElement([1, 2, 3, 4, 5]),
        'codigo_postal_id'  => 64547,
        'password'          => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'email'             => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'remember_token'    => Str::random(10),
    ];
});
