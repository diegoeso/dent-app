<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\PacienteTratamiento;
use Faker\Generator as Faker;

$factory->define(PacienteTratamiento::class, function (Faker $faker) {
    return [
        'paciente_id'    => $faker->numberBetween(1, 50),
        'tratamiento_id' => $faker->numberBetween(1, 20),
        'sintomas'       => $faker->realText(300, 2),
        'descripcion'    => $faker->realText(300, 2),
    ];
});
