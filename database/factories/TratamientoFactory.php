<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Tratamiento;
use Faker\Generator as Faker;

$factory->define(Tratamiento::class, function (Faker $faker) {
    return [
        'nombre'       => $faker->word,
        'descripcion'  => $faker->text(300),
        'precio'       => $faker->numberBetween(1000, 5000),
        'duracion'     => $faker->randomElement(['1 mes', '2 meses', '3 meses', '4 meses', '5 meses']),
        'categoria_id' => $faker->randomElement([1, 2, 3, 4, 5]),
    ];
});

// Tratamiento::create([
// 	'nombre'       => 'Cariadas',
//     'descripcion'  => 'No especificado',
//     'precio'       => '0.00',
//     'duracion'     => '1',
//     'categoria_id' => 1,
// ]);
// Tratamiento::create([
// 	'nombre'       => 'Perdidas',
//     'descripcion'  => 'No especificado',
//     'precio'       => '0.00',
//     'duracion'     => '1',
//     'categoria_id' => 1,
// ]);
// Tratamiento::create([
// 	'nombre'       => 'Obturadas',
//     'descripcion'  => 'No especificado',
//     'precio'       => '0.00',
//     'duracion'     => '1',
//     'categoria_id' => 1,
// ]);
// Tratamiento::create([
// 	'nombre'       => 'Extracciones indicadas',
//     'descripcion'  => 'No especificado',
//     'precio'       => '0.00',
//     'duracion'     => '1',
//     'categoria_id' => 1,
// ]);
// Tratamiento::create([
// 	'nombre'       => 'Cariadas',
//     'descripcion'  => 'No especificado',
//     'precio'       => '0.00',
//     'duracion'     => '1',
//     'categoria_id' => 2,
// ]);
// Tratamiento::create([
// 	'nombre'       => 'Perdidas',
//     'descripcion'  => 'No especificado',
//     'precio'       => '0.00',
//     'duracion'     => '1',
//     'categoria_id' => 2,
// ]);
// Tratamiento::create([
// 	'nombre'       => 'Obturadas',
//     'descripcion'  => 'No especificado',
//     'precio'       => '0.00',
//     'duracion'     => '1',
//     'categoria_id' => 2,
// ]);
// Tratamiento::create([
// 	'nombre'       => 'Extracciones indicadas',
//     'descripcion'  => 'No especificado',
//     'precio'       => '0.00',
//     'duracion'     => '1',
//     'categoria_id' => 2,
// ]);
