<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
 */

$factory->define(User::class, function (Faker $faker) {
    return [
        'nombre'            => $faker->name,
        'apellidos'         => $faker->lastName,
        'direccion'         => $faker->address,
        'telefono'          => $faker->phoneNumber,
        'username'          => $faker->userName,
        'email'             => $faker->unique()->safeEmail,
        'dentista_id'       => $faker->randomElement([1, 2, 3, 4, 5]),
        // 'clinica_id'        => $faker->randomElement([1, 2, 3, 4, 5]),
        'rol'               => $faker->randomElement([1, 2, 3]),
        'email_verified_at' => now(),
        'remember_token'    => Str::random(10),
        'password'          => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'codigo_postal_id'  => 64547,
    ];
});
