<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePacientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pacientes', function (Blueprint $table) {
            // $table->increments('id');
            $table->id();
            $table->integer('codigo_postal_id')->nullable();
            $table->integer('dentista_id');

            $table->integer('clinica_id');
            // $table->foreignId('clinica_id');
            $table->string('nombre');
            $table->string('apellidos');
            $table->string('direccion');
            $table->string('telefono');
            $table->string('telefono_alt')->nullable();
            $table->string('email')->unique();
            $table->string('password');
            $table->date('fecha_nacimiento')->nullable();
            $table->enum('enfermedades', ['Si', 'No']);
            $table->enum('alergias', ['Si', 'No']);
            $table->string('presion')->nullable();
            $table->enum('sexo', ['Masculino', 'Femenino']);
            $table->string('tratamiento_medico', 50)->nullable();
            $table->string('tuberculosis', 50)->nullable();
            $table->string('alergia_penicilina', 50)->nullable();
            $table->string('alergia_medicamento', 50)->nullable();
            $table->string('cardiovascular', 50)->nullable();
            $table->string('hemorragia', 50)->nullable();
            $table->string('diabetes', 50)->nullable();
            $table->string('complicaciones', 100)->nullable();
            $table->string('anestecia_local', 50)->nullable();
            $table->string('embarazada', 50)->nullable();
            $table->string('medico_general', 50)->nullable();
            $table->string('periodo_menstrual', 50)->nullable();
            $table->text('observaciones')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pacientes');
    }
}
