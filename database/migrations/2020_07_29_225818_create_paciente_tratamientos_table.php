<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePacienteTratamientosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paciente_tratamientos', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('paciente_id');
            $table->foreign('paciente_id')->references('id')->on('pacientes');

            // $table->integer('paciente_id')->unsigned();
            // $table->foreign('paciente_id')->references('id')->on('pacientes');

            $table->unsignedBigInteger('tratamiento_id');
            $table->foreign('tratamiento_id')->references('id')->on('tratamientos');

            // $table->integer('tratamiento_id')->unsigned();
            // $table->foreign('tratamiento_id')->references('id')->on('tratamiento');

            $table->text('sintomas');
            $table->float('presupuesto', 8, 2)->nullable();
            $table->text('descripcion');
            $table->string('estatus')->default('En Proceso');
            $table->tinyInteger('deleted')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paciente_tratamientos');
    }
}
