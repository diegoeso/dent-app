<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistorialTratamientosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historial_tratamientos', function (Blueprint $table) {
            $table->increments('id');
            $table->text('descripcion');
            $table->float('monto_a_pagar');
            $table->text('medicamento');
            $table->string('estatus')->default('activo');
            $table->char('deleted')->default(0);

            $table->unsignedBigInteger('paciente_tratamiento_id');
            $table->foreign('paciente_tratamiento_id')->references('id')->on('paciente_tratamientos');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historial_tratamientos');
    }
}
