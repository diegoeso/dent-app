<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCitasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('citas', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('paciente_id')->nullable();
            $table->foreign('paciente_id')->references('id')->on('pacientes');
            $table->string('paciente')->nullable();
            $table->date('fecha');
            $table->time('hora_inicio');
            $table->time('hora_fin')->nullable();
            $table->char('status', 1)->default(0);
            $table->mediumText('comentarios');
            $table->unsignedBigInteger('dentista_id')->nullable();
            $table->foreign('dentista_id')->references('id')->on('dentistas');
            $table->unsignedBigInteger('registrado')->nullable();
            $table->char('con_registro', 1)->default(1);
            $table->double('aprobada')->default(0);
            $table->double('visible')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('citas');
    }
}
