<?php

use App\User;
use Caffeinated\Shinobi\Models\Role;
use Faker\Generator as Faker;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $faker = new Faker;
        $this->call(UserSeeder::class);
        $this->call(DentistaSeeder::class);
        $this->call(ClinicaSeeder::class);
        $this->call(CategoriaTratamientoSeeder::class);
        $this->call(TratamientoSeeder::class);
        $this->call(PacienteSeeder::class);
        $this->call(PacienteTratamientoSeeder::class);
        $this->call(HistorialTratamientoSeeder::class);

        /*1*/
        Role::create([
            'name'        => 'Administrador',
            'slug'        => 'admin',
            'description' => 'Acceso total a todo el sistema',
            'special'     => 'all-access',
        ]);
        /*2*/
        Role::create([
            'name'        => 'Dentista',
            'slug'        => 'dentista',
            'description' => 'Acceso restringido a la elección del administrador.',
            // 'special'     => '',
        ]);
        /*3*/
        Role::create([
            'name'        => 'Asistente',
            'slug'        => 'asistente',
            'description' => 'Acceso restringido a la elección del administrador.',
            // 'special'     => '',
        ]);

        User::create([
            'nombre'            => 'Diego Enrique',
            'apellidos'         => 'Sanchez Ordoñez',
            'direccion'         => 'Av. Manuel Gonzalez',
            'telefono'          => '7131150285',
            'username'          => 'diegoeso',
            'email'             => 'diego.enrique76@gmail.com',
            'rol'               => 1,
            'email_verified_at' => now(),
            'password'          => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token'    => Str::random(10),
        ]);
        User::find(21)->assignRoles('admin');
        
        User::create([
            'nombre'            => 'Gustavo',
            'apellidos'         => 'Garcia',
            'direccion'         => 'Cda. Manuel Gomez Morin',
            'telefono'          => '5578948370',
            'username'          => 'ggarcgom',
            'email'             => 'garcia.go.gustavo@gmail.com',
            'rol'               => 2,
            'email_verified_at' => now(),
            'password'          => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token'    => Str::random(10),
        ]);
        User::findOrFail(22)->assignRoles('dentista');

        User::create([
            'dentista_id'       => 22,
            'nombre'            => 'Lorena',
            'apellidos'         => 'Martinez',
            'direccion'         => 'Cda. Manuel Gomez Morin',
            'telefono'          => '5578948370',
            'username'          => 'asistente',
            'email'             => 'lorena@gmail.com',
            'rol'               => 2,
            'email_verified_at' => now(),
            'password'          => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token'    => Str::random(10),
        ]);
        User::findOrFail(23)->assignRoles('asistente');

        \App\Dentista::create([
            'user_id'            => '22',
            'cedula'         => 'QWERTYUI',
            'escuela'         => 'UNAM',
            'estatus'          => 'activo',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $this->call(PermisosSeeder::class);

        // Rol dentista
        Role::find(2)->givePermissionTo(
            'asistente.index',
            'asistente.show',
            'asistente.create',
            'asistente.edit',
            'asistente.destroy',

            'clinica.index',
            'clinica.show',
            'clinica.create',
            'clinica.edit',
            'clinica.destroy',

            'paciente.index',
            'paciente.view',
            'paciente.create',
            'paciente.edit',
            // 'paciente.destroy',
        );
        // Rol dentista
        Role::find(3)->givePermissionTo(
            'paciente.index',
            'paciente.view',
            'paciente.create',
            'paciente.edit',
            // 'paciente.destroy',
        );

        \App\Tratamiento::create([
            'nombre'       => 'Cariadas',
            'descripcion'  => 'No especificado',
            'precio'       => '0.00',
            'duracion'     => '1',
            'categoria_id' => 1,
        ]);
        \App\Tratamiento::create([
            'nombre'       => 'Perdidas',
            'descripcion'  => 'No especificado',
            'precio'       => '0.00',
            'duracion'     => '1',
            'categoria_id' => 1,
        ]);
        \App\Tratamiento::create([
            'nombre'       => 'Obturadas',
            'descripcion'  => 'No especificado',
            'precio'       => '0.00',
            'duracion'     => '1',
            'categoria_id' => 1,
        ]);
        \App\Tratamiento::create([
            'nombre'       => 'Extracciones indicadas',
            'descripcion'  => 'No especificado',
            'precio'       => '0.00',
            'duracion'     => '1',
            'categoria_id' => 1,
        ]);
        \App\Tratamiento::create([
            'nombre'       => 'Cariadas',
            'descripcion'  => 'No especificado',
            'precio'       => '0.00',
            'duracion'     => '1',
            'categoria_id' => 2,
        ]);
        \App\Tratamiento::create([
            'nombre'       => 'Perdidas',
            'descripcion'  => 'No especificado',
            'precio'       => '0.00',
            'duracion'     => '1',
            'categoria_id' => 2,
        ]);
        \App\Tratamiento::create([
            'nombre'       => 'Obturadas',
            'descripcion'  => 'No especificado',
            'precio'       => '0.00',
            'duracion'     => '1',
            'categoria_id' => 2,
        ]);
        \App\Tratamiento::create([
            'nombre'       => 'Extracciones indicadas',
            'descripcion'  => 'No especificado',
            'precio'       => '0.00',
            'duracion'     => '1',
            'categoria_id' => 2,
        ]);
    }
}
