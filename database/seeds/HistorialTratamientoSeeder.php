<?php

use App\HistorialTratamiento;
use Illuminate\Database\Seeder;

class HistorialTratamientoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(HistorialTratamiento::class, 50)->create();
    }
}
