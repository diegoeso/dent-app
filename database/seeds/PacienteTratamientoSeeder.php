<?php

use App\PacienteTratamiento;
use Illuminate\Database\Seeder;

class PacienteTratamientoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(PacienteTratamiento::class, 10)->create();
    }
}
