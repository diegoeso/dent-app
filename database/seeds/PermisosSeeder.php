<?php

use Caffeinated\Shinobi\Models\Permission;
use Illuminate\Database\Seeder;

class PermisosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*1-5*/
        // Admin
        Permission::create([
            'name'        => 'Index Administrador',
            'slug'        => 'administrador.index',
            'description' => 'Lista y navegar todos los administradores del sistema',
        ]);

        Permission::create([
            'name'        => 'Mostrar Administrador',
            'slug'        => 'administrador.show',
            'description' => 'Mostrar el registro de un User',
        ]);

        Permission::create([
            'name'        => 'Crear Administrador',
            'slug'        => 'administrador.create',
            'description' => 'Crear registro de un User',
        ]);

        Permission::create([
            'name'        => 'Editar Administrador',
            'slug'        => 'administrador.edit',
            'description' => 'Editar el registro de un User',
        ]);
        Permission::create([
            'name'        => 'Eliminar Administrador',
            'slug'        => 'administrador.destroy',
            'description' => 'Eliminar el registro de un User',
        ]);

        /*6-10*/
        // Dentista
        Permission::create([
            'name'        => 'Index dentista',
            'slug'        => 'dentista.index',
            'description' => 'Lista y navegar todos los dentistas del sistema',
        ]);
        Permission::create([
            'name'        => 'Mostrar dentista',
            'slug'        => 'dentista.show',
            'description' => 'Mostrar el registro de un Dentista',
        ]);
        Permission::create([
            'name'        => 'Crear dentista',
            'slug'        => 'dentista.create',
            'description' => 'Crear registro de un Dentista',
        ]);
        Permission::create([
            'name'        => 'Editar dentista',
            'slug'        => 'dentista.edit',
            'description' => 'Editar el registro de un Dentista',
        ]);
        Permission::create([
            'name'        => 'Eliminar dentista',
            'slug'        => 'dentista.destroy',
            'description' => 'Eliminar el registro de un Dentista',
        ]);

        // Asistente
        // 11-15
        Permission::create([
            'name'        => 'Index asistente',
            'slug'        => 'asistente.index',
            'description' => 'Lista y navegar todos los asistente del sistema',
        ]);
        Permission::create([
            'name'        => 'Mostrar asistente',
            'slug'        => 'asistente.show',
            'description' => 'Mostrar el registro de un asistente',
        ]);
        Permission::create([
            'name'        => 'Crear asistente',
            'slug'        => 'asistente.create',
            'description' => 'Crear registro de un asistente',
        ]);
        Permission::create([
            'name'        => 'Editar asistente',
            'slug'        => 'asistente.edit',
            'description' => 'Editar el registro de un Dentista',
        ]);
        Permission::create([
            'name'        => 'Eliminar asistente',
            'slug'        => 'asistente.destroy',
            'description' => 'Eliminar el registro de un asistente',
        ]);

        // Clinicas
        // 16-20
        Permission::create([
            'name'        => 'Index clinica',
            'slug'        => 'clinica.index',
            'description' => 'Lista y navegar todos los clinicas del sistema',
        ]);
        Permission::create([
            'name'        => 'Mostrar clinica',
            'slug'        => 'clinica.show',
            'description' => 'Mostrar el registro de una clinica',
        ]);
        Permission::create([
            'name'        => 'Crear clinica',
            'slug'        => 'clinica.create',
            'description' => 'Crear registro de una clinica',
        ]);
        Permission::create([
            'name'        => 'Editar clinica',
            'slug'        => 'clinica.edit',
            'description' => 'Editar el registro de una clinica',
        ]);
        Permission::create([
            'name'        => 'Eliminar clinica',
            'slug'        => 'clinica.destroy',
            'description' => 'Eliminar el registro de una clinica',
        ]);

        //Pacientes
        Permission::create([
            'name'        => 'Index Paciente',
            'slug'        => 'paciente.index',
            'description' => 'Ver listado de pacientes',
        ]);
        Permission::create([
            'name'        => 'Ver Paciente',
            'slug'        => 'paciente.view',
            'description' => 'Ver informacion del paciente',
        ]);
        Permission::create([
            'name'        => 'Crear Paciente',
            'slug'        => 'paciente.create',
            'description' => 'Agregar paciente',
        ]);
        Permission::create([
            'name'        => 'Editar Paciente',
            'slug'        => 'paciente.edit',
            'description' => 'Modificar datos del paciente',
        ]);

        //Citas
        Permission::create([
            'name'        => 'Index Cita',
            'slug'        => 'cita.index',
            'description' => 'Ver calendario de citas',
        ]);
        Permission::create([
            'name'        => 'Ver Cita',
            'slug'        => 'cita.view',
            'description' => 'Ver detalles de cita',
        ]);
        Permission::create([
            'name'        => 'Crear Cita',
            'slug'        => 'cita.create',
            'description' => 'Agregar cita',
        ]);
        Permission::create([
            'name'        => 'Editar Cita',
            'slug'        => 'cita.edit',
            'description' => 'Modificar datos de cita',
        ]);

        //Roles y permisos
        Permission::create([
            'name'        => 'Index rol/permiso',
            'slug'        => 'rol.index',
            'description' => 'Ver roles y permisos',
        ]);
        Permission::create([
            'name'        => 'Ver rol/permiso',
            'slug'        => 'roles.view',
            'description' => 'Ver detalles de rol',
        ]);
        Permission::create([
            'name'        => 'Crear rol/permiso',
            'slug'        => 'rol.create',
            'description' => 'Agregar rol',
        ]);
        Permission::create([
            'name'        => 'Editar rol/permiso',
            'slug'        => 'rol.edit',
            'description' => 'Modificar datos de rol',
        ]);
    }
}
