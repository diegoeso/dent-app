function get_from_ajax(url, data = null, type = 'modal', extra = '') {
	$('#loader-dent').css('display', 'flex');
	$.get(baseuri + url, ({
		id: data
	}), function(rs) {
		if (rs.success) {
			$('#loader-dent').css('display', 'none');
			switch (type) {
				case 'modal':
					$('#body_gral_modal').html(rs.content);
					$('#title_gral_mdl').text(rs.title);
					if (rs.action) {
						$('#accept_button_modal').text(rs.button_text);
						$('#accept_button_modal').show();
						$('#accept_button_modal').attr('onclick', rs.action);
					} else $('#accept_button_modal').hide();
					setTimeout(function() {
						$('#gral_modal_dnt-app').modal('show');
						if (extra != '') window[extra]((rs.extra != 'undefined') ? rs.extra : '');
					}, 500);
					break;
				case 'another_script':
					if (extra != '') window[extra]((rs.extra != 'undefined') ? rs.extra : '');
					break;
			}
		} else {
			$('#loader-dent').css('display', 'none');
			error_request(rs.message);
		}
	}).fail(function() {
		$('#loader-wrapper').css('display', 'none');
		error_request();
	});
}

function load_info_post(url, data, type = 'data_table', extra = '') {
	$('#loader-dent').css('display', 'flex');
	var data_post = new FormData($("#" + data)[0]);
	//data_post.append('type', submit_type);
	$.ajax({
		type: "post",
		url: baseuri + url,
		processData: false,
		contentType: false,
		dataType: "json",
		data: data_post,
		success: function(isResultAjax) {
			if (isResultAjax.status) {
				$('#' + data).trigger("reset");
				//$('.tabs').tabs('select','test1');
				$('#loader-dent').css('display', 'none');
				/*if(type == 'create'){
					var row = data_table.row.add(isResultAjax.new_data).draw();
					row.nodes().to$().attr('id_row_table', isResultAjax.id);
				}
				else{
					data_table.row("[id_row_table='"+isResultAjax.id+"']").data(isResultAjax.new_data).draw();
					$("#modal1").modal('close');
				}*/
				switch (type) {
					case 'data_table':
						datable.ajax.reload();
						break;
					case 'another_script':
						if (extra != '') window[extra]((isResultAjax.extra != 'undefined') ? isResultAjax.extra : '');
						break;
				}
				$('#gral_modal_dnt-app').modal('hide');
				success_request(isResultAjax.message);
			} else {
				$('#loader-dent').css('display', 'none');
				error_request(isResultAjax.message);
			}
		},
		error: function(data) {
			$('#loader-dent').css('display', 'none');
			error_request();
		}
	});
}

function delete_data_gral(url, data, type = 'another_script', extra_function = false) {
	swal({
		title: '¡Advertencia!',
		text: "¿Estas seguro de eliminar el dato?",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#0CC27E',
		cancelButtonColor: '#FF586B',
		confirmButtonText: 'Eliminar',
		cancelButtonText: 'Cancelar',
		confirmButtonClass: 'btn btn-success mr-5',
		cancelButtonClass: 'btn btn-danger',
		buttonsStyling: false
	}).then(function() {
		$('#loader-dent').css('display', 'flex');
		$.get(baseuri + url, {
			data: data
		}, function(isResultAjax) {
			$('#loader-dent').css('display', 'none');
			if (isResultAjax.status) {
				switch (type) {
					case 'another_script':
						if (extra_function) window[extra_function]((isResultAjax.extra != 'undefined') ? isResultAjax.extra : '');
						break;
					default:
						break;
				}
				success_request('El registro ha sido eliminado correctamente');
			} else {
				$('#loader-dent').css('display', 'none');
				error_request();
			}
		}).fail(function(data) {
			$('#loader-dent').css('display', 'none');
			switch (data.status) {
				case 404:
					error_request("Error en el sitio web. Por favor consulta al administrador");
					break;
				case 403:
					error_request("Parece ser que no cuentas con permisos necesarios para esta acción. Por favor consulta a tu administrador.");
					break;
				case 401:
				case 419:
					Swal.fire({
						title: 'Upsss..',
						text: "Tu sesión ha caducado. Por favor, vuelve a ingresar para realizar esta acción",
						icon: 'error',
						showCancelButton: false,
						confirmButtonColor: '#075800',
						confirmButtonText: 'Iniciar Sesión',
					}).then((result) => {
						if (result.value) {
							window.location.href = get_url('/login');
						}
					});
					break;
				default:
					error_request();
					break;
			}
		});
	});
}

function search_cp(complement = '') {
	//var complement = update ? '_update' : '';
	var cp = $("#cp_code" + complement).val();
	$("#edo_select_cp" + complement).val('');
	$("#del_select_cp" + complement).val('');
	clear_cp(complement);
	if (cp.length < 5) return 0;
	$('#loader-dent').css('display', 'flex');
	$.get(baseuri + 'Config/search_cp', {
		data: cp
	}, function(isResultAjax) {
		$('#loader-dent').css('display', 'none');
		if (isResultAjax.status_proccess == 'success') {
			$("#col_select_cp" + complement).html(isResultAjax.cp);
			$("#edo_select_cp" + complement).val(isResultAjax.edo);
			$("#del_select_cp" + complement).val(isResultAjax.del);
		} else {
			error_request(isResultAjax.message);
		}
	}).fail(function() {
		$('#loader-dent').css('display', 'none');
		error_request();
	});
}

function clear_cp(update = '') {
	$("#col_select_cp" + update).html("<option value='' disabled selected>Ingresa C.P.</option>");
}

function error_request(message = "Error al obtener información") {
	/*Swal.fire({
	    position: 'top-end',
	    icon: 'error',
	    title: 'Upss..',
	    text: message,
	    showConfirmButton: false,
	    timer: 1500
	});*/
	swal('Upss..', message, 'error');
}

function success_request(message = "La solicitud ha sido realizada") {
	/*Swal.fire({
	    position: 'top-end',
	    icon: 'success',
	    title: '¡Listo!',
	    text: message,
	    showConfirmButton: false,
	    timer: 1500
	});*/
	swal({
		title: '¡Hecho!',
		html: message,
		type: 'success',
		timer: 4000
	});
}

function redirect_new_paciente(id) {
	window.location.href = baseuri + 'pacientes/view/' + id + '?new=1';
}

function function_password() {
	var text = $('#nombre_add').val();
	$('#password').val(text.replace(' ', moment().format('ss')).toLowerCase());
}