function get_tratamientos_client(id) {
	$("#main-tratamientos-lst-client").hide();
	$("#load-info-serv").fadeIn(500);
	$.get(baseuri + "tratamientos/listar", ({
		id: id
	}), function(rs) {
		$("#load-info-serv").hide();
		if (rs.success) {
			$('#main-tratamientos-lst-client').html(rs.content);
			$("#main-tratamientos-lst-client").fadeIn(500);
		} else {
			error_request(rs.message);
		}
	}).fail(function() {
		$("#load-info-serv").hide();
		error_request();
	});
}

function set_new_tratamiento(data) {
	console.log(data);
	var text = "<div class=\"mail-item\" onclick=\"get_tratamientos_client('" + data.id + "');\" id=\"option-trtmnt_" + data.id + "\">";
	text += "<div class=\"col-xs-6 details\"><span class=\"name text-muted\">" + data.titulo + "</span>";
	text += "<p class=\"m-0\">" + data.tratamiento + "</p>";
	text += "</div>";
	text += "<div class=\"col-xs-3 date\"><span class=\"text-muted\">" + data.created + "</span></div>";
	text += "</div>";
	$("#div_tratamientos_paciente_list").prepend(text);
	get_tratamientos_client(data.id);
}

function set_tratamientos_list_to_add(data) {
	$("#tratamientos_list_slct").html(data);
}

function set_new_historial_tr(data) {
	var result = "<div class=\"card\" id=\"card_hstrl-" + data.id + "\"+>";
	result += "<div class=\"card-header header-elements-inline\">";
	result += "<h6 class=\"card-title ul-collapse__icon--size ul-collapse__right-icon mb-0\"><a class=\"text-default collapsed\" data-toggle=\"collapse\" href=\"#accordion-tratamiento-client-" + data.id + "\" aria-expanded=\"false\" id=\"title-accordion-hstrl-" + data.id + "\">" + data.titulo + ".. <small>(" + data.created + ")</small></a></h6>";
	result += "</div>";
	result += "<div class=\"collapse\" id=\"accordion-tratamiento-client-" + data.id + "\" data-parent=\"#tratamientos-client\">";
	result += "<div class=\"card-body\">";
	result += "<h4 class=\"text-center\">" + data.created + "</h4>";
	result += "<div class='row'>";
	result += "<div class='col-md-6'>";
	result += "<h6 class=\"text-warning\">Estatus</h6>";
	result += "<p id=\"estatus-accordion-hstrl-" + data.id + "\">" + data.estatus + "</p>";
	result += "</div>";
	result += "<div class='col-md-6'>";
	result += "<h6 class=\"text-warning\">Cantidad:</h6>";
	result += "<p id=\"cant-accordion-hstrl-" + data.id + "\">$" + data.cantidad + " MXN</p>";
	result += "</div>";
	result += "</div>";
	result += "<h6 class=\"text-warning\">Medicación</h6>";
	result += "<p id=\"medicacion-accordion-hstrl-" + data.id + "\">" + data.medicacion + "</p>";
	result += "<h6 class=\"text-warning\">Observaciones</h6>";
	result += "<p id=\"observ-accordion-hstrl-" + data.id + "\">" + data.observ + "</p>";
	result += "<div class='row'>";
	result += "</div>";
	result += "</div>";
	result += "</div>";
	result += "</div>";
	$("#tratamientos-client").prepend(result);
	$("#mnt-cbrd-trtmnt").text(data.cobrado);
}

function delete_historial_client(data) {
	$("#card_hstrl-" + data.id).remove();
	$("#mnt-cbrd-trtmnt").text(data.cobrado);
}

function edit_historial_client(data) {
	$("#title-accordion-hstrl-" + data.id).text(data.titulo + ".. (" + data.created + ")");
	$("#estatus-accordion-hstrl-" + data.id).text(data.estatus);
	$("#cant-accordion-hstrl-" + data.id).text("$" + data.cantidad + " MXN");
	$("#medicacion-accordion-hstrl-" + data.id).html(data.medicacion);
	$("#observ-accordion-hstrl-" + data.id).html(data.observ);
	$("#mnt-cbrd-trtmnt").text(data.cobrado);
}

function edit_tratamiento_client(data) {
	get_tratamientos_client(data.id);
	$("#trtmnt-service_cat_" + data.id).text(data.cat);
	$("#trtmnt-service_tr_" + data.id).text(data.tr);
	$("#trtmnt-service_status_" + data.id).text(data.status);
}

function delete_tratamiento_client(data) {
	$("#option-trtmnt_" + data).remove();
	$('#main-tratamientos-lst-client').html("<h5 class='text-center'>Selecciona el tratamiento a visualizar</h5>");
}

function add_new_cita(data) {
	text = '<tr>';
	text += '<td>' + data.fecha + '</td>';
	text += '<td>' + data.inicio + '</td>';
	text += '<td>' + data.fin + '</td>';
	text += '<td>' + data.status + '</td>';
	text += '</tr>';
	$("#pacientes_cts-table tbody").prepend(text);
}

function refresh_paciente_info(data) {
	$("#about").html(data);
}