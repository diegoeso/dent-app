import VueRouter from 'vue-router'
// Pages
import Home from './pages/home'
import Citas from './pages/citas'
// Routes
const routes = [{
  path: '/home-paciente',
  name: 'home',
  component: Home,
}, {
  path: '/citas-paciente',
  name: 'citas',
  component: Citas,
}]
const router = new VueRouter({
  history: true,
  mode: 'history',
  routes,
})
export default router