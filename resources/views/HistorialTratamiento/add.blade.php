<form id="agrega_historial_frm">
    <input type="hidden" name="_pcnt_trtmnt" value="{{$id}}">
    <div class="row">
        <div class="col-md-12 form-group mb-3">
            <label for="observaciones">
                Medicación
            </label>
            <textarea class="form-control" name="medicamento" placeholder="Agrega los medicamentos del paciente"></textarea>
        </div>
        <div class="col-md-12 form-group mb-3">
            <label for="observaciones">
                Observaciones
            </label>
            <textarea class="form-control" name="descripcion" placeholder="Agrega comentarios extras"></textarea>
        </div>
        <div class="col-md-6 form-group mb-3">
            <label for="monto">
                Por cobrar
            </label>
            <input class="form-control" id="monto" type="number" name="monto" placeholder="Monto cobrado">
        </div>
        <div class="col-md-6 form-group mb-3">
            <label for="nombre_add">
                Estatus
            </label>
            <select class="form-control" name="estatus">
                <option value="" disabled selected>Selecciona Estatus</option>
                <option value="Finalizado">Finalizado</option>
                <option value="En Proceso">En Proceso</option>
                <option value="Por Pagar">Por Pagar</option>
            </select>
        </div>
    </div>
</form>
