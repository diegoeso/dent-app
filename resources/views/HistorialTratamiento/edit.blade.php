<form id="editar_historial_frm">
    <input type="hidden" name="_hstrl" value="{{$historial->id}}">
    <div class="row">
        <div class="col-md-12 form-group mb-3">
            <label for="observaciones">
                Medicación
            </label>
            <textarea class="form-control" name="medicamento" placeholder="Agrega los medicamentos del paciente">{{$historial->medicamento}}</textarea>
        </div>
        <div class="col-md-12 form-group mb-3">
            <label for="observaciones">
                Observaciones
            </label>
            <textarea class="form-control" name="descripcion" placeholder="Agrega comentarios extras">{{$historial->descripcion}}</textarea>
        </div>
        <div class="col-md-6 form-group mb-3">
            <label for="monto">
                Por cobrar
            </label>
            <input class="form-control" id="monto" value="{{$historial->monto_a_pagar}}" type="number" name="monto" placeholder="Monto cobrado">
        </div>
        <div class="col-md-6 form-group mb-3">
            <label for="nombre_add">
                Estatus
            </label>
            <select class="form-control" name="estatus">
                <option value="" disabled selected>Selecciona Estatus</option>
                <option value="Finalizado" {{$historial->estatus == 'Finalizado' ? 'selected' : ''}}>Finalizado</option>
                <option value="En Proceso" {{$historial->estatus == 'En Proceso' ? 'selected' : ''}}>En Proceso</option>
                <option value="Por Pagar" {{$historial->estatus == 'Por Pagar' ? 'selected' : ''}}>Por Pagar</option>
            </select>
        </div>
    </div>
</form>
