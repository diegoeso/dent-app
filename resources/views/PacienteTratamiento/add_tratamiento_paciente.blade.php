<form id="agrega_tratamiento_frm">
    <input name="_pcnt" type="hidden" value="{{$id}}"/>
    <div class="row">
        <div class="col-md-6 form-group mb-3">
            <label for="nombre_add">
                Operatoria
            </label>
            <select class="form-control" name="categoria" onchange="get_from_ajax('Tratamiento/get_all_by_id', this.value,'another_script', 'set_tratamientos_list_to_add')">
                <option disabled="" selected="" value="">
                    Selecciona Operatoria
                </option>
                @foreach($cat_tratamientos as $k => $categoria)
                <option value="{{$categoria->id}}">
                    {{$categoria->nombre}}
                </option>
                @endforeach
            </select>
        </div>
        <div class="col-md-6 form-group mb-3">
            <label for="nombre_add">
                Categoria
            </label>
            <select class="form-control" id="tratamientos_list_slct" name="tratamiento">
                <option disabled="" selected="" value="">
                    Selecciona Categoria
                </option>
            </select>
        </div>
        <div class="col-md-12 form-group mb-3">
            <label for="observaciones">
                Sintomatologia
            </label>
            <textarea class="form-control" name="sintomas" placeholder="Describe los sintomas del paciente">
            </textarea>
        </div>
        <div class="col-md-12 form-group mb-3">
            <label for="observaciones">
                Tratamiento
            </label>
            <textarea class="form-control" name="descripcion" placeholder="Describe el diagnostico">
            </textarea>
        </div>
        <div class="col-md-12 form-group mb-3">
            <label for="presupuesto">
                Presupuesto
            </label>
            <input class="form-control" name="presupuesto" placeholder="Cantidad en pesos" type="number">
            </input>
        </div>
    </div>
</form>
