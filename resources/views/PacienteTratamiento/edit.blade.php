<form id="editar_tratamiento_frm">
    <input type="hidden" name="_trtmnt_pcnt" value="{{$tratamiento->id}}">
    <div class="row">
        <div class="col-md-6 form-group mb-3">
            <label for="nombre_add">
                Operatoria
            </label>
            <select class="form-control" name="categoria" onchange="get_from_ajax('Tratamiento/get_all_by_id', this.value,'another_script', 'set_tratamientos_list_to_add')">
                <option value="" disabled selected>Selecciona Operatoria</option>
                @foreach($cat_tratamientos as $k => $categoria)
                <option value="{{$categoria->id}}" {{($tratamiento->tratamiento->categoria_id == $categoria->id) ? 'selected' : ''}}>{{$categoria->nombre}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-6 form-group mb-3">
            <label for="nombre_add">
                Categoria
            </label>
            <select class="form-control" name="tratamiento" id="tratamientos_list_slct">
                <option value="" disabled selected>Selecciona Categoria</option>
                @foreach($tratamientos as $k => $tr)
                <option value="{{$tr->id}}" {{($tratamiento->tratamiento_id == $tr->id) ? 'selected' : ''}}>{{$tr->nombre}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-12 form-group mb-3">
            <label for="observaciones">
                Sintomatologia
            </label>
            <textarea class="form-control" name="sintomas" placeholder="Describe los sintomas del paciente">{{ $tratamiento->sintomas }}</textarea>
        </div>
        <div class="col-md-12 form-group mb-3">
            <label for="observaciones">
                Tratamiento
            </label>
            <textarea class="form-control" name="descripcion" placeholder="Describe el diagnostico">{{ $tratamiento->descripcion }}</textarea>
        </div>
        <div class="col-md-6 form-group mb-3">
            <label for="presupuesto">
                Presupuesto
            </label>
            <input type="number" name="presupuesto" placeholder="Cantidad en pesos" class="form-control" value="{{ $tratamiento->presupuesto }}">
        </div>
        <div class="col-md-6 form-group mb-3">
            <label for="presupuesto">
                Estatus
            </label>
            <select class="form-control" name="estatus">
                <option value="" disabled selected>Selecciona Categoria</option>
                <option value="En Proceso" {{($tratamiento->estatus == 'En Proceso') ? 'selected' : ''}}>En Proceso</option>
                <option value="Finalizado" {{($tratamiento->estatus == 'Finalizado') ? 'selected' : ''}}>Finalizado</option>
                <option value="Cancelado" {{($tratamiento->estatus == 'Cancelado') ? 'selected' : ''}}>Cancelado</option>
            </select>
        </div>
    </div>
</form>
