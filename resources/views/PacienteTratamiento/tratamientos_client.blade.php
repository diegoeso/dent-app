<div class="inbox-topbar box-shadow-1 perfect-scrollbar rtl-ps-none pl-3" data-suppress-scroll-y="true">
    <div class="d-flex">
        @if($tratamiento->estatus != 'Finalizado')
        <a class="link-icon mr-3" onclick="get_from_ajax('HistorialTratamiento/add_from_ajax', '{{$tratamiento->id}}')">
            <i class="icon-regular i-Add">
            </i>
            Agregar
        </a>
        <a class="link-icon mr-3" onclick="get_from_ajax('PacienteTratamiento/edit_from_ajax', '{{$tratamiento->id}}')">
            <i class="icon-regular i-Pen-2">
            </i>
            Editar
        </a>
        <a class="link-icon mr-3" onclick="delete_data_gral('PacienteTratamiento/delete','{{$tratamiento->id}}','another_script', 'delete_tratamiento_client')">
            <i class="icon-regular i-Close-Window">
            </i>
            Eliminar
        </a>
        @endif
    </div>
</div>
<!-- EMAIL DETAILS-->
<div class="inbox-details perfect-scrollbar rtl-ps-none" data-suppress-scroll-x="true" style="padding: 0.5rem 1rem;">
    <h4 class="mb-3 text-center">
        {{$tratamiento->tratamiento->categoria->nombre}}
        <br>
            <small>
                ({{$tratamiento->tratamiento->nombre}})
            </small>
        </br>
    </h4>
    <div class="row no-gutters">
        <div class="col-xs-12 col-md-6">
            <p class="m-0">
                {{$tratamiento->estatus}}
            </p>
            <p class="m-0">
                {{$tratamiento->created_at}}
            </p>
        </div>
        <div class="col-xs-12 col-md-6">
            <p class="m-0 text-12">
                Presupuesto ${{$tratamiento->presupuesto}} MXN
            </p>
            <p class="m-0 text-12">
                Monto cobrado $
                <span id="mnt-cbrd-trtmnt">
                    {{$pagado}}
                </span>
                MXN
            </p>
        </div>
    </div>
    <h5 class="mb-3 text-primary">
        Sintomatorio
    </h5>
    <div>
        <p>
            {!! nl2br($tratamiento->sintomas) !!}
        </p>
    </div>
    <h5 class="mb-3 text-primary">
        Tratamiento
    </h5>
    <div>
        <p>
            {!! nl2br($tratamiento->descripcion) !!}
        </p>
    </div>
    <div class="accordion" id="tratamientos-client">
        @foreach($tratamiento->historial as $key => $historial)
        <div class="card" id="card_hstrl-{{$historial->id}}">
            <div class="card-header header-elements-inline">
                <h6 class="card-title ul-collapse__icon--size ul-collapse__right-icon mb-0">
                    <a aria-expanded="false" class="text-default collapsed" data-toggle="collapse" href="#accordion-tratamiento-client-{{$historial->id}}" id="title-accordion-hstrl-{{$historial->id}}">
                        {{substr($historial->descripcion,0,30)}}..
                        <small>
                            ({{$historial->created_at}})
                        </small>
                    </a>
                </h6>
            </div>
            <div class="collapse" data-parent="#tratamientos-client" id="accordion-tratamiento-client-{{$historial->id}}">
                <div class="card-body">
                    <h4 class="text-center">
                        {{$historial->created_at}}
                    </h4>
                    <div class="row">
                        <div class="col-md-6">
                            <h6 class="text-warning">
                                Estatus
                            </h6>
                            <p id="estatus-accordion-hstrl-{{$historial->id}}">
                                {{$historial->estatus}}
                            </p>
                        </div>
                        <div class="col-md-6">
                            <h6 class="text-warning">
                                Cantidad:
                            </h6>
                            <p id="cant-accordion-hstrl-{{$historial->id}}">
                                ${{$historial->monto_a_pagar}} MXN
                            </p>
                        </div>
                    </div>
                    <h6 class="text-warning">
                        Medicación
                    </h6>
                    <p id="medicacion-accordion-hstrl-{{$historial->id}}">
                        {!! nl2br($historial->medicamento) !!}
                    </p>
                    <h6 class="text-warning">
                        Observaciones
                    </h6>
                    <p id="observ-accordion-hstrl-{{$historial->id}}">
                        {!! nl2br($historial->descripcion) !!}
                    </p>
                    <!-- <div class="row"> -->
                    @if($tratamiento->estatus != 'Finalizado')
                    <div class="col-12 text-center">
                        <a class="mr-3" onclick="get_from_ajax('HistorialTratamiento/edit_from_ajax', '{{$historial->id}}')">
                            <i class="icon-regular i-Pen-2">
                            </i>
                            Editar
                        </a>
                        <a class="mr-3" onclick="delete_data_gral('HistorialTratamiento/delete','{{$historial->id}}','another_script', 'delete_historial_client')">
                            <i class="icon-regular i-Close-Window">
                            </i>
                            Eliminar
                        </a>
                    </div>
                    @endif
                    <!-- </div> -->
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>