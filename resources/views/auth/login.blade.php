<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8"/>
        <title>
            Document
        </title>
        <meta charset="utf-8"/>
        <meta content="width=device-width,initial-scale=1" name="viewport"/>
        <meta content="ie=edge" http-equiv="X-UA-Compatible"/>
        <title>
            Iniciar sesión
        </title>
        <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,400i,600,700,800,900" rel="stylesheet"/>
        <link href="{{asset('dist-assets/css/themes/lite-purple.css')}}" rel="stylesheet">
        </link>
    </head>
    <body>
        <div class="auth-layout-wrap" style="background-image: url({{asset('dist-assets/images/chair-2589771_1920.jpg')}})">
            <div class="auth-content">
                <div class="card o-hidden">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="p-4">
                                <div class="auth-logo text-center mb-4">
                                    <img alt="" src="{{asset('dist-assets/images/logo.png')}}"/>
                                </div>
                                <h1 class="mb-3 text-18">
                                    Bienvenido
                                </h1>
                                @if(Session::has('estatus'))
                                <div class="alert alert-danger">
                                    {{Session::get('estatus')}}
                                </div>
                                @endif
                                <form action="{{ route('login') }}" method="post">
                                    @csrf
                                    <div class="form-group">
                                        <label for="email">
                                            Usuario
                                        </label>
                                        <input class="form-control form-control-rounded" id="email" name="email" type="text" value="{{ old('email') }}">
                                        </input>
                                    </div>
                                    <div class="form-group">
                                        <label for="password">
                                            Contraseña
                                        </label>
                                        <input class="form-control form-control-rounded" id="password" name="password" type="password">
                                        </input>
                                    </div>
                                    <button class="btn btn-rounded btn-primary mt-2 text-center">
                                        Ingresar
                                    </button>
                                </form>
                                <!--<div class="mt-3 text-center"><a class="text-muted" href="forgot.html">
                                <u>Forgot Password?</u></a></div>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
