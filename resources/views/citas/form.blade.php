<div class="box-body">
    <div class="row">
        <div class="col-md-12 form-group">
            <label class="switch switch-primary">
                <span>
                    Paciente Nuevo
                </span>
                <input id="cl_nuevo_citas" name="nuevo" type="checkbox">
                    <span class="slider">
                    </span>
                </input>
            </label>
        </div>
        <div class="col-md-6" id="pcnt_list-slct">
            <div class="form-group">
                {!! Form::label('area_id','Paciente') !!}
                <select class="form-control select2" id="paciente_id" name="paciente_id" style="width: 100%">
                </select>
                <span class="label label-danger ">
                    <strong class="error-paciente errors">
                    </strong>
                </span>
            </div>
        </div>
        <div class="col-md-6" id="pcnt_new-inpt" style="display: none">
            <div class="form-group">
                <label for="">
                    Paciente
                </label>
                <input class="form-control" id="paciente_nombre" name="paciente_new" placeholder="Nombre(s)">
                </input>
                <span class="label label-danger ">
                    <strong class="error-paciente errors">
                    </strong>
                </span>
            </div>
        </div>
        <div class="col-md-6" id="fecha-Inicio">
            <div class="form-group">
                <label for="">
                    Fecha de inicio
                </label>
                <input class="form-control text-capitalize" id="fechaInicio" name="fecha_inicio" placeholder="Fecha de finalización" required="" type="date">
                    <span class="label label-danger">
                        <strong class="error-telefono errors">
                            {{ $errors->first('fechaInicio') }}
                        </strong>
                    </span>
                </input>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>
                    Hora de inicio
                </label>
                <input class="form-control text-capitalize" id="horaInicio" name="hora_inicio" type="time">
                    <span class="label label-danger">
                        <strong class="error-telefono errors">
                        </strong>
                    </span>
                </input>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="">
                    Hora de finalización
                </label>
                <input class="form-control text-capitalize" id="horaFin" name="hora_fin" type="time">
                    <span class="label label-danger">
                        <strong class="error-telefono errors">
                        </strong>
                    </span>
                </input>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label for="">
                    Observaciones
                </label>
                <textarea class="form-control" cols="30" id="comentarios" name="comentarios" placeholder="Observaciones" rows="3">
                </textarea>
                <span class="label label-danger">
                    <strong class="error-telefono errors">
                    </strong>
                </span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="">
                    Estatus
                </label>
                <select class="form-control" id="status" name="status">
                    <option value="0">
                        Pendiente
                    </option>
                    <option value="1">
                        En proceso
                    </option>
                    <option value="2">
                        Cancelada
                    </option>
                    <option value="3">
                        Finalizada
                    </option>
                    <option value="4">
                        Re programanda
                    </option>
                </select>
                <span class="label label-danger">
                    <strong class="error-status errors">
                    </strong>
                </span>
            </div>
        </div>
    </div>
</div>