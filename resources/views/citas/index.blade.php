@extends('layouts.app_dent')
@section('titulo', 'Citas programadas')
@section('contenido')
<div class="row mb-4">
    <div class="col-md-12 mb-4">
        <div class="card text-left">
            <div class="card-header d-flex align-items-center">
                <h3 class="w-50 float-left card-title m-0">
                    Agendar cita
                </h3>
                {{--
                <div class="col-md-8">
                    <div class="text-right w-50 float-right show">
                        <button class="btn btn-info btn-sm" id="btnModal" type="button">
                            <i class="i-Add">
                            </i>
                            Nuevo
                        </button>
                    </div>
                </div>
                --}}
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <div id="calendar">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="modalCitas" role="dialog" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('citas.store') }}" id="altaCitas" method="POST" name="altaCitas">
                {{ csrf_field() }}
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-title">
                        Registrar cita
                    </h5>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                        <span aria-hidden="true">
                            ×
                        </span>
                    </button>
                </div>
                <div class="modal-body">
                    @include('citas.form')
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" id="btnCerrarModal" type="button">
                        Cancelar
                    </button>
                    <button class="btn btn-primary ml-2" id="btnGuardar" type="submit">
                        Guardar
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Modal -->
<div aria-hidden="true" aria-labelledby="exampleModalLabel1" class="modal fade" id="modalMostrar" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    Detalles de cita
                </h5>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                    <span aria-hidden="true">
                        ×
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div aria-labelledby="about-tab" class="tab-pane fade active show" id="about" role="tabpanel">
                    <div class="row mb-2" id="s_paciente">
                        <div class="col-md-8 col-8 col-sm-8 col-xs-8">
                            <h2 class="text-primary mb-1">
                            </h2>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <span class="float-right">
                                Updated daily
                            </span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-4" id="s_fecha">
                            <div class="mb-4">
                                <p class="text-primary mb-1">
                                    Fecha
                                </p>
                                <span>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-4 col-4" id="s_horario">
                            <div class="mb-4">
                                <p class="text-primary mb-1">
                                    Horario
                                </p>
                                <span>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-4 col-4" id="s_status">
                            <div class="mb-4">
                                <p class="text-primary mb-1">
                                    Estatus
                                </p>
                                <span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-12" id="s_comentarios">
                            <div class="mb-4">
                                <p class="text-primary mb-1">
                                    Observaciones
                                </p>
                                <span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-12 text-center">
                            <button class="btn btn-primary" id="bntEditar" type="button">
                                Modificar cita
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" data-dismiss="modal" type="button">
                    Cerrar
                </button>
            </div>
        </div>
    </div>
</div>
@endsection


@section('scripts')
<script src="{{ asset('js/system/pacientes.js') }}">
</script>
<script>
    $(document).ready(function() {
        var id_dentista = '';
      
        calendario(id_dentista);
        pintar_pacientes();
        $('#paciente_id').select2({
            width: '100%',
            dropdownParent: $('#modalCitas')
        });


        $('#btnModal').click(function(event) {
            event.preventDefault();
            $('#altaCitas').trigger('reset');
            $("#altaCitas").attr('action', '{{ route('citas.store') }}');  
            $("#altaCitas").attr('method', 'POST');
            $('#modalCitas #modal-title').html('Nuevo Registro');   
            $('#modalCitas').modal('show'); 
        });

        $("#altaCitas").submit(function (e) {
            e.preventDefault();
            $.ajax({
                type: $(this).attr('method'),
                url: $(this).attr('action'),
                data: $("#altaCitas").serialize(),
                dataType: 'json',
                success: function (res) {
                    if (res.success == true) {
                        $(".errors").html('');
                        $('#altaCitas').trigger('reset');
                        $('#modalCitas').modal('hide');   
                        
                        swal({
                          type: 'success',
                          title: 'Hecho!',
                          html: '<p>Cita programada para el paciente: '+res.data.paciente+'</p> <p>Fecha: '+moment(res.data.fecha).format('LL')+'</p>',
                          // text: '¡Registro almacenado correctamente!',
                          buttonsStyling: false,
                          confirmButtonClass: 'btn btn-lg btn-success'
                        });
                        $('#calendar').fullCalendar("refetchEvents");
                    }
                }
                , error: function (e) {
                    pintar_errores(e.responseJSON.errors);
                }
            });
        });

        $('#btnCerrarModal').click(function(event) {
            event.preventDefault();
            $('#altaCitas').trigger('reset');
            $(".errors").html('');
            $('#modalCitas').modal('hide'); 
        });

        $("#cl_nuevo_citas").click(function(){
            if($(this).is(":checked")){
                $("#pcnt_list-slct").hide();
                $("#pcnt_new-inpt").fadeIn(200);
            }
            else{
                $("#pcnt_new-inpt").hide();
                $("#pcnt_list-slct").fadeIn(200);
            }
        });

        $('#bntEditar').click(function(event) {
            event.preventDefault();
            var id = $(this).attr('data-id');
            $('#altaCitas').trigger('reset');
            $("#altaCitas").attr('action', 'citas/'+id+'/edit');  
            $("#altaCitas").attr('method', 'GET');
            $('#modalCitas #modal-title').html('Editar Registro');  
            $.ajax({
                url: baseuri + 'citas/'+ id,
                type: 'GET',
                dataType: 'JSON',
                success:function(data) {
                    $('#modalMostrar').modal('hide');
                    $('#modalCitas').modal('show'); 

                    if (data.paciente_id == null) {
                        $('#cl_nuevo_citas').prop('checked', true);
                        $("#pcnt_list-slct").hide();
                        $("#pcnt_new-inpt").fadeIn(200);
                    }
                    else{
                        $("#pcnt_new-inpt").hide();
                        $("#pcnt_list-slct").fadeIn(200);
                    }
                    $('#modalCitas #paciente_nombre').val(data.paciente);
                    $('#modalCitas #fechaInicio').val(data.fecha);
                    $('#modalCitas #horaInicio').val(data.hora_inicio);
                    $('#modalCitas #horaFin').val(data.hora_fin);
                    $('#modalCitas #comentarios').val(data.comentarios);
                    $('#modalCitas #status').val(data.status);


                }
            });
        });
    });

    function calendario(dentista = null) {
        $('#calendar').fullCalendar({
            defaultView: 'agendaWeek',
            navLinks: true,
            lang: 'es',
            minTime: "09:00:00",
            maxTime: "22:00:00",
            hiddenDays: [  ],
            header    : {
                left  : 'prev,next today',
                center: 'title',
                right : 'month,agendaWeek,agendaDay,listWeek'
            },
            buttonText: {
                today: 'Hoy',
                month: 'Mes',
                week : 'Semana',
                day  : 'Dia',
                listWeek :'Lista'
            },
            monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
            monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
            dayNames: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
            dayNamesShort: ['Dom','Lun','Mar','Mié','Jue','Vie','Sáb'],
            events : 'listar-citas',
            // + dentista,
            weekNumbers: true,
            eventLimit: true,
            timeFormat: 'hh(:mm)t',
            nowIndicator: true,
            selectable: true,
            selectHelper: true,
            select: function(start, end) {
                $('#cl_nuevo_citas').prop('checked', false);
                $("#pcnt_new-inpt").hide();
                $("#pcnt_list-slct").fadeIn(200);
                $('#altaCitas').trigger('reset');
                $("#altaCitas").attr('action', '{{ route('citas.store') }}');  
                $("#altaCitas").attr('method', 'POST');
                $('#modalCitas #modal-title').html('Nuevo Registro');   
                $('#modalCitas #fechaInicio').val(moment(start).format('YYYY-MM-DD'));
                var fecha = new Date(end);
                var fin = fecha.setDate(fecha.getDate());
                $('#modalCitas #fechaFin').val(moment(fin).format('YYYY-MM-DD'));
                $('#modalCitas #horaInicio').val(moment(start).format('HH:mm'));
                $('#modalCitas #horaFin').val(moment(end).format('HH:mm'));
                $('#modalCitas').modal('show');
            },
            eventClick: function(calEvent, jsEvent) {
                if (calEvent.url) {
                    return true;
                }else{
                    $.ajax({
                        url: baseuri + 'citas/'+calEvent.id,
                        type: 'GET',
                        dataType: 'JSON',
                        success:function(data) {
                            console.log(data);
                            $('#s_paciente h2').html(data.paciente);
                            $('#s_paciente span').html((data.paciente_id) ? "<a class='btn btn-info btn-sm' href='"+baseuri+"pacientes/view/"+data.paciente_r.id+"'>Ver perfil del paciente</a>" : "<a style='color:white;' class='btn btn-primary btn-sm' onclick=\"get_from_ajax('pacientes/add_view?id_cita="+calEvent.id+"', '"+dentista+"')\">Agregar paciente</a>");
                            $('#s_fecha span').html(moment(data.fecha).format('dddd D [de] MMMM YYYY') );
                            $('#s_horario span').html(moment(data.fecha + ' ' +data.hora_inicio).format('h:mm a') + ' a '+ moment(data.fecha + ' ' +data.hora_fin).format('h:mm a'));
                            $('#s_status span').html(estatus(data.status));
                            $('#s_comentarios span').html(data.comentarios);
                            $('#bntEditar').attr('data-id', data.id);
                            $('#modalMostrar').modal('show');
                        }
                    });
                }
            },
        });
    }
    function pintar_errores(errores = null){
        $(".errors").html('');
        $(".errors").parent().removeClass('has-error');
        $.each(errores, function (k, v) {
            $(".error-" + k).html(v);
            $(".error-" + k).parent().addClass('has-error');
        });
    }

    function pintar_pacientes(){
        $.ajax({
            url: baseuri + 'citas-pacientes',
            type: 'GET',
            dataType: 'json',
            success:function(res){
                $.each(res, function(index, val) {
                     $('#paciente_id').append('<option value="'+val.id+'">'+val.nombre +' '+val.apellidos+'</option>')
                });
                
            }
        });
    }

    function estatus(estatus){
        switch (estatus) {
            case '0':
                return 'Pendiente';
                break;
            case '1':
                return 'En Proceso';
                break;
            case '2':
                return 'Cancelada';
                break;
            case '3':
                return 'Finalizada';
                break;
            case '4':
                return 'Reprogramada';
                break;
            default:
                return "N/A";
                break;
        }
    }

    function set_agnd_id(){

    }
</script>
@endsection
