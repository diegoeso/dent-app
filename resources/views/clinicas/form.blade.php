<div class="row">
    <input id="tipo_rol" name="tipo_rol" type="hidden"/>
    <div class="col-md-6 form-group mb-3 from-group {{ $errors->has('nombre') ? ' has-error' : '' }}" id="div_nombre">
        <label for="firstName1">
            Nombre
        </label>
        <input class="form-control" id="nombre" name="nombre" placeholder="Nombre" type="text">
        </input>
        <span class="text-danger error-nombre errors">
        </span>
    </div>
    <div class="col-md-3 form-group {{ $errors->has('telefono') ? ' has-error' : '' }} mb-3" id="div_telefono">
        <label for="phone">
            Teléfono
        </label>
        <input class="form-control" id="telefono" name="telefono" placeholder="Teléfono">
        </input>
        <span class="text-danger error-telefono errors">
        </span>
    </div>
    <div class="col-md-3 form-group {{ $errors->has('telefono') ? ' has-error' : '' }} mb-3" id="div_telefono_alt">
        <label for="phone">
            Teléfono Alternativo
        </label>
        <input class="form-control" id="telefono_alt" name="telefono_alt" placeholder="Teléfono Alternativo">
        </input>
        <span class="text-danger error-telefono_alt errors">
        </span>
    </div>
</div>
<div class="row">
    <div class="col-md-4 form-group {{ $errors->has('direccion') ? ' has-error' : '' }} mb-3" id="div_cp">
        <label for="">
            Codigo Postal
        </label>
        <input class="form-control" id="cp" maxlength="5" name="cp" onkeypress="if(this.value.length==5) return false;" placeholder="Ingresa tu código postal para buscar tu ubicación." type="number" value="{{ old('postal') }}">
            <span class="text-danger error-cp errors">
            </span>
        </input>
    </div>
    <div class="col-md-8 form-group {{ $errors->has('direccion') ? ' has-error' : '' }} mb-3" id="div_direccion">
        <label for="">
            Dirección
        </label>
        <input class="form-control" id="direccion" name="direccion" placeholder="Dirección" type="text">
        </input>
        <span class="text-danger error-direccion errors">
        </span>
    </div>
    <div class="form-group col-md-4 col-sm-8 col-xs-8" id="div_colonia">
        <label class="" for="direccion">
            Colonia:
        </label>
        <div class="input-group{{ $errors->has('codigo_postal_id') ? ' has-error' : '' }}">
            <select class="form-control" id="colonias" name="codigo_postal_id">
                <option value="">
                    Ingresa tu Código Postal..
                </option>
            </select>
        </div>
        <span class="text-danger error-codigo_postal_id errors">
        </span>
    </div>
    <div class="col-md-4" id="div_municipio">
        <label class="" for="pais">
            Municipio / Delegación:
        </label>
        <div class="input-group">
            <input class="form-control" disabled="" id="municipio" name="municipio" placeholder="" type="text">
            </input>
        </div>
    </div>
    <div class="col-md-4" id="div_estado">
        <label class="" for="provincia">
            Estado:
        </label>
        <div class="input-group">
            <input class="form-control" disabled="" id="estado" name="estado" type="text">
            </input>
        </div>
    </div>
    <div class="col-md-6 form-group {{ $errors->has('dentista_id') ? ' has-error' : '' }} mb-3" id="div_dentista_id">
        <label for="picker1">
            Dentista
        </label>
        <select class="form-control" id="dentista_id" name="dentista_id">
        </select>
        <span class="text-danger error-dentista_id errors">
        </span>
    </div>
</div>