@extends('layouts.app_dent')
@section('titulo', 'Listado de clinicas')
@section('contenido')
<div class="row mb-4">
    <div class="col-md-12 mb-4">
        <div class="card text-left">
            <div class="card-header d-flex align-items-center">
                <h3 class="w-50 float-left card-title m-0">
                    Clinicas registradas
                </h3>
                <div class="col-md-8">
                    <div class="text-right w-50 float-right show">
                        <button class="btn btn-info btn-sm" id="btnModal" type="button">
                            <i class="i-Add">
                            </i>
                            Nuevo
                        </button>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12 table-responsive">
                        <table aria-describedby="example2_info" class="display table table-striped table-bordered dataTable" id="clinicas-table" role="grid" style="width: 100%">
                            <thead>
                                <tr role="row">
                                    <th>
                                        ID
                                    </th>
                                    <th>
                                        Nombre
                                    </th>
                                    <th>
                                        Teléfono
                                    </th>
                                    <th width="80">
                                        Opciones
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="modalClinica" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="{{ route('clinicas.store') }}" id="altaClinica" method="POST" name="altaClinica">
                {{ csrf_field() }}
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-title">
                        Registrar clincia
                    </h5>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                        <span aria-hidden="true">
                            ×
                        </span>
                    </button>
                </div>
                <div class="modal-body">
                    @include('clinicas.form')
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" id="btnCerrarModal" type="button">
                        Cancelar
                    </button>
                    <button class="btn btn-primary ml-2" id="btnGuardar" type="submit">
                        Guardar
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Modal -->
<div aria-hidden="true" aria-labelledby="exampleModalLabel1" class="modal fade" id="modalMostrar" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    Información del registro
                </h5>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                    <span aria-hidden="true">
                        ×
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div aria-labelledby="about-tab" class="tab-pane fade active show" id="about" role="tabpanel">
                    <div class="row">
                        <div class="col-md-6 col-6" id="s_nombre">
                            <div class="mb-4">
                                <p class="text-primary mb-1">
                                    Nombre Completo
                                </p>
                                <span>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-6 col-6" id="s_telefono">
                            <div class="mb-4">
                                <p class="text-primary mb-1">
                                    Teléfono
                                </p>
                                <span>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-6 col-6" id="s_telefono_alt">
                            <div class="mb-4">
                                <p class="text-primary mb-1">
                                    Teléfono Alternativo
                                </p>
                                <span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-12" id="s_direccion">
                            <div class="mb-4">
                                <p class="text-primary mb-1">
                                    Dirección
                                </p>
                                <span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-info" data-dismiss="modal" type="button">
                    Cerrar
                </button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        var rol = '{{Auth::user()->rol}}';
        $('#tipo_rol').val(rol);
        mostrar_form(rol);

        // listar_clinicas();
        listar_dentistas();
        listar_roles();

        var datable=$('#clinicas-table').DataTable({
            order: ([0, 'DESC']),
            ajax: baseuri+"listar-clinicas/"+rol,
            columns: [
                {data: 'id', name: 'id'},
                {data: 'nombre', name: 'nombre'},
                {data: 'telefono', name: 'telefono'},
                {data: 'action', name: 'action', orderable: true, searchable: true}
            ]
        });

        $("#cp").keyup(function () {
            cp = $(this).val();
            lengthInput = $(this).val().length;
            if (lengthInput < 5) {
                return false;
            }
            direccion(cp);
        });

        $('#btnModal').click(function(event) {
            event.preventDefault();
            $('#altaClinica').trigger('reset');
            $("#altaClinica").attr('action', '{{ route('clinicas.store') }}');  
            $("#altaClinica").attr('method', 'POST');
            $('#modalClinica #modal-title').html('Nuevo Registro');   
            $('#modalClinica').modal('show'); 
        });

        $("#altaClinica").submit(function (e) {
            e.preventDefault();
            $.ajax({
                type: $(this).attr('method'),
                url: $(this).attr('action'),
                data: $("#altaClinica").serialize(),
                dataType: 'json',
                success: function (res) {
                    if (res.success==true) {
                        $(".errors").html('');
                        $('#altaClinica').trigger('reset');
                        datable.ajax.reload();
                        $('#modalClinica').modal('hide');   
                        
                        swal({
                          type: 'success',
                          title: 'Hecho!',
                          text: '¡Registro almacenado correctamente!',
                          buttonsStyling: false,
                          confirmButtonClass: 'btn btn-lg btn-success'
                        });
                    }
                }
                , error: function (e) {
                    pintar_errores(e.responseJSON.errors);
                }
            });
        });

        $("body").on("click", "#clinicas-table #btnMostrar", function (event) {
            event.preventDefault();
            var id = $(this).data('id');
            $.ajax({
                url: baseuri+'clinicas/'+id+'/edit',
                type: 'GET',
                dataType: 'json',
                success:function(res){
                    // console.log(res);
                    if (res.success==true) {
                        $('#modalMostrar').modal('show');
                        $('#s_nombre span').html(res.res.nombre);
                        $('#s_telefono span').html(res.res.telefono);
                        $('#s_telefono_alt span').html(res.res.telefono_alt);
                        if(res.res.codigop){
                            $('#s_direccion span').html(res.res.direccion +', '+ res.res.codigop.id_municipio+', '+ res.res.codigop.id_estado +' CP:'+ res.res.codigop.id_codigo);
                        }else{
                            $('#s_direccion span').html(res.res.direccion);
                        }
                    }
                }  
            });

        });           

        $("body").on("click", "#clinicas-table #btnEditar", function (event) {
            event.preventDefault();
            var id = $(this).data('id');
            $.ajax({
                url: baseuri+'clinicas/'+id+'/edit',
                type: 'GET',
                dataType: 'json',
                success:function(res){
                    if (res.success==true) {
                        $("#altaClinica").attr('action', baseuri+'clinicas/'+id);  
                        $("#altaClinica").attr('method', 'PUT');
                        $('#modalClinica #modal-title').html('Editar Registro');   
                        $('#nombre').val(res.res.nombre);
                        $('#cp').val(res.res.codigop.id_codigo);
                        direccion(res.res.codigop.id_codigo, res.res.codigop.id);                        

                        $('#direccion').val(res.res.direccion);
                        $('#telefono').val(res.res.telefono);
                        $('#telefono_alt').val(res.res.telefono_alt);
                        if(rol == 1){
                            listar_dentistas(res.res.dentista_id);
                        }
                        $('#modalClinica').modal('show'); 
                    }
                }  
            });
        });

        $("body").on("click", "#clinicas-table #btnEliminar", function (event) {
            event.preventDefault();
            var id = $(this).data('id');
            swal({
              // title: '¿Seguro que desea eliminar el registro?',
              text: "¿Seguro que desea eliminar el registro?",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#0CC27E',
              cancelButtonColor: '#FF586B',
              confirmButtonText: 'Si',
              cancelButtonText: 'No',
              confirmButtonClass: 'btn btn-success mr-5',
              cancelButtonClass: 'btn btn-danger',
              buttonsStyling: false
            }).then(function () {
                $.ajax({
                    url: baseuri+'clinicas/'+id,
                    type: 'DELETE',
                    dataType: 'json',
                    success: function (data) {
                        if (data.success == 'true') {                               
                            swal('Eliminado', 'El registro se elimino exitosamente.', 'success');  
                            datable.ajax.reload();
                        }
                        else {                               
                            swal('Error', 'Hubo un error al tratar de eliminar el registro!', 'error');
                            datable.ajax.reload();
                        }
                    }
                });
              
            }, function (dismiss) {
              // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
              if (dismiss === 'cancel') {
                swal('Cancelado', 'No se realizaron cambios', 'error');
              }
            });
        });

        $('#btnCerrarModal').click(function(event) {
            event.preventDefault();
            $('#altaClinica').trigger('reset');
            $(".errors").html('');
            $('#modalClinica').modal('hide'); 
        });
    });

    function listar_clinicas(data = null){
        $('#clinica_id').html('');
        $.ajax({
            url: baseuri+'listar-clinicas',
            type: 'GET',
            dataType: 'JSON',
            success:function(res){
                if (res) {
                    $.each(res, function(index, val) {
                        if (data == val.id) {
                            $('#clinica_id').append('<option selected value="'+val.id+'">'+val.nombre+'</option>');        
                        }else {
                            $('#clinica_id').append('<option value="'+val.id+'">'+val.nombre+'</option>');        
                        }
                        
                    });
                }
            }
        });
    }

    function listar_dentistas(data = null){
        $('#dentista_id').html('');
        $.ajax({
            url: baseuri+'dentistas',
            type: 'GET',
            dataType: 'JSON',
            success:function(res){

                if (res) {
                    $.each(res, function(index, val) {
                        if (data == val.id) {
                            $('#dentista_id').append('<option selected value="'+val.id+'">'+val.user.nombre+' '+val.user.apellidos+' </option>');
                        }else {
                            $('#dentista_id').append('<option  value="'+val.id+'">'+val.user.nombre+' '+val.user.apellidos+' </option>');   
                        }
                         
                    });
                }
            }
        });
    }

    function listar_roles(data = null) {
        $('#rol').html('');
        $.ajax({
            url: baseuri + 'listar-roles',
            type: 'GET',
            dataType: 'json',
            success:function(res){
                $.each(res, function(index, val) {
                    if (data == val.id) {
                        $('#rol').append('<option selected value="'+val.id+'">'+val.name+'</option>');
                    }else {
                        $('#rol').append('<option value="'+val.id+'">'+val.name+'</option>');
                    }
                });
                
            }
        });
    }

    function pintar_errores(errores = null){
        $(".errors").html('');
        $(".errors").parent().removeClass('has-error');
        $.each(errores, function (k, v) {
            $(".error-" + k).html(v);
            $(".error-" + k).parent().addClass('has-error');
        });
    }

    function mostrar_form(rol){
        switch (rol) {
            case '1':
                $('#div_dentista_id').show();
            break;
            case '2':
                $('#div_dentista_id').hide();
            break;
        }
    }

    function direccion(cp, id=null){
        $.ajax({
            type: "get", 
            url: baseuri + "cp/" + cp, 
            dataType: "json", 
            success: function (res) {
                if (res.success==true) {

                    $('#colonias').html('');
                    $("#estado").val(res.info[0].id_estado);
                    $("#municipio").val(res.info[0].id_municipio);
                    
                    if (res.colonia.length > 0) {
                        for (var i = 0; i < res.colonia.length; i++) {
                            
                            if (id == res.colonia[i].id) {
                                $('#colonias').append('<option selected value='+res.colonia[i].id+'>'+res.colonia[i].colonia+'</option>');    
                            }else
                            {
                                $('#colonias').append('<option value='+res.colonia[i].id+'>'+res.colonia[i].colonia+'</option>');
                            }
                            
                        }
                    }
                }else{
                    swal({
                          type: 'error',
                          title: '¡Error!',
                          text: '¡Codigo Postal erróneo!',
                          buttonsStyling: false,
                          confirmButtonClass: 'btn btn-lg btn-success'
                    });
                    $('#colonias').html('');
                    $("#estado").val('');
                    $("#municipio").val('');
                }
              
            }
            , error: function (data) {
                alert("Codigo postal erroneo, por favor verifica los datos.");
            }
        });
    }
</script>
@endsection
