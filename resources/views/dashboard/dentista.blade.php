@extends('layouts.app_dent')
@section('titulo', 'Bienvenido '. Auth::user()->fullName)
@section('contenido')
<div class="row">
    <div class="col-lg-4 col-md-6 col-sm-6">
        <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
            <div class="card-body text-center">
                <a href="{{ route('pacientes.index') }}">
                    <i class="i-Add-User">
                    </i>
                </a>
                <div class="content">
                    <p class="text-muted mt-2 mb-0">
                        Pacientes
                    </p>
                    <p class="text-primary text-24 line-height-1 mb-2">
                        {{count($pacientes)}}
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-6 col-sm-6">
        <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
            <div class="card-body text-center">
                <a href="{{ route('citas.index') }}">
                    <i class="i-Calendar">
                    </i>
                </a>
                <div class="content">
                    <p class="text-muted mt-2 mb-0">
                        Citas
                    </p>
                    <p class="text-primary text-24 line-height-1 mb-2">
                        {{count($citas)}}
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-6 col-sm-6">
        <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
            <div class="card-body text-center">
                <i class="i-Money-2">
                </i>
                <div class="content">
                    <p class="text-muted mt-2 mb-0">
                        Finanzas
                    </p>
                    <p class="text-primary text-24 line-height-1 mb-2">
                        $1200
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="card mb-4">
            <div class="card-body">
                <div class="card-title">
                    This Year Sales
                </div>
                <div id="echartBar" style="height: 300px;">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
