<div aria-hidden="true" aria-labelledby="title_gral_mdl" class="modal fade" id="gral_modal_dnt-app" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title_gral_mdl">
                        
                </h5>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                    <span aria-hidden="true">
                        ×
                    </span>
                </button>
            </div>
            <div class="modal-body" id="body_gral_modal">
                    
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" data-dismiss="modal" type="button">
                    Cancelar
                </button>
                <button class="btn btn-primary ml-2" type="button" id="accept_button_modal">
                    Guardar
                </button>
            </div>
        </div>
    </div>
</div>
