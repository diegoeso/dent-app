@extends('layouts.app_pacient')
@section('titulo', 'Bienvenido '.Auth::user()->fullName)
@section('contenido')
<div class="row">
    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
            <div class="card-body text-center">
                <i class="i-Add-User">
                </i>
                <div class="content">
                    <p class="text-muted mt-2 mb-0">
                        Tratamientos
                    </p>
                    <p class="text-primary text-24 line-height-1 mb-2">
                        10
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
