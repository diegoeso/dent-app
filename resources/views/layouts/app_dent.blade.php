<!DOCTYPE html>
<html dir="" lang="es">
    <head>
        <meta charset="utf-8"/>
        <meta content="width=device-width,initial-scale=1" name="viewport"/>
        <meta content="ie=edge" http-equiv="X-UA-Compatible"/>
        <meta content="{{ csrf_token() }}" name="csrf-token"/>
        <title>
            Dent-App
        </title>
        <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,400i,600,700,800,900" rel="stylesheet"/>
        <link href="{{asset('dist-assets/css/themes/lite-purple.css')}}" rel="stylesheet"/>
        <link href="{{asset('dist-assets/css/plugins/perfect-scrollbar.min.css')}}" rel="stylesheet"/>
        <link href="{{ asset('dist-assets/css/plugins/datatables.min.css') }}" rel="stylesheet"/>
        <link href="{{ asset('dist-assets/css/plugins/fontawesome-free-5.10.1-web/css/all.html') }}" rel="stylesheet"/>
        <link href="{{ asset('dist-assets/css/plugins/sweetalert2.min.css') }}" rel="stylesheet"/>
        {{--
        <link href="{{ asset('plugins/fullcalendar/main.min.css') }}" rel="stylesheet"/>
        --}}
        <link href="{{ asset('dist-assets/css/plugins/calendar/fullcalendar.min.css') }}" rel="stylesheet"/>
        <style type="text/css">
            #loader-dent{
                position: fixed;
                z-index: 11001;
                background-color: #eeeeee8c;
                width: 100%;
                height: 100%;
                justify-content: center;
                align-items: center;
                display: none;
            }
            .container-load {
              position: absolute;
              width: 200px;
              height: 200px;
              top: 0;
              bottom: 0;
              left: 0;
              right: 0;
              margin: auto;
            }

            .item {
              width: 100px;
              height: 100px;
              position: absolute;
            }

            .item-1 {
              background-color: #dac8ff;
              top: 0;
              left: 0;
              z-index: 1;
              animation: item-1_move 1.8s cubic-bezier(.6,.01,.4,1) infinite;
            }

            .item-2 {
              background-color: #b590ff;
              top: 0;
              right: 0;
              animation: item-2_move 1.8s cubic-bezier(.6,.01,.4,1) infinite;
            }

            .item-3 {
              background-color: #6f47bf;
              bottom: 0;
              right: 0;
              z-index: 1;
              animation: item-3_move 1.8s cubic-bezier(.6,.01,.4,1) infinite;
            }

            .item-4 {
              background-color: #eee6ff;
              bottom: 0;
              left: 0;
              animation: item-4_move 1.8s cubic-bezier(.6,.01,.4,1) infinite;
            }

            @keyframes item-1_move {
              0%, 100% {transform: translate(0, 0)} 
              25% {transform: translate(0, 100px)} 
              50% {transform: translate(100px, 100px)} 
              75% {transform: translate(100px, 0)} 
            }

            @keyframes item-2_move {
              0%, 100% {transform: translate(0, 0)} 
              25% {transform: translate(-100px, 0)} 
              50% {transform: translate(-100px, 100px)} 
              75% {transform: translate(0, 100px)} 
            }

            @keyframes item-3_move {
              0%, 100% {transform: translate(0, 0)} 
              25% {transform: translate(0, -100px)} 
              50% {transform: translate(-100px, -100px)} 
              75% {transform: translate(-100px, 0)} 
            }

            @keyframes item-4_move {
              0%, 100% {transform: translate(0, 0)} 
              25% {transform: translate(100px, 0)} 
              50% {transform: translate(100px, -100px)} 
              75% {transform: translate(0, -100px)} 
            }
        </style>
        <link href="{{ asset('plugins/select2/css/select2.min.css') }}" rel="stylesheet"/>
    </head>
    <body class="text-left">
        <div class="app-admin-wrap layout-sidebar-large">
            @include('layouts.header')
            @if(View::exists('menu.'. Auth::user()->roles[0]->slug))
                @include('menu.'.Auth::user()->roles[0]->slug)
            @else
                {{-- @include('menu.admin') --}}
                @include('menu.asistente')
            @endif
            <!-- =============== Left side End ================-->
            <div class="main-content-wrap sidenav-open d-flex flex-column">
                <!-- ============ Body content start ============= -->
                <div class="main-content">
                    <div class="breadcrumb">
                        <h1 class="mr-2">
                            @yield('titulo')
                        </h1>
                    </div>
                    <div class="separator-breadcrumb border-top">
                    </div>
                    @yield('contenido')
                    @include('elements.general_modal')
                    <!--<div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
                            <div class="card-body text-center"><i class="i-Add-User"></i>
                                <div class="content">
                                    <p class="text-muted mt-2 mb-0">New Leads</p>
                                    <p class="text-primary text-24 line-height-1 mb-2">205</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
                            <div class="card-body text-center"><i class="i-Financial"></i>
                                <div class="content">
                                    <p class="text-muted mt-2 mb-0">Sales</p>
                                    <p class="text-primary text-24 line-height-1 mb-2">$4021</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
                            <div class="card-body text-center"><i class="i-Checkout-Basket"></i>
                                <div class="content">
                                    <p class="text-muted mt-2 mb-0">Orders</p>
                                    <p class="text-primary text-24 line-height-1 mb-2">80</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
                            <div class="card-body text-center"><i class="i-Money-2"></i>
                                <div class="content">
                                    <p class="text-muted mt-2 mb-0">Expense</p>
                                    <p class="text-primary text-24 line-height-1 mb-2">$1200</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>-->
                </div>
            </div>
        </div>
        <div id="loader-dent">
            <div class="container-load">
                <div class="item item-1">
                </div>
                <div class="item item-2">
                </div>
                <div class="item item-3">
                </div>
                <div class="item item-4">
                </div>
            </div>
        </div>
        <div class="footer-bottom border-top pt-3 d-flex flex-column flex-sm-row align-items-center">
            <div class="d-flex align-items-center">
                <div>
                    <p class="m-0">
                        © 2018 Gull HTML
                    </p>
                    <p class="m-0">
                        All rights reserved
                    </p>
                </div>
            </div>
        </div>
        <script>
            var baseuri='{!!asset('');!!}';
        </script>
        <!-- fotter end -->
        <!-- ============ Search UI Start ============= -->
        <script src="{{asset('dist-assets/js/plugins/jquery-3.3.1.min.js')}}">
        </script>
        <script src="{{asset('dist-assets/js/plugins/bootstrap.bundle.min.js')}}">
        </script>
        <script src="{{asset('dist-assets/js/plugins/perfect-scrollbar.min.js')}}">
        </script>
        <script src="{{asset('dist-assets/js/scripts/script.min.js')}}">
        </script>
        <script src="{{asset('dist-assets/js/scripts/sidebar.large.script.min.js')}}">
        </script>
        <script src="{{asset('dist-assets/js/scripts/sidebar.script.min.js')}}">
        </script>
        <script src="{{asset('dist-assets/js/plugins/echarts.min.js')}}">
        </script>
        <script src="{{asset('dist-assets/js/scripts/echart.options.min.js')}}">
        </script>
        <script src="{{asset('dist-assets/js/scripts/dashboard.v1.script.min.js')}}">
        </script>
        <script src="{{ asset('dist-assets/js/plugins/datatables.min.js') }}">
        </script>
        <script src="{{ asset('dist-assets/js/scripts/datatables.script.min.js') }}">
        </script>
        <script src="{{ asset('dist-assets/js/plugins/sweetalert2.min.js') }}">
        </script>
        <script src="{{ asset('dist-assets/js/scripts/sweetalert.script.min.js') }}">
        </script>
        <script src="{{ asset('dist-assets/js/plugins/sweetalert2.min.js') }}">
        </script>
        <script src="{{ asset('dist-assets/js/scripts/sweetalert.script.min.js') }}">
        </script>
        <script src="{{ asset('dist-assets/js/plugins/calendar/jquery-ui.min.js') }}">
        </script>
        <script src="{{ asset('plugins/moment/moment.min.js') }}">
        </script>
        <script src="{{ asset('plugins/moment/locale/es.js') }}">
        </script>
        {{--
        <script src="{{ asset('plugins/moment/moment-with-locales.js') }}">
        </script>
        --}}
        {{--
        <script src="{{ asset('dist-assets/js/plugins/calendar/moment.min.js') }}">
        </script>
        <script src="{{ asset('plugins/moment/locales.min.js') }}">
        </script>
        --}}
        <script src="{{ asset('dist-assets/js/plugins/calendar/fullcalendar.min.js') }}">
        </script>
        <script src="{{ asset('plugins/select2/js/select2.min.js') }}">
        </script>
        {{--
        <script src="{{ asset('dist-assets/js/scripts/calendar.script.min.js') }}">
            --}}
        </script>
        <script src="{{ asset('js/system/general_function.js') }}">
        </script>
        {{--
        <script src="{{ asset('plugins/fullcalendar/main.min.js') }}">
        </script>
        <script src="{{ asset('plugins/fullcalendar/locales-all.min.js') }}">
        </script>
        --}}
        <script>
            $.extend( $.fn.dataTable.defaults, {
            language:{
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },

            searching: true,
            processing: true,
            // serverSide: true,
            deferRender: true,
            info: false,
            select: true, 
            responsive: true,
        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        moment.locale('es');
        </script>
        @stack('script_local')
        @yield('scripts')
        <script>
            $('body').on('click', '#btnNotificacion', function(event) {
                event.preventDefault();
                var id = $(this).data('id');
                $.ajax({
                    url: baseuri + 'citas/'+ id,
                    type: 'get',
                    dataType: 'json',
                    success:function(res){
                        $('#exampleModalLabel').html('Información');
                        $('#btnAprobar').html('Aprobar');
                        $('#btnAprobar').attr('type', 'button');
                        $('#fecha').attr('readonly', 'readonly');
                        $('#horario').attr('readonly', 'readonly');
                        $('#paciente').val(res.paciente_r.nombre + ' '+ res.paciente_r.apellidos);
                        $('#fecha').val(res.fecha);
                        $('#horario').val(res.hora_inicio);
                        $('#cita_id').val(res.id);
                        $('#modalNotificacion').modal('show');
                    }
                });
            });

            $('#formNotificacion').submit(function(event) {
                event.preventDefault();
                console.log($('#formNotificacion').serialize());
            });
            $('#btnEditar').click(function(event) {
                event.preventDefault();
                $('#exampleModalLabel').html('Editar cita');
                $('#fecha').removeAttr('readonly');
                $('#horario').removeAttr('readonly');
                $('#btnAprobar').html('Guardar');
                $('#btnAprobar').attr('type', 'submit');
            });

            $('#btnAprobar').click(function(event) {
                event.preventDefault();
                var estatus = $(this).data('id');
                var id = $('#cita_id').val();
                $.ajax({
                    url: '/citas/'+ id ,
                    type: 'PUT',
                    dataType: 'json',
                    data: $('#formNotificacion').serialize(),
                    success:function(res){
                        if (res.success == true) {
                            $('#modalNotificacion').modal('hide');
                            swal({
                                type: 'success',
                                title: 'Hecho!',
                                text: 'Cita aprobada',
                                buttonsStyling: false,
                                confirmButtonClass: 'btn btn-lg btn-success'    
                            });
                            window.location.reload(); 
                        }
                    }
                });
            });
        </script>
    </body>
</html>
