<!DOCTYPE html>
<html dir="" lang="es">
    <head>
        <meta charset="utf-8"/>
        <meta content="width=device-width,initial-scale=1" name="viewport"/>
        <meta content="ie=edge" http-equiv="X-UA-Compatible"/>
        <meta content="{{ csrf_token() }}" name="csrf-token"/>
        <title>
            Paciente | {{Auth::user()->fullName}}
        </title>
        <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,400i,600,700,800,900" rel="stylesheet"/>
        <link href="{{asset('dist-assets/css/themes/lite-purple.min.css')}}" rel="stylesheet"/>
        <link href="{{asset('dist-assets/css/plugins/perfect-scrollbar.min.css')}}" rel="stylesheet"/>
        <link href="{{ asset('dist-assets/css/plugins/datatables.min.css') }}" rel="stylesheet"/>
        <link href="{{ asset('dist-assets/css/plugins/fontawesome-free-5.10.1-web/css/all.html') }}" rel="stylesheet"/>
        <link href="{{ asset('dist-assets/css/plugins/sweetalert2.min.css') }}" rel="stylesheet"/>
        <link href="{{ asset('dist-assets/css/plugins/calendar/fullcalendar.min.css') }}" rel="stylesheet"/>
        <link href="{{ asset('plugins/select2/css/select2.min.css') }}" rel="stylesheet"/>
        <style>
            .ocultar{

            }
        </style>
    </head>
    <body class="text-left">
        <div class="app-admin-wrap layout-sidebar-large" id="app">
            @include('layouts.header_pacient')
            @include('menu.paciente')
            <div class="main-content-wrap sidenav-open d-flex flex-column">
                <div class="main-content">
                    <div class="breadcrumb">
                        <h1 class="mr-2">
                            @yield('titulo')
                        </h1>
                    </div>
                    <div class="separator-breadcrumb border-top">
                    </div>
                    @yield('contenido')
                </div>
            </div>
        </div>
        <div id="loader-dent">
            <div class="container-load">
                <div class="item item-1">
                </div>
                <div class="item item-2">
                </div>
                <div class="item item-3">
                </div>
                <div class="item item-4">
                </div>
            </div>
        </div>
        <div class="footer-bottom border-top pt-3 d-flex flex-column flex-sm-row align-items-center">
            <div class="d-flex align-items-center">
                <div>
                    <p class="m-0">
                        © 2018 Gull HTML
                    </p>
                    <p class="m-0">
                        All rights reserved
                    </p>
                </div>
            </div>
        </div>
        <script>
            var baseuri='{!!asset('');!!}';
        </script>
        <!-- fotter end -->
        <!-- ============ Search UI Start ============= -->
        <script src="{{asset('dist-assets/js/plugins/jquery-3.3.1.min.js')}}">
        </script>
        <script src="{{asset('dist-assets/js/plugins/bootstrap.bundle.min.js')}}">
        </script>
        <script src="{{asset('dist-assets/js/plugins/perfect-scrollbar.min.js')}}">
        </script>
        <script src="{{asset('dist-assets/js/scripts/script.min.js')}}">
        </script>
        <script src="{{asset('dist-assets/js/scripts/sidebar.large.script.min.js')}}">
        </script>
        <script src="{{asset('dist-assets/js/scripts/sidebar.script.min.js')}}">
        </script>
        <script src="{{asset('dist-assets/js/plugins/echarts.min.js')}}">
        </script>
        <script src="{{asset('dist-assets/js/scripts/echart.options.min.js')}}">
        </script>
        <script src="{{asset('dist-assets/js/scripts/dashboard.v1.script.min.js')}}">
        </script>
        <script src="{{ asset('dist-assets/js/plugins/datatables.min.js') }}">
        </script>
        <script src="{{ asset('dist-assets/js/scripts/datatables.script.min.js') }}">
        </script>
        <script src="{{ asset('dist-assets/js/plugins/sweetalert2.min.js') }}">
        </script>
        <script src="{{ asset('dist-assets/js/scripts/sweetalert.script.min.js') }}">
        </script>
        <script src="{{ asset('dist-assets/js/plugins/sweetalert2.min.js') }}">
        </script>
        <script src="{{ asset('dist-assets/js/scripts/sweetalert.script.min.js') }}">
        </script>
        <script src="{{ asset('dist-assets/js/plugins/calendar/jquery-ui.min.js') }}">
        </script>
        <script src="{{ asset('plugins/moment/moment.min.js') }}">
        </script>
        <script src="{{ asset('plugins/moment/locale/es.js') }}">
        </script>
        <script src="{{ asset('dist-assets/js/plugins/calendar/fullcalendar.min.js') }}">
        </script>
        <script src="{{ asset('plugins/select2/js/select2.min.js') }}">
        </script>
        <script src="{{ asset('js/system/general_function.js') }}">
        </script>
        {{--
        <script src="{{ asset('js/app.js') }}">
        </script>
        --}}
        <script>
            $.extend( $.fn.dataTable.defaults, {
            language:{
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },

            searching: true,
            processing: true,
            // serverSide: true,
            deferRender: true,
            info: false,
            select: true,
            responsive: true,
        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        moment.locale('es');
        </script>
        @stack('script_local')
        @yield('scripts')
    </body>
</html>
