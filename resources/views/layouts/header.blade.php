<div class="main-header">
    <div class="logo">
        <img alt="" src="{{asset('dist-assets/images/logo.png')}}">
        </img>
    </div>
    <div class="menu-toggle">
        <div>
        </div>
        <div>
        </div>
        <div>
        </div>
    </div>
    <div class="d-flex align-items-center">
        {{ Auth::user()->fullName }}
    </div>
    <div style="margin: auto">
    </div>
    <div class="header-part-right">
        <i class="i-Full-Screen header-icon d-none d-sm-inline-block" data-fullscreen="">
        </i>
        {{--
        <div class="dropdown">
            <i aria-expanded="false" aria-haspopup="true" class="i-Safe-Box text-muted header-icon" data-toggle="dropdown" id="dropdownMenuButton" role="button">
            </i>
            <div aria-labelledby="dropdownMenuButton" class="dropdown-menu">
                <div class="menu-icon-grid">
                    <a href="#">
                        <i class="i-Shop-4">
                        </i>
                        Home
                    </a>
                    <a href="#">
                        <i class="i-Library">
                        </i>
                        UI Kits
                    </a>
                    <a href="#">
                        <i class="i-Drop">
                        </i>
                        Apps
                    </a>
                    <a href="#">
                        <i class="i-File-Clipboard-File--Text">
                        </i>
                        Forms
                    </a>
                    <a href="#">
                        <i class="i-Checked-User">
                        </i>
                        Sessions
                    </a>
                    <a href="#">
                        <i class="i-Ambulance">
                        </i>
                        Support
                    </a>
                </div>
            </div>
        </div>
        --}}
        <div class="dropdown">
            <div aria-expanded="false" aria-haspopup="true" class="badge-top-container" data-toggle="dropdown" id="dropdownNotification" role="button">
                <span class="badge badge-primary">
                    {{count($notificaciones)}}
                </span>
                <i class="i-Bell text-muted header-icon">
                </i>
            </div>
            <div aria-labelledby="dropdownNotification" class="dropdown-menu dropdown-menu-right notification-dropdown rtl-ps-none ps" data-perfect-scrollbar="" data-suppress-scroll-x="true">
                @foreach ($notificaciones as $el)
                <a data-id="{{$el->id}}" href="#" id="btnNotificacion">
                    <div class="dropdown-item d-flex">
                        <div class="notification-icon">
                            <i class="text-20 i-Calendar-4 text-primary mr-1">
                            </i>
                        </div>
                        <div class="notification-details flex-grow-1">
                            <p class="m-0 d-flex align-items-center">
                                <span>
                                    Nueva Cita
                                </span>
                                <span class="badge badge-pill badge-primary ml-1 mr-1">
                                    Nuevo
                                </span>
                                <span class="flex-grow-1">
                                </span>
                                <span class="text-small text-muted ml-auto">
                                    {{ $el->creado }}
                                </span>
                            </p>
                            <p class="text-small text-muted m-0">
                                Paciente:
                                <strong>
                                    {{$el->paciente_r->fullName}}
                                </strong>
                            </p>
                            <p class="text-small text-muted m-0">
                                Fecha:
                                <strong>
                                    {{$el->cita}}
                                </strong>
                            </p>
                            <p class="text-small text-muted m-0">
                                Horario:
                                <strong>
                                    {{$el->hora_inicio}} a {{$el->hora_fin}}
                                </strong>
                            </p>
                            <p>
                                Pendiente de aprobar
                            </p>
                        </div>
                    </div>
                </a>
                @endforeach
                <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
                    <div class="ps__thumb-x" style="left: 0px; width: 0px;" tabindex="0">
                    </div>
                </div>
                <div class="ps__rail-y" style="top: 0px; right: 0px;">
                    <div class="ps__thumb-y" style="top: 0px; height: 0px;" tabindex="0">
                    </div>
                </div>
            </div>
        </div>
        <div class="dropdown">
            <div class="user col align-self-end">
                <img alt="" aria-expanded="false" aria-haspopup="true" data-toggle="dropdown" id="userDropdown" src="{{asset('dist-assets/images/faces/1.jpg')}}">
                    <div aria-labelledby="userDropdown" class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="">
                            Perfil
                        </a>
                        <a class="dropdown-item" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            {{ __('Salir') }}
                        </a>
                        <form action="{{ route('logout') }}" id="logout-form" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </img>
            </div>
        </div>
    </div>
</div>
<div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="modalNotificacion" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    Información
                </h5>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                    <span aria-hidden="true">
                        ×
                    </span>
                </button>
            </div>
            <form action="" id="formNotificacion" method="post">
                @csrf
                <input id="cita_id" name="cita_id" type="hidden"/>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="">
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label" for="inputEmail3">
                                        Paciente
                                    </label>
                                    <div class="col-sm-8">
                                        <input class="form-control" id="paciente" name="paciente" placeholder="Paciente" readonly="readonly" type="text">
                                        </input>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label" for="inputPassword3">
                                        Fecha
                                    </label>
                                    <div class="col-sm-6">
                                        <input class="form-control" id="fecha" name="fecha" placeholder="Fecha" readonly="readonly" type="date">
                                        </input>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label" for="inputPassword3">
                                        Hora
                                    </label>
                                    <div class="col-sm-6">
                                        <input class="form-control" id="horario" name="horario" placeholder="Horario" readonly="readonly" type="time">
                                        </input>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success mr-auto" id="btnEditar" type="button">
                        Editar
                    </button>
                    <button class="btn btn-secondary" data-dismiss="modal" type="button">
                        Cerrar
                    </button>
                    <button class="btn btn-primary" data-id="1" id="btnAprobar" type="submit">
                        Aprobar
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
