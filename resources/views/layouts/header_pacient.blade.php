<div class="main-header">
    <div class="logo">
        <img alt="" src="{{asset('dist-assets/images/logo.png')}}">
        </img>
    </div>
    <div class="menu-toggle">
        <div>
        </div>
        <div>
        </div>
        <div>
        </div>
    </div>
    <div class="d-flex align-items-center">
        {{ Auth::user()->fullName }}
    </div>
    <div style="margin: auto">
    </div>
    <div class="header-part-right">
        <i class="i-Full-Screen header-icon d-none d-sm-inline-block" data-fullscreen="">
        </i>
        <div class="dropdown">
            <div class="user col align-self-end">
                <img alt="" aria-expanded="false" aria-haspopup="true" data-toggle="dropdown" id="userDropdown" src="{{asset('dist-assets/images/faces/1.jpg')}}">
                    <div aria-labelledby="userDropdown" class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="">
                            Perfil
                        </a>
                        <a class="dropdown-item" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            {{ __('Salir') }}
                        </a>
                        <form action="{{ route('logout') }}" id="logout-form" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </img>
            </div>
        </div>
    </div>
</div>
