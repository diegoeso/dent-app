<div class="side-content-wrap">
    <div class="sidebar-left open rtl-ps-none" data-perfect-scrollbar="" data-suppress-scroll-x="true">
        <ul class="navigation-left">
            <li class="nav-item {{request()->is('home') ? 'active':''}}">
                <a class="nav-item-hold" href="{{route('home')}}">
                    <i class="nav-icon i-Shop-4">
                    </i>
                    <span class="nav-text">
                        Inicio
                    </span>
                </a>
                <div class="triangle">
                </div>
            </li>
        </ul>
    </div>
</div>