<div class="side-content-wrap">
    <div class="sidebar-left open rtl-ps-none" data-perfect-scrollbar="" data-suppress-scroll-x="true">
        <ul class="navigation-left">
            <li class="nav-item {{request()->is('home') ? 'active':''}}">
                <a class="nav-item-hold" href="{{route('home')}}">
                    <i class="nav-icon i-Shop-4">
                    </i>
                    <span class="nav-text">
                        Inicio
                    </span>
                </a>
                <div class="triangle">
                </div>
            </li>
            <li class="nav-item" data-item="users">
                <a class="nav-item-hold" href="#">
                    <i class="nav-icon i-Administrator">
                    </i>
                    <span class="nav-text">
                        Usuarios
                    </span>
                </a>
                <div class="triangle">
                </div>
            </li>
            {{--          @can('paciente.index')
            <li class="nav-item" data-item="">
                <a class="nav-item-hold" href="{{route('pacientes.index')}}">
                    <i class="nav-icon i-MaleFemale">
                    </i>
                    <span class="nav-text">
                        Pacientes
                    </span>
                </a>
                <div class="triangle">
                </div>
            </li>
            @endcan --}}
            @can('cita.index')
            <li class="nav-item">
                <a class="nav-item-hold" href="{{ route('citas.index') }}">
                    <i class="nav-icon i-Over-Time">
                    </i>
                    <span class="nav-text">
                        Citas
                    </span>
                </a>
                <div class="triangle">
                </div>
            </li>
            @endcan
        {{--     @can('asistente.index')
            <li class="nav-item {{request()->is('usuario/asistentes') ? 'active':''}}" data-item="">
                <a class="nav-item-hold" href="{{ route('usuario.index', 'asistentes') }}">
                    <i class="nav-icon i-Female">
                    </i>
                    <span class="nav-text">
                        Asistentes
                    </span>
                </a>
                <div class="triangle">
                </div>
            </li>
            @endcan --}}
            @can('clinica.index')
            <li class="nav-item {{request()->is('clinicas') ? 'active':''}}">
                <a class="nav-item-hold" href="{{ route('clinicas.index')  }}">
                    <i class="nav-icon i-Building">
                    </i>
                    <span class="nav-text">
                        Clinicas
                    </span>
                </a>
                <div class="triangle">
                </div>
            </li>
            @endcan
            {{-- @can('reportes.menu') --}}
            <li class="nav-item" data-item="reportes">
                <a class="nav-item-hold" href="#">
                    <i class="nav-icon i-File-Clipboard-File--Text">
                    </i>
                    <span class="nav-text">
                        Reportes
                    </span>
                </a>
                <div class="triangle">
                </div>
            </li>
            {{-- @endcan --}}
            {{-- @can('rol.index') --}}
            <li class="nav-item {{request()->is('roles') ? 'active':''}}">
                <a class="nav-item-hold" href="{{ route('roles.index')  }}">
                    <i class="nav-icon i-Building">
                    </i>
                    <span class="nav-text">
                        Roles y Permisos
                    </span>
                </a>
                <div class="triangle">
                </div>
            </li>
            {{-- @endcan --}}
            <!-- </li>
                <a class="nav-item-hold" href="#">
                    <i class="nav-icon i-Bar-Chart">
                    </i>
                    <span class="nav-text">
                        Dashboard
                    </span>
                </a>
                <div class="triangle">
                </div>
            </li>
            -->
        </ul>
    </div>
    <div class="sidebar-left-secondary rtl-ps-none" data-perfect-scrollbar="" data-suppress-scroll-x="true">
        <!-- Submenu Dashboards-->
        <ul class="childNav" data-parent="users">
            @can('administrador.index')
            <li class="nav-item">
                <a href="{{ route('usuario.index', 'administradores') }}">
                    <i class="nav-icon i-Bell1">
                    </i>
                    <span class="item-name">
                        Administradores
                    </span>
                </a>
            </li>
            @endcan
            @can('dentista.index')
            <li class="nav-item">
                <a href="{{ route('usuario.index', 'dentistas') }}">
                    <i class="nav-icon i-Split-Horizontal-2-Window">
                    </i>
                    <span class="item-name">
                        Dentistas
                    </span>
                </a>
            </li>
            @endcan
           @can('asistente.index')
            <li class="nav-item">
                <a href="{{ route('usuario.index', 'asistentes') }}">
                    <i class="nav-icon i-Medal-2">
                    </i>
                    <span class="item-name">
                        Asistentes
                    </span>
                </a>
            </li>
            @endcan
        </ul>
        <ul class="childNav" data-parent="reportes">
            <li class="nav-item">
                <a href="dashboard1.html">
                    <i class="nav-icon i-Administrator">
                    </i>
                    <span class="item-name">
                        Clientes
                    </span>
                </a>
            </li>
            <li class="nav-item">
                <a href="dashboard2.html">
                    <i class="nav-icon i-Money-2">
                    </i>
                    <span class="item-name">
                        Ganancias
                    </span>
                </a>
            </li>
            <!-- <li class="nav-item">
                <a href="dashboard3.html">
                    <i class="nav-icon i-Over-Time">
                    </i>
                    <span class="item-name">
                        
                    </span>
                </a>
            </li>
            <li class="nav-item">
                <a href="dashboard4.html">
                    <i class="nav-icon i-Clock">
                    </i>
                    <span class="item-name">
                        Version 4
                    </span>
                </a>
            </li> -->
        </ul>
        <ul class="childNav" data-parent="forms">
            <li class="nav-item">
                <a href="form.basic.html">
                    <i class="nav-icon i-File-Clipboard-Text--Image">
                    </i>
                    <span class="item-name">
                        Basic Elements
                    </span>
                </a>
            </li>
            <li class="nav-item">
                <a href="form.layouts.html">
                    <i class="nav-icon i-Split-Vertical">
                    </i>
                    <span class="item-name">
                        Form Layouts
                    </span>
                </a>
            </li>
            <li class="nav-item">
                <a href="form.input.group.html">
                    <i class="nav-icon i-Receipt-4">
                    </i>
                    <span class="item-name">
                        Input Groups
                    </span>
                </a>
            </li>
            <li class="nav-item">
                <a href="form.validation.html">
                    <i class="nav-icon i-Close-Window">
                    </i>
                    <span class="item-name">
                        Form Validation
                    </span>
                </a>
            </li>
            <li class="nav-item">
                <a href="smart.wizard.html">
                    <i class="nav-icon i-Width-Window">
                    </i>
                    <span class="item-name">
                        Smart Wizard
                    </span>
                </a>
            </li>
            <li class="nav-item">
                <a href="tag.input.html">
                    <i class="nav-icon i-Tag-2">
                    </i>
                    <span class="item-name">
                        Tag Input
                    </span>
                </a>
            </li>
            <li class="nav-item">
                <a href="editor.html">
                    <i class="nav-icon i-Pen-2">
                    </i>
                    <span class="item-name">
                        Rich Editor
                    </span>
                </a>
            </li>
        </ul>
        <ul class="childNav" data-parent="apps">
            <li class="nav-item">
                <a href="invoice.html">
                    <i class="nav-icon i-Add-File">
                    </i>
                    <span class="item-name">
                        Invoice
                    </span>
                </a>
            </li>
            <li class="nav-item">
                <a href="inbox.html">
                    <i class="nav-icon i-Email">
                    </i>
                    <span class="item-name">
                        Inbox
                    </span>
                </a>
            </li>
            <li class="nav-item">
                <a href="chat.html">
                    <i class="nav-icon i-Speach-Bubble-3">
                    </i>
                    <span class="item-name">
                        Chat
                    </span>
                </a>
            </li>
        </ul>
        <ul class="childNav" data-parent="widgets">
            <li class="nav-item">
                <a href="widget-card.html">
                    <i class="nav-icon i-Receipt-4">
                    </i>
                    <span class="item-name">
                        widget card
                    </span>
                </a>
            </li>
            <li class="nav-item">
                <a href="widget-statistics.html">
                    <i class="nav-icon i-Receipt-4">
                    </i>
                    <span class="item-name">
                        widget statistics
                    </span>
                </a>
            </li>
            <li class="nav-item">
                <a href="widget-list.html">
                    <i class="nav-icon i-Receipt-4">
                    </i>
                    <span class="item-name">
                        Widget List
                        <span class="ml-2 badge badge-pill badge-danger">
                            New
                        </span>
                    </span>
                </a>
            </li>
            <li class="nav-item">
                <a href="widget-app.html">
                    <i class="nav-icon i-Receipt-4">
                    </i>
                    <span class="item-name">
                        Widget App
                        <span class="ml-2 badge badge-pill badge-danger">
                            New
                        </span>
                    </span>
                </a>
            </li>
            <li class="nav-item">
                <a href="weather-card.html">
                    <i class="nav-icon i-Receipt-4">
                    </i>
                    <span class="item-name">
                        Weather App
                        <span class="ml-2 badge badge-pill badge-danger">
                            New
                        </span>
                    </span>
                </a>
            </li>
        </ul>
        <ul class="childNav" data-parent="charts">
            <li class="nav-item">
                <a href="charts.echarts.html">
                    <i class="nav-icon i-File-Clipboard-Text--Image">
                    </i>
                    <span class="item-name">
                        echarts
                    </span>
                </a>
            </li>
            <li class="nav-item">
                <a href="charts.chartsjs.html">
                    <i class="nav-icon i-File-Clipboard-Text--Image">
                    </i>
                    <span class="item-name">
                        ChartJs
                    </span>
                </a>
            </li>
            <li class="nav-item dropdown-sidemenu">
                <a href="#">
                    <i class="nav-icon i-File-Clipboard-Text--Image">
                    </i>
                    <span class="item-name">
                        Apex Charts
                    </span>
                    <i class="dd-arrow i-Arrow-Down">
                    </i>
                </a>
                <ul class="submenu">
                    <li>
                        <a href="charts.apexAreaCharts.html">
                            Area Charts
                        </a>
                    </li>
                    <li>
                        <a href="charts.apexBarCharts.html">
                            Bar Charts
                        </a>
                    </li>
                    <li>
                        <a href="charts.apexBubbleCharts.html">
                            Bubble Charts
                        </a>
                    </li>
                    <li>
                        <a href="charts.apexColumnCharts.html">
                            Column Charts
                        </a>
                    </li>
                    <li>
                        <a href="charts.apexCandleStickCharts.html">
                            CandleStick Charts
                        </a>
                    </li>
                    <li>
                        <a href="charts.apexLineCharts.html">
                            Line Charts
                        </a>
                    </li>
                    <li>
                        <a href="charts.apexMixCharts.html">
                            Mix Charts
                        </a>
                    </li>
                    <li>
                        <a href="charts.apexPieDonutCharts.html">
                            PieDonut Charts
                        </a>
                    </li>
                    <li>
                        <a href="charts.apexRadarCharts.html">
                            Radar Charts
                        </a>
                    </li>
                    <li>
                        <a href="charts.apexRadialBarCharts.html">
                            RadialBar Charts
                        </a>
                    </li>
                    <li>
                        <a href="charts.apexScatterCharts.html">
                            Scatter Charts
                        </a>
                    </li>
                    <li>
                        <a href="charts.apexSparklineCharts.html">
                            Sparkline Charts
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
        <ul class="childNav" data-parent="extrakits">
            <li class="nav-item">
                <a href="image.cropper.html">
                    <i class="nav-icon i-Crop-2">
                    </i>
                    <span class="item-name">
                        Image Cropper
                    </span>
                </a>
            </li>
            <li class="nav-item">
                <a href="loaders.html">
                    <i class="nav-icon i-Loading-3">
                    </i>
                    <span class="item-name">
                        Loaders
                    </span>
                </a>
            </li>
            <li class="nav-item">
                <a href="ladda.button.html">
                    <i class="nav-icon i-Loading-2">
                    </i>
                    <span class="item-name">
                        Ladda Buttons
                    </span>
                </a>
            </li>
            <li class="nav-item">
                <a href="toastr.html">
                    <i class="nav-icon i-Bell">
                    </i>
                    <span class="item-name">
                        Toastr
                    </span>
                </a>
            </li>
            <li class="nav-item">
                <a href="sweet.alerts.html">
                    <i class="nav-icon i-Approved-Window">
                    </i>
                    <span class="item-name">
                        Sweet Alerts
                    </span>
                </a>
            </li>
            <li class="nav-item">
                <a href="tour.html">
                    <i class="nav-icon i-Plane">
                    </i>
                    <span class="item-name">
                        User Tour
                    </span>
                </a>
            </li>
            <li class="nav-item">
                <a href="upload.html">
                    <i class="nav-icon i-Data-Upload">
                    </i>
                    <span class="item-name">
                        Upload
                    </span>
                </a>
            </li>
        </ul>
        <ul class="childNav" data-parent="sessions">
            <li class="nav-item">
                <a href="../sessions/signin.html">
                    <i class="nav-icon i-Checked-User">
                    </i>
                    <span class="item-name">
                        Sign in
                    </span>
                </a>
            </li>
            <li class="nav-item">
                <a href="../sessions/signup.html">
                    <i class="nav-icon i-Add-User">
                    </i>
                    <span class="item-name">
                        Sign up
                    </span>
                </a>
            </li>
            <li class="nav-item">
                <a href="../sessions/forgot.html">
                    <i class="nav-icon i-Find-User">
                    </i>
                    <span class="item-name">
                        Forgot
                    </span>
                </a>
            </li>
        </ul>
        <ul class="childNav" data-parent="others">
            <li class="nav-item">
                <a href="../sessions/not-found.html">
                    <i class="nav-icon i-Error-404-Window">
                    </i>
                    <span class="item-name">
                        Not Found
                    </span>
                </a>
            </li>
            <li class="nav-item">
                <a href="user.profile.html">
                    <i class="nav-icon i-Male">
                    </i>
                    <span class="item-name">
                        User Profile
                    </span>
                </a>
            </li>
            <li class="nav-item">
                <a class="open" href="blank.html">
                    <i class="nav-icon i-File-Horizontal">
                    </i>
                    <span class="item-name">
                        Blank Page
                    </span>
                </a>
            </li>
        </ul>
    </div>
    <div class="sidebar-overlay">
    </div>
</div>