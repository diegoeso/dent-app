<div class="side-content-wrap">
    <div class="sidebar-left open rtl-ps-none" data-perfect-scrollbar="" data-suppress-scroll-x="true">
        <ul class="navigation-left">
            <li class="nav-item {{request()->is('home') ? 'active':''}}">
                <a class="nav-item-hold" href="{{route('home')}}">
                    <i class="nav-icon i-Shop-4">
                    </i>
                    <span class="nav-text">
                        Inicio
                    </span>
                </a>
                <div class="triangle">
                </div>
            </li>
            <li class="nav-item" data-item="">
                <a class="nav-item-hold" href="{{route('pacientes.index')}}">
                    <i class="nav-icon i-MaleFemale">
                    </i>
                    <span class="nav-text">
                        Pacientes
                    </span>
                </a>
                <div class="triangle">
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-item-hold" href="{{ route('citas.index') }}">
                    <i class="nav-icon i-Over-Time">
                    </i>
                    <span class="nav-text">
                        Citas
                    </span>
                </a>
                <div class="triangle">
                </div>
            </li>
            <li class="nav-item" data-item="reportes">
                <a class="nav-item-hold" href="#">
                    <i class="nav-icon i-File-Clipboard-File--Text">
                    </i>
                    <span class="nav-text">
                        Reportes
                    </span>
                </a>
                <div class="triangle">
                </div>
            </li>
        </ul>
    </div>
    <div class="sidebar-left-secondary rtl-ps-none" data-perfect-scrollbar="" data-suppress-scroll-x="true">
        <ul class="childNav" data-parent="reportes">
            <li class="nav-item">
                <a href="{{ route('reportes.pacientes') }}">
                    <i class="nav-icon i-Administrator">
                    </i>
                    <span class="item-name">
                        Pacintes
                    </span>
                </a>
            </li>
            <li class="nav-item">
                <a href="">
                    <i class="nav-icon i-Money-2">
                    </i>
                    <span class="item-name">
                        Ganancias
                    </span>
                </a>
            </li>
        </ul>
    </div>
    <div class="sidebar-overlay">
    </div>
</div>