<div class="side-content-wrap">
    <div class="sidebar-left open rtl-ps-none" data-perfect-scrollbar="" data-suppress-scroll-x="true">
        <ul class="navigation-left">
            <li class="nav-item {{request()->is('home') ? 'active':''}}">
                {{--
                <router-link :to="{name: 'home'}" class="nav-item-hold">
                    --}}
                    <a class="nav-item-hold" href="">
                        <i class="nav-icon i-Shop-4">
                        </i>
                        <span class="nav-text">
                            Inicio
                        </span>
                    </a>
                    {{--
                </router-link>
                --}}
                <div class="triangle">
                </div>
            </li>
            <li class="nav-item">
                {{--
                <router-link :to="{name: 'citas'}" class="nav-item-hold">
                    --}}
                    <a class="nav-item-hold" href="{{ route('cita.index') }}">
                        <i class="nav-icon i-Over-Time">
                        </i>
                        <span class="nav-text">
                            Citas
                        </span>
                    </a>
                    {{--
                </router-link>
                --}}
                <div class="triangle">
                </div>
            </li>
        </ul>
    </div>
    <div class="sidebar-overlay">
    </div>
</div>
