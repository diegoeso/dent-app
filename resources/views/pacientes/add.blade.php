<form id="agrega_paciente_frm">
    <input name="_dntst" type="hidden" value="{{$id}}"/>
    <input name="_cita-agndd" type="hidden" value=""/>
    <div class="row">
        <div class="col-md-6 col-lg-4 form-group mb-3">
            <label for="nombre_add">
                Nombre(s)
            </label>
            <input class="form-control" id="nombre_add" name="nombre" onfocusout="function_password()" placeholder="Nombre(s)" type="text">
            </input>
        </div>
        <div class="col-md-6 col-lg-4 form-group mb-3">
            <label for="apellidos_add">
                Apellidos
            </label>
            <input class="form-control" id="apellidos_add" name="apellidos" placeholder="Apellidos" type="text">
            </input>
        </div>
        <div class="col-md-6 col-lg-4 form-group mb-3">
            <label>
                Genero
            </label>
            <label class="radio radio-primary">
                <input name="sexo" type="radio" value="Masculino"/>
                <span>
                    Hombre
                </span>
                <span class="checkmark">
                </span>
            </label>
            <label class="radio radio-primary">
                <input name="sexo" type="radio" value="Femenino"/>
                <span>
                    Mujer
                </span>
                <span class="checkmark">
                </span>
            </label>
        </div>
        <div class="col-md-8 form-group mb-3">
            <label for="direccion_add">
                Dirección
            </label>
            <input class="form-control" id="direccion_add" name="direccion" placeholder="Dirección" type="text">
            </input>
        </div>
        <div class="col-md-4 form-group mb-3">
            <label for="cp_code">
                Código Postal
            </label>
            <input class="form-control" id="cp_code" name="cp" onkeyup="search_cp();" placeholder="C.P." type="number">
            </input>
        </div>
        <div class="col-md-4 form-group mb-3">
            <label for="col_select_cp">
                Colonia
            </label>
            <select class="form-control" id="col_select_cp" name="cp_id">
                <option value="">
                    Ingresa C.P.
                </option>
            </select>
        </div>
        <div class="col-md-4 form-group mb-3">
            <label for="del_select_cp">
                Del./Municipio
            </label>
            <input class="form-control" id="del_select_cp" placeholder="Ingresa C.P." readonly="readonly" type="text">
            </input>
        </div>
        <div class="col-md-4 form-group mb-3">
            <label for="edo_select_cp">
                Estado
            </label>
            <input class="form-control" id="edo_select_cp" placeholder="Ingresa C.P." readonly="readonly" type="text">
            </input>
        </div>
        <div class="col-md-6 form-group mb-3">
            <label for="telefono_add">
                Teléfono
            </label>
            <input class="form-control" id="telefono_add" name="telefono" placeholder="Teléfono">
            </input>
        </div>
        <div class="col-md-6 form-group mb-3">
            <label for="tel2_add">
                Teléfono Alternativo
            </label>
            <input class="form-control" id="tel2_add" name="tel" placeholder="Teléfono alternativo" type="text">
            </input>
        </div>
        <div class="col-md-6 form-group mb-3">
            <label for="email_add">
                Correo electrónico
            </label>
            <input class="form-control" id="email_add" name="email" placeholder="Correo electrónico" type="email">
            </input>
        </div>
        <div class="col-md-6 form-group mb-3">
            <label for="email_add">
                Contraseña
            </label>
            <input class="form-control" id="password" name="password" placeholder="Contraseña" type="text">
            </input>
        </div>
        <div class="col-md-6 form-group mb-3">
            <label for="f_nac">
                Fecha Nacimiento
            </label>
            <input class="form-control" id="f_nac_add" name="f_nac" placeholder="Selecciona fecha" type="date">
            </input>
        </div>
        <div class="col-md-6 form-group mb-3">
            <label for="presion_add">
                Presión
            </label>
            <input class="form-control" id="presion_add" name="presion" placeholder="Presión del paciente" type="text">
            </input>
        </div>
        <div class="col-md-12 form-group mb-3">
            <label class="switch pr-5 switch-primary mr-3">
                <span>
                    Enfermedades
                </span>
                <input name="enfermedades" type="checkbox">
                    <span class="slider">
                    </span>
                </input>
            </label>
        </div>
        <div class="col-md-12 form-group mb-3">
            <label class="switch pr-5 switch-primary mr-3">
                <span>
                    Alergias
                </span>
                <input name="alergias" type="checkbox"/>
                <span class="slider">
                </span>
            </label>
        </div>
        <div class="col-md-6">
            <h5>
                Esta usted bajo tratamiento:
            </h5>
            <div class="col-md-12 form-group mb-3">
                <label for="t_medico">
                    Medico
                </label>
                <input class="form-control" id="t_medico" name="t_medico" placeholder="Indica" type="text" value="No">
                </input>
            </div>
            <div class="col-md-12 form-group mb-3">
                <label for="tuberculosis">
                    ¿Ha padecido tuberculosis?
                </label>
                <input class="form-control" id="tuberculosis" name="tuberculosis" placeholder="Indica" type="text" value="No">
                </input>
            </div>
            <div class="col-md-12 form-group mb-3">
                <label for="penicilina">
                    ¿Es alergico a la penicilina?
                </label>
                <input class="form-control" id="penicilina" name="penicilina" placeholder="Indica" type="text" value="No">
                </input>
            </div>
            <div class="col-md-12 form-group mb-3">
                <label for="a_medicamento">
                    ¿Alergico a otro medicamento?
                </label>
                <input class="form-control" id="a_medicamento" name="a_medicamento" placeholder="Indica" type="text" value="No">
                </input>
            </div>
            <div class="col-md-12 form-group mb-3">
                <label for="cardiovascular">
                    Cardiovascular
                </label>
                <input class="form-control" id="cardiovascular" name="cardiovascular" placeholder="Indica" type="text" value="No">
                </input>
            </div>
        </div>
        <div class="col-md-6">
            <h5>
                Es usted propenso a la:
            </h5>
            <div class="col-md-12 form-group mb-3">
                <label for="hemorragia">
                    Hemorragia
                </label>
                <input class="form-control" id="hemorragia" name="hemorragia" placeholder="Indica" type="text" value="No">
                </input>
            </div>
            <div class="col-md-12 form-group mb-3">
                <label for="diabetes">
                    Diabetes
                </label>
                <input class="form-control" id="diabetes" name="diabetes" placeholder="Indica" type="text" value="No">
                </input>
            </div>
            <div class="col-md-12 form-group mb-3">
                <label for="complicaciones">
                    Ha tenido complicaciones con..
                </label>
                <input class="form-control" id="complicaciones" name="complicaciones" placeholder="Indica" type="text" value="N/A">
                </input>
            </div>
            <div class="col-md-12 form-group mb-3">
                <label for="anestecia_local">
                    La anestecia local
                </label>
                <input class="form-control" id="anestecia_local" name="anestecia_local" placeholder="Indica" type="text" value="No">
                </input>
            </div>
            <div class="col-md-12 form-group mb-3">
                <label for="embarazada">
                    ¿Se encuentra embarazada?
                </label>
                <input class="form-control" id="embarazada" name="embarazada" placeholder="Indica" type="text" value="No">
                </input>
            </div>
            <div class="col-md-12 form-group mb-3">
                <label for="medico_general">
                    Medico Gral del paciente
                </label>
                <input class="form-control" id="medico_general" name="medico_general" placeholder="Indica" type="text" value="N/A">
                </input>
            </div>
            <div class="col-md-12 form-group mb-3">
                <label for="periodo_menstrual">
                    Su periodo menstrual es..
                </label>
                <input class="form-control" id="periodo_menstrual" name="periodo_menstrual" placeholder="Indica" type="text" value="N/A">
                </input>
            </div>
        </div>
        <div class="col-md-12 form-group mb-3">
            <label for="observaciones">
                Observaciones
            </label>
            <textarea class="form-control" name="observaciones" placeholder="Información extra del paciente. Detalla enfermedades, alergias, etc.">
            </textarea>
        </div>
    </div>
</form>
