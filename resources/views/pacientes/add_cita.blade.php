<form id="agrega_cita_paciente">
    <input name="_pcnt" type="hidden" value="{{$id}}">
        <div class="row">
            <div class="col-md-4" id="fecha-Inicio">
                <div class="form-group">
                    <label for="">
                        Fecha
                    </label>
                    <input class="form-control text-capitalize" id="ct_fecha" name="fecha_inicio" placeholder="Fecha de finalización" required="" type="date">
                    </input>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>
                        Hora de inicio
                    </label>
                    <input class="form-control text-capitalize" id="horaInicio" name="hora_inicio" type="time">
                    </input>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="">
                        Hora de finalización
                    </label>
                    <input class="form-control text-capitalize" id="horaFin" name="hora_fin" type="time">
                    </input>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="">
                        Observaciones
                    </label>
                    <textarea class="form-control" cols="30" id="comentarios" name="comentarios" rows="3">
                    </textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="">
                        Estatus
                    </label>
                    <select class="form-control" id="status" name="status">
                        <option value="0">
                            Pendiente
                        </option>
                        <option value="1">
                            En proceso
                        </option>
                        <option value="2">
                            Cancelada
                        </option>
                        <option value="3">
                            Finalizada
                        </option>
                        <option value="4">
                            Re programanda
                        </option>
                    </select>
                    <span class="label label-danger">
                        <strong class="error-status errors">
                        </strong>
                    </span>
                </div>
            </div>
        </div>
    </input>
</form>
