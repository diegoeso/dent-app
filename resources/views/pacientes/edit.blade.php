<form id="edita_paciente_frm">
    <input type="hidden" name="_pcnt" value="{{$paciente->id}}">
    <div class="row">
        <div class="col-md-6 col-lg-4 form-group mb-3">
            <label for="nombre_add">
                Nombre(s)
            </label>
            <input class="form-control" id="nombre_add" name="nombre" placeholder="Nombre(s)" type="text" value="{{$paciente->nombre}}">
            </span>
        </div>
        <div class="col-md-6  col-lg-4 form-group mb-3">
            <label for="apellidos_add">
                Apellidos
            </label>
            <input class="form-control" id="apellidos_add" name="apellidos" placeholder="Apellidos" type="text" value="{{$paciente->apellidos}}">
        </div>
        <div class="col-md-6  col-lg-4 form-group mb-3">
            <label>Genero</label>
            <label class="radio radio-primary">
                <input type="radio" name="sexo" value="Masculino" {{($paciente->sexo == 'Masculino') ? 'checked':''}} /><span>Hombre</span><span class="checkmark"></span>
            </label>
            <label class="radio radio-primary">
                <input type="radio" name="sexo" value="Femenino"  {{($paciente->sexo == 'Femenino') ? 'checked':''}}/><span>Mujer</span><span class="checkmark"></span>
            </label>
        </div>
        <div class="col-md-8 form-group mb-3">
            <label for="direccion_add">
                Dirección
            </label>
            <input class="form-control" id="direccion_add" name="direccion" placeholder="Dirección" type="text" value="{{$paciente->direccion}}">
        </div>
        <div class="col-md-4 form-group mb-3">
            <label for="cp_code">
                Código Postal
            </label>
            <input class="form-control" id="cp_code" name="cp" placeholder="C.P." type="number" onkeyup="search_cp();" value="{{$paciente->codigo_postal->id_codigo}}">
        </div>
        <div class="col-md-4 form-group mb-3">
            <label for="col_select_cp">
                Colonia
            </label>
            <select name="cp_id" id="col_select_cp"  class="form-control">
            <option value="" disabled>Ingresa C.P.</option>
            @foreach($cp as $k => $c_p)
            <option value="{{$c_p->id}}" {{($c_p->id == $paciente->codigo_postal_id) ? 'selected':''}}>{{$c_p->id_asenta}}</option>
            @endforeach
            </select>
        </div>
        <div class="col-md-4 form-group mb-3">
            <label for="del_select_cp">
                Del./Municipio
            </label>
            <input class="form-control" id="del_select_cp" placeholder="Ingresa C.P." type="text" readonly="readonly" value="{{$paciente->codigo_postal->id_municipio}}">
        </div>
        <div class="col-md-4 form-group mb-3">
            <label for="edo_select_cp">
                Estado
            </label>
            <input class="form-control" id="edo_select_cp" placeholder="Ingresa C.P." type="text" readonly="readonly" value="{{$paciente->codigo_postal->id_estado}}">
        </div>
        <div class="col-md-6 form-group mb-3">
            <label for="telefono_add">
                Teléfono
            </label>
            <input class="form-control" id="telefono_add" name="telefono" placeholder="Teléfono" value="{{$paciente->telefono}}">
        </div>
        <div class="col-md-6 form-group mb-3">
            <label for="tel2_add">
                Teléfono Alternativo
            </label>
            <input class="form-control" id="tel2_add" type="text" name="tel" placeholder="Teléfono alternativo" value="{{$paciente->telefono_alt}}">
        </div>
        <div class="col-md-6 form-group mb-3">
            <label for="email_add">
                Correo electrónico
            </label>
            <input class="form-control" id="email_add" type="email" name="email" placeholder="Correo electrónico" value="{{$paciente->email}}">
        </div>
         <div class="col-md-6 form-group mb-3">
            <label for="password">
                Contraseña
            </label>
            <input class="form-control" id="passowrd" type="password" name="password" placeholder="Contraseña">
        </div>
        <div class="col-md-6 form-group mb-3">
            <label for="f_nac">
                Fecha Nacimiento
            </label>
            <input class="form-control" id="f_nac_add" type="date" name="f_nac" placeholder="Selecciona fecha" value="{{$paciente->fecha_nacimiento}}">
        </div>
        <div class="col-md-6 form-group mb-3">
            <label for="presion_add">
                Presión
            </label>
            <input class="form-control" id="presion_add" type="text" name="presion" placeholder="Presión del paciente" value="{{$paciente->presion}}">
        </div>
        <div class="col-md-12 form-group mb-3">
            <label class="switch pr-5 switch-primary mr-3">
                <span>Enfermedades</span>
                <input type="checkbox" name="enfermedades" {{($paciente->enfermedades == 'Si') ? 'checked':''}}><span class="slider"></span>
            </label>
        </div>
        <div class="col-md-12 form-group mb-3">
            <label class="switch pr-5 switch-primary mr-3"><span>Alergias</span>
                <input type="checkbox" name="alergias"  {{($paciente->alergias == 'Si') ? 'checked':''}}/><span class="slider"></span>
            </label>
        </div>

        <div class="col-md-6">
            <h5>Esta usted bajo tratamiento:</h5>
            <div class="col-md-12 form-group mb-3">
                <label for="t_medico">
                    Medico
                </label>
                <input class="form-control" id="t_medico" type="text" name="t_medico" placeholder="Indica" value="{{$paciente->tratamiento_medico}}">
            </div>
            <div class="col-md-12 form-group mb-3">
                <label for="tuberculosis">
                    ¿Ha padecido tuberculosis?
                </label>
                <input class="form-control" id="tuberculosis" type="text" name="tuberculosis" placeholder="Indica" value="{{$paciente->tuberculosis}}">
            </div>
            <div class="col-md-12 form-group mb-3">
                <label for="penicilina">
                    ¿Es alergico a la penicilina?
                </label>
                <input class="form-control" id="penicilina" type="text" name="penicilina" placeholder="Indica" value="{{$paciente->alergia_penicilina}}">
            </div>
            <div class="col-md-12 form-group mb-3">
                <label for="a_medicamento">
                    ¿Alergico a otro medicamento?
                </label>
                <input class="form-control" id="a_medicamento" type="text" name="a_medicamento" placeholder="Indica" value="{{$paciente->alergia_medicamento}}">
            </div>
            <div class="col-md-12 form-group mb-3">
                <label for="cardiovascular">
                    Cardiovascular
                </label>
                <input class="form-control" id="cardiovascular" type="text" name="cardiovascular" placeholder="Indica" value="{{$paciente->cardiovascular}}">
            </div>
        </div>
        <div class="col-md-6">
            <h5>Es usted propenso a la:</h5>
            <div class="col-md-12 form-group mb-3">
                <label for="hemorragia">
                    Hemorragia
                </label>
                <input class="form-control" id="hemorragia" type="text" name="hemorragia" placeholder="Indica" value="{{$paciente->hemorragia}}">
            </div>
            <div class="col-md-12 form-group mb-3">
                <label for="diabetes">
                    Diabetes
                </label>
                <input class="form-control" id="diabetes" type="text" name="diabetes" placeholder="Indica" value="{{$paciente->diabetes}}">
            </div>
            <div class="col-md-12 form-group mb-3">
                <label for="complicaciones">
                    Ha tenido complicaciones con..
                </label>
                <input class="form-control" id="complicaciones" type="text" name="complicaciones" placeholder="Indica" value="{{$paciente->complicaciones}}">
            </div>
            <div class="col-md-12 form-group mb-3">
                <label for="anestecia_local">
                    La anestecia local
                </label>
                <input class="form-control" id="anestecia_local" type="text" name="anestecia_local" placeholder="Indica" value="{{$paciente->anestecia_local}}">
            </div>
            <div class="col-md-12 form-group mb-3">
                <label for="embarazada">
                    ¿Se encuentra embarazada?
                </label>
                <input class="form-control" id="embarazada" type="text" name="embarazada" placeholder="Indica" value="{{$paciente->embarazada}}">
            </div>
            <div class="col-md-12 form-group mb-3">
                <label for="medico_general">
                    Medico Gral del paciente
                </label>
                <input class="form-control" id="medico_general" type="text" name="medico_general" placeholder="Indica" value="{{$paciente->medico_general}}">
            </div>
            <div class="col-md-12 form-group mb-3">
                <label for="periodo_menstrual">
                    Su periodo menstrual es..
                </label>
                <input class="form-control" id="periodo_menstrual" type="text" name="periodo_menstrual" placeholder="Indica" value="{{$paciente->periodo_menstrual}}">
            </div>
        </div>
        <div class="col-md-12 form-group mb-3">
            <label for="observaciones">
                Observaciones
            </label>
            <textarea class="form-control" name="observaciones" placeholder="Información extra del paciente. Detalla enfermedades, alergias, etc.">{{$paciente->observaciones}}</textarea>
        </div>
    </div>
</form>
