@extends('layouts.app_dent')
@section('titulo', 'Pacientes')
@section('contenido')
<div class="row mb-4">
    <div class="col-md-12 mb-4">
        <div class="card text-left">
            <div class="card-header d-flex align-items-center">
                <h3 class="w-50 float-left card-title m-0">
                    Listado de pacientes
                </h3>
                @can('paciente.create')
                <div class="text-right w-50 float-right show">
                    <button class="btn btn-info btn-sm" data-toggle="modal" onclick="get_from_ajax('pacientes/add_view', '{{$id}}')" type="button">
                        <i class="i-Add-User">
                        </i>
                        Nuevo
                    </button>
                </div>
                @endcan
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12 table-responsive">
                        <table aria-describedby="example2_info" class="display table table-striped table-bordered dataTable" id="pacientes-table" role="grid">
                            <thead>
                                <th>
                                    Nombre(s)
                                </th>
                                <th>
                                    Apellidos
                                </th>
                                <th>
                                    Teléfono
                                </th>
                                <th width="80">
                                    Opciones
                                </th>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    //$(document).ready(function() {
        var datable = $('#pacientes-table').DataTable({
            order: ([0, 'DESC']),
            ajax: baseuri+"pacientes-listar/"+"{{$id}}",
            columns: [
                {data: 'nombre', name: 'nombre'},
                {data: 'apellidos', name: 'apellidos'},
                {data: 'telefono', name: 'telefono'},            
                {data: 'action', name: 'action', orderable: true, searchable: true}
            ]
        });
</script>
@endsection
