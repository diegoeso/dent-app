<h4>
    Información Personal
</h4>
<div class="col-12 text-center my-3">
    <button class="btn btn-primary btn-icon m-1 btn-xs" onclick="get_from_ajax('paciente/edit', '{{$paciente->id}}')" type="button">
        <span class="ul-btn__icon">
            <i class="i-Pen-2">
            </i>
        </span>
        <span class="ul-btn__text">
            Modificar
        </span>
    </button>
</div>
<div class="row">
    <div class="col-md-4 col-6">
        <div class="mb-4">
            <p class="text-primary mb-1">
                <i class="i-Calendar text-16 mr-1">
                </i>
                Cumpleaños
            </p>
            <span>
                {{$paciente->fecha_nacimiento}}
            </span>
        </div>
        <div class="mb-4">
            <p class="text-primary mb-1">
                <i class="i-Telephone text-16 mr-1">
                </i>
                Teléfono
            </p>
            <span>
                {{$paciente->telefono}}
            </span>
        </div>
        <div class="mb-4">
            <p class="text-primary mb-1">
                <i class="i-Telephone text-16 mr-1">
                </i>
                Teléfono Alternativo
            </p>
            <span>
                {{($paciente->telefono_alt) ? : 'N/A'}}
            </span>
        </div>
    </div>
    <div class="col-md-4 col-6">
        <div class="mb-4">
            <p class="text-primary mb-1">
                <i class="i-Mail text-16 mr-1">
                </i>
                Correo Electrónico
            </p>
            <span>
                {{($paciente->email) ? : 'N/A'}}
            </span>
        </div>
        <div class="mb-4">
            <p class="text-primary mb-1">
                <i class="i-Map text-16 mr-1">
                </i>
                Dirección
            </p>
            <span>
                {{$paciente->direccion}} {{$paciente->codigo_postal->direccion}}
            </span>
        </div>
        <div class="mb-4">
            <p class="text-primary mb-1">
                <i class="i-MaleFemale text-16 mr-1">
                </i>
                Sexo
            </p>
            <span>
                {{$paciente->sexo}}
            </span>
        </div>
    </div>
    <div class="col-md-4 col-6">
        <div class="mb-4">
            <p class="text-primary mb-1">
                <i class="i-Heart text-16 mr-1">
                </i>
                Alergias
            </p>
            <span>
                {{$paciente->alergias}}
            </span>
        </div>
        <div class="mb-4">
            <p class="text-primary mb-1">
                <i class="i-Heart text-16 mr-1">
                </i>
                Enfermedades
            </p>
            <span>
                {{$paciente->enfermedades}}
            </span>
        </div>
        <div class="mb-4">
            <p class="text-primary mb-1">
                <i class="i-Heart text-16 mr-1">
                </i>
                Presión
            </p>
            <span>
                {{($paciente->presion) ? : 'N/A'}}
            </span>
        </div>
    </div>
</div>
<h5>
    Información Medica
</h5>
<div class="row">
    <div class="col-md-6 col-6">
        <div class="mb-4">
            <p class="text-primary mb-1">
                <i class="i-Heart text-16 mr-1">
                </i>
                Tratamiento Medico
            </p>
            <span>
                {{$paciente->tratamiento_medico}}
            </span>
        </div>
        <div class="mb-4">
            <p class="text-primary mb-1">
                <i class="i-Heart text-16 mr-1">
                </i>
                Tuberculosis
            </p>
            <span>
                {{$paciente->tuberculosis}}
            </span>
        </div>
        <div class="mb-4">
            <p class="text-primary mb-1">
                <i class="i-Heart text-16 mr-1">
                </i>
                Alergia a penicilina
            </p>
            <span>
                {{($paciente->alergia_penicilina) ? : 'N/A'}}
            </span>
        </div>
        <div class="mb-4">
            <p class="text-primary mb-1">
                <i class="i-Heart text-16 mr-1">
                </i>
                Alergia a medicamento
            </p>
            <span>
                {{($paciente->alergia_medicamento) ? : 'N/A'}}
            </span>
        </div>
        <div class="mb-4">
            <p class="text-primary mb-1">
                <i class="i-Heart text-16 mr-1">
                </i>
                Tratamiento cardiovascular
            </p>
            <span>
                {{($paciente->cardiovascular) ? : 'N/A'}}
            </span>
        </div>
    </div>
    <div class="col-md-6 col-6">
        <div class="mb-4">
            <p class="text-primary mb-1">
                <i class="i-Heart text-16 mr-1">
                </i>
                Propenso a Hemorragias
            </p>
            <span>
                {{$paciente->hemorragia}}
            </span>
        </div>
        <div class="mb-4">
            <p class="text-primary mb-1">
                <i class="i-Heart text-16 mr-1">
                </i>
                Diabetes
            </p>
            <span>
                {{$paciente->diabetes}}
            </span>
        </div>
        <div class="mb-4">
            <p class="text-primary mb-1">
                <i class="i-Heart text-16 mr-1">
                </i>
                Complicaciones
            </p>
            <span>
                {{($paciente->complicaciones) ? : 'N/A'}}
            </span>
        </div>
        <div class="mb-4">
            <p class="text-primary mb-1">
                <i class="i-Heart text-16 mr-1">
                </i>
                Anestecia Local
            </p>
            <span>
                {{($paciente->anestecia_local) ? : 'N/A'}}
            </span>
        </div>
        <div class="mb-4">
            <p class="text-primary mb-1">
                <i class="i-Heart text-16 mr-1">
                </i>
                Embarazada
            </p>
            <span>
                {{($paciente->embarazada) ? : 'N/A'}}
            </span>
        </div>
        <div class="mb-4">
            <p class="text-primary mb-1">
                <i class="i-Heart text-16 mr-1">
                </i>
                Medico Gral.
            </p>
            <span>
                {{($paciente->medico_general) ? : 'N/A'}}
            </span>
        </div>
        <div class="mb-4">
            <p class="text-primary mb-1">
                <i class="i-Heart text-16 mr-1">
                </i>
                Periodo Menstrual
            </p>
            <span>
                {{($paciente->periodo_menstrual) ? : 'N/A'}}
            </span>
        </div>
    </div>
</div>
<hr/>
@if($paciente->observaciones)
<h4>
    Observaciones
</h4>
<p>
    {!! nl2br($paciente->observaciones) !!}
</p>
<hr/>
@endif
