@extends('layouts.app_dent')
@section('titulo', 'Paciente')
@section('contenido')
<div class="card user-profile o-hidden mb-4">
    {{--
    <div class="header-cover" style="background-image: url({{asset('/dist-assets/images/sonrisa.jpg')}})">
    </div>
    --}}
    <div class="user-info mt-4">
        <img alt="" class="profile-picture avatar-lg mb-2" src="{{asset('dist-assets/images/faces/user_unknown.png')}}"/>
        <p class="m-0 text-24">
            {{$paciente->nombre}} {{$paciente->apellidos}}
        </p>
        <p class="text-muted m-0">
            Cliente desde {{$paciente->created_at}}
        </p>
    </div>
    <div class="card-body">
        <ul class="nav nav-tabs profile-nav mb-2" id="profileTab" role="tablist">
            <li class="nav-item">
                <a aria-controls="timeline" aria-selected="false" class="nav-link active" data-toggle="tab" href="#timeline" id="timeline-tab" role="tab">
                    Servicios
                </a>
            </li>
            <li class="nav-item">
                <a aria-controls="about" aria-selected="true" class="nav-link" data-toggle="tab" href="#about" id="about-tab" role="tab">
                    Información
                </a>
            </li>
            <li class="nav-item">
                <a aria-controls="friends" aria-selected="false" class="nav-link" data-toggle="tab" href="#friends" id="friends-tab" role="tab">
                    Citas
                </a>
            </li>
            <li class="nav-item">
                <a aria-controls="photos" aria-selected="false" class="nav-link" data-toggle="tab" href="#photos" id="photos-tab" role="tab">
                    Fotografias
                </a>
            </li>
        </ul>
        <div class="tab-content" id="profileTabContent">
            <div aria-labelledby="timeline-tab" class="tab-pane fade active show" id="timeline" role="tabpanel">
                <div class="col-12 text-center">
                    <button class="btn btn-primary btn-xs btn-icon" id="btn-add-trtmnt-pcnt" onclick="get_from_ajax('PacienteTratamiento/add_from_ajax', '{{$paciente->id}}')" type="button">
                        <span class="ul-btn__icon">
                            <i class="i-Add">
                            </i>
                        </span>
                        <span class="ul-btn__text">
                            Nuevo Tratamiento
                        </span>
                    </button>
                </div>
                <div class="inbox-main-sidebar-container mt-3" data-sidebar-container="main" style="padding: 0;">
                    <div class="inbox-main-content" data-sidebar-content="main">
                        <!-- SECONDARY SIDEBAR CONTAINER-->
                        <div class="inbox-secondary-sidebar-container box-shadow-1" data-sidebar-container="secondary">
                            <div data-sidebar-content="secondary">
                                <div class="text-center mt-5" id="load-info-serv" style="display: none;">
                                    <div class="spinner-bubble spinner-bubble-primary m-5">
                                    </div>
                                </div>
                                <div class="inbox-topbar box-shadow-1 perfect-scrollbar rtl-ps-none pl-3" data-suppress-scroll-y="true">
                                    <a class="link-icon mr-3 d-md-none" data-sidebar-toggle="secondary">
                                        <i class="icon-regular mr-1 i-Left-3">
                                        </i>
                                        Tratamientos
                                    </a>
                                </div>
                                <div class="inbox-secondary-sidebar-content position-relative" id="main-tratamientos-lst-client" style="min-height: 500px;">
                                    <h5 class="text-center">
                                        Selecciona el tratamiento a visualizar
                                    </h5>
                                </div>
                            </div>
                            <!-- Secondary Inbox sidebar-->
                            <div class="inbox-secondary-sidebar perfect-scrollbar rtl-ps-none" data-sidebar="secondary" id="div_tratamientos_paciente_list">
                                <i class="sidebar-close i-Close" data-sidebar-toggle="secondary" style="right: 0px;position: relative;">
                                </i>
                                @foreach($paciente->tratamientos as $k => $tratamiento)
                                <div class="mail-item" id="option-trtmnt_{{$tratamiento->id}}" onclick="get_tratamientos_client('{{$tratamiento->id}}');">
                                    <div class="col-xs-6 details">
                                        <span class="name text-muted" id="trtmnt-service_cat_{{$tratamiento->id}}">
                                            {{$tratamiento->tratamiento->categoria->nombre}}
                                        </span>
                                        <p class="m-0" id="trtmnt-service_tr_{{$tratamiento->id}}">
                                            {{$tratamiento->tratamiento->nombre}}
                                        </p>
                                        <p class="m-0" id="trtmnt-service_status_{{$tratamiento->id}}">
                                            {{$tratamiento->estatus}}
                                        </p>
                                    </div>
                                    <div class="col-xs-3 date">
                                        <span class="text-muted">
                                            {{$tratamiento->created_at}}
                                        </span>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <!-- MAIN INBOX SIDEBAR-->
                    <!-- <div class="inbox-main-sidebar" data-sidebar="main" data-sidebar-position="left">
                            <div class="pt-3 pr-3 pb-3"><i class="sidebar-close i-Close" data-sidebar-toggle="main"></i>
                                <button class="btn btn-rounded btn-primary btn-block mb-4">Compose</button>
                                <div class="pl-3">
                                    <p class="text-muted mb-2">Browse</p>
                                    <ul class="inbox-main-nav">
                                        <li><a class="active" href="#"><i class="icon-regular i-Mail-2"></i> Inbox (2)</a></li>
                                        <li><a href="#"><i class="icon-regular i-Mail-Outbox"></i> Sent</a></li>
                                        <li><a href="#"><i class="icon-regular i-Mail-Favorite"></i> Starred</a></li>
                                        <li><a href="#"><i class="icon-regular i-Folder-Trash"></i> Trash</a></li>
                                        <li><a href="#"><i class="icon-regular i-Spam-Mail"></i> Spam</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div> -->
                </div>
            </div>
            <div aria-labelledby="about-tab" class="tab-pane fade" id="about" role="tabpanel">
                @include('pacientes.info_paciente')
            </div>
            <div aria-labelledby="friends-tab" class="tab-pane fade" id="friends" role="tabpanel">
                <h4>
                    Citas Agendadas
                </h4>
                <div class="col-12 text-center my-3">
                    <button class="btn btn-primary btn-xs btn-icon m-1" onclick="get_from_ajax('paciente/add_cita', '{{$paciente->id}}')" type="button">
                        <span class="ul-btn__icon">
                            <i class="i-Add">
                            </i>
                        </span>
                        <span class="ul-btn__text">
                            Nueva Cita
                        </span>
                    </button>
                </div>
                <div class="row">
                    <!-- <div class="col-md-3">
                            <div class="card card-profile-1 mb-4">
                                <div class="card-body text-center">
                                    <div class="avatar box-shadow-2 mb-3"><img src="../../dist-assets/images/faces/16.jpg" alt="" /></div>
                                    <h5 class="m-0">Jassica Hike</h5>
                                    <p class="mt-0">UI/UX Designer</p>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae cumque.</p>
                                    <button class="btn btn-primary btn-rounded">Contact Jassica</button>
                                    <div class="card-socials-simple mt-4"><a href="#"><i class="i-Linkedin-2"></i></a><a href="#"><i class="i-Facebook-2"></i></a><a href="#"><i class="i-Twitter"></i></a></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card card-profile-1 mb-4">
                                <div class="card-body text-center">
                                    <div class="avatar box-shadow-2 mb-3"><img src="../../dist-assets/images/faces/2.jpg" alt="" /></div>
                                    <h5 class="m-0">Frank Powell</h5>
                                    <p class="mt-0">UI/UX Designer</p>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae cumque.</p>
                                    <button class="btn btn-primary btn-rounded">Contact Frank</button>
                                    <div class="card-socials-simple mt-4"><a href="#"><i class="i-Linkedin-2"></i></a><a href="#"><i class="i-Facebook-2"></i></a><a href="#"><i class="i-Twitter"></i></a></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card card-profile-1 mb-4">
                                <div class="card-body text-center">
                                    <div class="avatar box-shadow-2 mb-3"><img src="../../dist-assets/images/faces/3.jpg" alt="" /></div>
                                    <h5 class="m-0">Arthur Mendoza</h5>
                                    <p class="mt-0">UI/UX Designer</p>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae cumque.</p>
                                    <button class="btn btn-primary btn-rounded">Contact Arthur</button>
                                    <div class="card-socials-simple mt-4"><a href="#"><i class="i-Linkedin-2"></i></a><a href="#"><i class="i-Facebook-2"></i></a><a href="#"><i class="i-Twitter"></i></a></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card card-profile-1 mb-4">
                                <div class="card-body text-center">
                                    <div class="avatar box-shadow-2 mb-3"><img src="../../dist-assets/images/faces/4.jpg" alt="" /></div>
                                    <h5 class="m-0">Jacqueline Day</h5>
                                    <p class="mt-0">UI/UX Designer</p>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae cumque.</p>
                                    <button class="btn btn-primary btn-rounded">Contact Jacqueline</button>
                                    <div class="card-socials-simple mt-4"><a href="#"><i class="i-Linkedin-2"></i></a><a href="#"><i class="i-Facebook-2"></i></a><a href="#"><i class="i-Twitter"></i></a></div>
                                </div>
                            </div>
                        </div> -->
                    <div class="col-md-12">
                        <table aria-describedby="example2_info" class="display table table-striped table-bordered dataTable" id="pacientes_cts-table" role="grid">
                            <thead>
                                <th>
                                    Fecha
                                </th>
                                <th>
                                    Hora Inicio
                                </th>
                                <th>
                                    Hora Fin
                                </th>
                                <th>
                                    Estatus
                                </th>
                            </thead>
                            <tbody>
                                @foreach($paciente->citas as $k => $cita)
                                <tr>
                                    <td>
                                        {{$cita->fecha}}
                                    </td>
                                    <td>
                                        {{$cita->hora_inicio}}
                                    </td>
                                    <td>
                                        {{$cita->hora_fin}}
                                    </td>
                                    <td>
                                        {{$cita->status_nombre}}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div aria-labelledby="photos-tab" class="tab-pane fade" id="photos" role="tabpanel">
                <div class="row mb-3">
                    <div class="col-md-8 col-md-offset-2">
                        <button class="btn btn-primary" data-target="#exampleModal" data-toggle="modal" type="button">
                            Cargar
                        </button>
                    </div>
                </div>
                <div class="row">
                    @foreach ($imagenes as $imagen)
                    <div class="col-md-4">
                        <div class="card text-white o-hidden mb-3">
                            <img alt="" class="card-img" src="{{ asset($imagen->url) }}"/>
                            <div class="card-img-overlay">
                                <div class="p-1 text-left card-footer font-weight-light d-flex">
                                    <span class="mr-3 d-flex align-items-center">
                                        {{$imagen->titulo}}
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="exampleModal" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    Cargar Fotografías
                </h5>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                    <span aria-hidden="true">
                        ×
                    </span>
                </button>
            </div>
            <form action="{{ route('images.store') }}" enctype="multipart/form-data" id="upload-image-form" method="post">
                <div class="modal-body">
                    <button class="btn btn-info btn-xs mb-1" id="agregar" type="button">
                        +
                    </button>
                    @csrf
                    <div class="form-group">
                        <input id="paciente_id" name="paciente_id" type="hidden" value="{{$paciente->id}}"/>
                        <table class="table" id="dynamic_field">
                            <tbody>
                                <tr id="row0">
                                    <td>
                                        <input class="form-control" id="titulo" name="titulo[]" placeholder="Titulo" type="text"/>
                                    </td>
                                    <td>
                                        <input class="form-control" id="image-input" name="file[]" type="file"/>
                                    </td>
                                    <td>
                                        {{--
                                        <button class="btn btn-danger btn_remove" id="0" name="remove" type="button">
                                            <span class="fa fa-trash">
                                            </span>
                                            X
                                        </button>
                                        --}}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-success" type="submit">
                            Cargar
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('js/system/pacientes.js') }}">
</script>
<script>
    modal = "{{$active_modal}}";
    if(modal == '1') $("#btn-add-trtmnt-pcnt").click();

    var cont = 1;
    $('#agregar').click(function (event) {
        cont++;
        $('#dynamic_field').append('<tr id="row' + cont + '"><td><input class="form-control" id="titulo" name="titulo[]" placeholder="Titulo" type="text"/></td><td><input class="form-control" id="image-input" name="file[]" type="file"/></td><td><button type="button" name="remove" id="' + cont + '" class="btn btn-danger btn_remove"><span class="fa fa-trash"></span>X</button></td></tr>');

    });

    $(document).on('click', '.btn_remove', function () {
        var button_id = $(this).attr("id");
        $('#row' + button_id + '').remove();
    });


    var img = '{{Session::has('flag')}}';
    
    if(img){
        $('#timeline-tab').removeClass('active');
        $('#photo-tab').addClass('active');

        $('#timeline').removeClass('active show');
        $('#photos').addClass('active show');
        swal({
            type: 'success',
            title: 'Hecho!',
            text: '¡Registro almacenado correctamente!',
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-lg btn-success'
        });
    }else {
        
    }
</script>
@endsection
