@extends('layouts.app_pacient')
@section('titulo', 'Bienvenido ')
<style>
    .div_observaciones{
        display: none;
    }
</style>
@section('contenido')
<div class="row">
    <div class="col-lg-12">
        <div class="card mb-4">
            <div class="card-body p-0">
                <div class="card-title border-bottom d-flex align-items-center m-0 p-3">
                    <span>
                        Citas
                    </span>
                    <span class="flex-grow-1">
                    </span>
                    <button class="btn btn-info btn-sm" onclick="mostrar_modal()">
                        Agregar
                    </button>
                </div>
                {{--
                <table class="table table-hover" id="table_citas">
                    <thead>
                        <td>
                            Fecha
                        </td>
                        <td>
                            Horario
                        </td>
                        <td>
                            Estatus
                        </td>
                        <td>
                            Fecha de creacion
                        </td>
                        <td>
                            Opciones
                        </td>
                    </thead>
                </table>
                --}}
                <div id="contenido">
                </div>
            </div>
        </div>
    </div>
</div>
<div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="modalCita" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    Modal title
                </h5>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                    <span aria-hidden="true">
                        ×
                    </span>
                </button>
            </div>
            <form id="agrega_cita_paciente" method="post">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4" id="fecha-Inicio">
                            <div class="form-group">
                                <label for="">
                                    Fecha
                                </label>
                                <input class="form-control text-capitalize" id="ct_fecha" name="fecha" placeholder="Fecha de finalización" required="" type="date">
                                </input>
                                <span class="text-danger error-fecha errors">
                                </span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>
                                    Hora de inicio
                                </label>
                                <input class="form-control text-capitalize" id="horaInicio" name="hora_inicio" type="time">
                                </input>
                                <span class="text-danger error-hora_inicio errors">
                                </span>
                            </div>
                        </div>
                        {{--
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">
                                    Hora de finalización
                                </label>
                                <input class="form-control text-capitalize" id="horaFin" name="hora_fin" type="time">
                                </input>
                                <span class="text-danger error-hora_fin errors">
                                </span>
                            </div>
                        </div>
                        --}}
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">
                                    Comentarios
                                </label>
                                <textarea class="form-control" cols="30" id="comentarios" name="comentarios" placeholder="Comentarios" rows="3">
                                </textarea>
                                <span class="text-danger error-comentarios errors">
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" data-dismiss="modal" type="button">
                        Cerrar
                    </button>
                    <button class="btn btn-primary" type="submit">
                        Guarcar
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    $(document).ready(function() {
        pintar_citas();
        
        $("body").on("click", "#btnMostrar", function (event) {
            event.preventDefault();
            var id = $(this).attr('value');
            var validar = $(this).attr('data-id');
            if (validar == 'false') {
                $(this).attr('data-id', 'true');
                $(this).html('Ocultar observaciones');
                $(this).removeClass('btn-info');
                $(this).addClass('btn-danger');
                $('#div'+id).css('display','block');
            }else {
                $(this).attr('data-id', 'false');
                $(this).html('Mostrar observaciones');
                $(this).removeClass('btn-danger');
                $(this).addClass('btn-info');
                $('#div'+id).css('display','none');
            }
        });

        $('#agrega_cita_paciente').submit(function(event) {
            event.preventDefault();
            $.ajax({
                url: 'cita',
                type: 'POST',
                dataType: 'json',
                data: $(this).serialize(),
                success:function(res){
                    if (res.success==false) {
                        pintar_errores(res.errors);    
                    }
                    if (res.success == true) {
                        $('#modalCita').modal('hide');
                        $(".errors").html('');
                        $('#altaUsuario').trigger('reset');
                        // datable.ajax.reload();
                        swal({
                          type: 'success',
                          title: 'Hecho!',
                          text: '¡Registro almacenado correctamente!',
                          buttonsStyling: false,
                          confirmButtonClass: 'btn btn-lg btn-success'
                        });
                        pintar_citas();
                    }
                }, error: function (e) {
                    console.log(e.responseJSON.errors);
                    
                }
            });
            
        });

        
        $("body").on("click", "#btnTodo", function (event) {
            event.preventDefault();
            pintar_citas(1)
        });
    });

    function pintar_citas(total = null) {
        if (total) {
            total = 1;
        }else {
            var total = $('#btnTodo').data('id');    
        }
    
        $('#contenido').html('');
        $.ajax({
            url: 'listar-citas-paciente/'+ total,
            type: 'GET',
            dataType: 'JSON',
            success:function(res){
                console.log(res);
                $.each(res.data, function(index, val) {
                    switch (val.status) {
                        case 1:
                        $res = '<span class="badge badge-success">En proceso</span>'
                            break;
                        case 2:
                        $res = '<span class="badge badge-danger">Cancelada</span>'
                            break;
                        case 3:
                        $res = '<span class="badge badge-secondary">Finalizada</span>'
                            break;
                        case 4:
                        $res = '<span class="badge badge-primary">Reprogramada</span>'
                            break;
                        default:
                        $res = '<span class="badge badge-info">Pendiente</span>'
                            break;
                    }
                    if (val.aprobada == 0) {
                        $class = 'alert alert-danger';
                    }else {
                        $class = '';
                    }
                    $('#contenido').append('<div style="" class="d-flex border-bottom justify-content-between p-3 '+$class+'"><div class="flex-grow-1"><span class="text-small text-muted">Fecha</span><h5 class="m-0">'+moment(val.fecha).format('LL')+'</h5></div><div class="flex-grow-1"><span class="text-small text-muted">Hora Inicio</span><h5 class="m-0">'+val.hora_inicio+'</h5></div><div class="flex-grow-1"><span class="text-small text-muted">Hora Fin</span><h5 class="m-0">'+val.hora_fin+'</h5></div><div class="flex-grow-1"><span class="text-small text-muted">Estatus</span><h5 class="m-0"><span class="badge">'+$res+'</span></h5></div><div class="flex-grow-1"><span class="text-small text-muted">Creado</span><h5 class="m-0">'+moment(val.created_at).format('LL')+'</h5></div><div class="flex-grow-1"><button class="btn btn-sm btn-info" data-id="false" id="btnMostrar" value="'+val.id+'">Mostrar observaciones</button></div></div><div class="border-bottom justify-content-between p-3 div_observaciones" id="div'+val.id+'"><div class="flex-grow-1"><span class="text-small text-muted">Observaciones</span><h5 class="m-0">'+val.comentarios+'</h5></div></div>');
                });

                if (res.total > 10) {
                    $('#contenido').append('<div class="text-center mt-3 mb-3"><button class="btn btn-info btn-xs" data-id="0" id="btnTodo">Mostrar todos</button></div>')
                }
                // $('#table_citas').dataTable();
            }
        });
        
    }

    function mostrar_modal() {
        $('#modalCita').modal('show');
    }

    function pintar_errores(errores = null){
        $(".errors").html('');
        $(".errors").parent().removeClass('has-error');
        $.each(errores, function (k, v) {
            $(".error-" + k).html(v);
            $(".error-" + k).parent().addClass('has-error');
        });
    }
</script>
@endsection
