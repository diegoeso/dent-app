@extends('layouts.app_dent')
@section('titulo', 'Pacientes')
@section('contenido')
<div class="row mb-4">
    <div class="col-md-12 mb-4">
        <div class="card text-left">
            <div class="card-header d-flex align-items-center">
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12 table-responsive">
                        <table aria-describedby="example2_info" class="display table table-striped table-bordered dataTable" id="pacientes-table" role="grid">
                            <thead>
                                <th>
                                    Nombre(s)
                                </th>
                                <th>
                                    Apellidos
                                </th>
                                <th>
                                    Teléfono
                                </th>
                                <th width="80">
                                    Opciones
                                </th>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    //$(document).ready(function() {
        var datable = $('#pacientes-table').DataTable({
            order: ([0, 'DESC']),
            ajax: baseuri+"pacientes-listar/",
            columns: [
                {data: 'nombre', name: 'nombre'},
                {data: 'apellidos', name: 'apellidos'},
                {data: 'telefono', name: 'telefono'},            
                {data: 'action', name: 'action', orderable: true, searchable: true}
            ]
        });
</script>
@endsection
