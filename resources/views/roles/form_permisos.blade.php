<div class="row">
    <div class="col-md-6 form-group mb-3 from-group {{ $errors->has('name') ? ' has-error' : '' }}" id="div_nombre">
        <label for="firstName1">
            Nombre
        </label>
        <input class="form-control" id="name" name="name" placeholder="Nombre" type="text">
        </input>
        <span class="text-danger error-name errors">
        </span>
    </div>
    <div class="col-md-6 form-group {{ $errors->has('slug') ? ' has-error' : '' }} mb-3" id="div_slug">
        <label for="phone">
            Slug
        </label>
        <input class="form-control" id="slug" name="slug" placeholder="Slug">
        </input>
        <span class="text-danger error-slug errors">
        </span>
    </div>
    <div class="col-md-12 form-group mb-3 from-group {{ $errors->has('description') ? ' has-error' : '' }}" id="div_nombre">
        <label for="firstName1">
            Descripción
        </label>
        <textarea class="form-control" cols="5" id="description" name="description" placeholder="Descripción" rows="2">
        </textarea>
        <span class="text-danger error-description errors">
        </span>
    </div>
</div>
<div class="row" id="permisos_listar">
</div>