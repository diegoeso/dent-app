@extends('layouts.app_dent')
@section('titulo', 'Roles y Permisos')
@section('contenido')
<div class="row mb-4">
    <div class="col-md-5 mb-4">
        <div class="card text-left">
            <div class="card-header d-flex align-items-center">
                <h3 class="w-50 float-left card-title m-0">
                    Roles
                </h3>
                <div class="col-md-8">
                    <div class="text-right w-50 float-right show">
                        <button class="btn btn-info btn-sm" id="btnModal" type="button">
                            <i class="i-Add">
                            </i>
                            Nuevo
                        </button>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12 table-responsive">
                        <table aria-describedby="example2_info" class="display table table-striped table-bordered dataTable" id="roles-table" role="grid" style="width: 100%">
                            <thead>
                                <tr role="row">
                                    <th>
                                        ID
                                    </th>
                                    <th>
                                        Nombre
                                    </th>
                                    <th>
                                        Slug
                                    </th>
                                    <th>
                                        Especial
                                    </th>
                                    <th width="80">
                                        Opciones
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-7 mb-4">
        <div class="card text-left">
            <div class="card-header d-flex align-items-center">
                <h3 class="w-50 float-left card-title m-0">
                    Permisos
                </h3>
                <div class="col-md-8">
                    <div class="text-right w-50 float-right show">
                        <button class="btn btn-info btn-sm" id="btnModalPermisos" type="button">
                            <i class="i-Add">
                            </i>
                            Nuevo
                        </button>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12 table-responsive">
                        <table aria-describedby="example2_info" class="display table table-striped table-bordered dataTable" id="permisos-table" role="grid" style="width: 100%">
                            <thead>
                                <tr role="row">
                                    <th>
                                        ID
                                    </th>
                                    <th>
                                        Nombre
                                    </th>
                                    <th>
                                        Slug
                                    </th>
                                    <th>
                                        Descripción
                                    </th>
                                    {{--
                                    <th width="80">
                                        Opciones
                                    </th>
                                    --}}
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="modalRoles" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="{{ route('roles.store') }}" id="altaRoles" method="POST" name="altaRoles">
                {{ csrf_field() }}
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-title">
                        Registrar clínica
                    </h5>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                        <span aria-hidden="true">
                            ×
                        </span>
                    </button>
                </div>
                <div class="modal-body">
                    @include('roles.form')
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" id="btnCerrarModal" type="button">
                        Cancelar
                    </button>
                    <button class="btn btn-primary ml-2" id="btnGuardar" type="submit">
                        Guardar
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="modalPermisos" role="dialog" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('roles.permisos') }}" id="altaPermisos" method="POST" name="altaPermisos">
                {{ csrf_field() }}
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-title">
                        Registrar Permiso
                    </h5>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                        <span aria-hidden="true">
                            ×
                        </span>
                    </button>
                </div>
                <div class="modal-body">
                    @include('roles.form_permisos')
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" id="btnCerrarModal" type="button">
                        Cancelar
                    </button>
                    <button class="btn btn-primary ml-2" id="btnGuardar" type="submit">
                        Guardar
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Modal -->
<div aria-hidden="true" aria-labelledby="exampleModalLabel1" class="modal fade" id="modalMostrar" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    Información del registro
                </h5>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                    <span aria-hidden="true">
                        ×
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div aria-labelledby="about-tab" class="tab-pane fade active show" id="about" role="tabpanel">
                    <div class="row">
                        <div class="col-md-4 col-4" id="s_name">
                            <div class="mb-4">
                                <p class="text-primary mb-1">
                                    Rol
                                </p>
                                <span>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-4 col-4" id="s_slug">
                            <div class="mb-4">
                                <p class="text-primary mb-1">
                                    Slug
                                </p>
                                <span>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-4 col-4" id="s_special">
                            <div class="mb-4">
                                <p class="text-primary mb-1">
                                    Permisos especiales
                                </p>
                                <span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-12" id="s_description">
                            <div class="mb-4">
                                <p class="text-primary mb-1">
                                    Descripción
                                </p>
                                <span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-12">
                            <div class="mb-4">
                                <p class="text-primary mb-1">
                                    Permisos
                                </p>
                            </div>
                            <div class="row" id="mostrar_permisos">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-info" data-dismiss="modal" type="button">
                    Cerrar
                </button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        listar_permisos();
        var datableR=$('#roles-table').DataTable({
            order: ([0, 'DESC']),
            ajax: baseuri+"listar-roles-table/",
            columns: [
                {data: 'id', name: 'id'},
                {data: 'name', name: 'name'},
                {data: 'slug', name: 'slug'},
                {data: 'special', name: 'special'},
                {data: 'action', name: 'action', orderable: true, searchable: true}
            ]
        });

        var datableP=$('#permisos-table').DataTable({
            order: ([0, 'DESC']),
            ajax: baseuri+"listar-permisos-table/",
            columns: [
                {data: 'id', name: 'id'},
                {data: 'name', name: 'name'},
                {data: 'slug', name: 'slug'},
                {data: 'description', name: 'description'},
                // {data: 'action', name: 'action', orderable: true, searchable: true}
            ]
        });

        $('#btnModal').click(function(event) {
            event.preventDefault();
            $('#altaRoles').trigger('reset');
            $("#altaRoles").attr('action', '{{ route('roles.store') }}');  
            $("#altaRoles").attr('method', 'POST');
            $('#modalRoles #modal-title').html('Nuevo Registro');   
            $('#modalRoles').modal('show'); 
        });  

        $('#btnModalPermisos').click(function(event) {
            event.preventDefault();
            $('#altaPermisos').trigger('reset');
            $("#altaPermisos").attr('action', '{{ route('roles.permisos') }}');  
            $("#altaPermisos").attr('method', 'POST');
            $('#modalPermisos #modal-title').html('Nuevo Registro');   
            $('#modalPermisos').modal('show'); 
        });

        $("#altaRoles").submit(function (e) {
            e.preventDefault();
            $.ajax({
                type: $(this).attr('method'),
                url: $(this).attr('action'),
                data: $("#altaRoles").serialize(),
                dataType: 'json',
                success: function (res) {
                    console.log(res);
                    if (res.success==true) {
                        $(".errors").html('');
                        $('#altaRoles').trigger('reset');
                        datableR.ajax.reload();
                        $('#modalRoles').modal('hide');   
                        
                        swal({
                          type: 'success',
                          title: 'Hecho!',
                          text: '¡Registro almacenado correctamente!',
                          buttonsStyling: false,
                          confirmButtonClass: 'btn btn-lg btn-success'
                        });
                    }
                }
                , error: function (e) {
                    pintar_errores(e.responseJSON.errors);
                }
            });
        });

        // Permisos
        $("#altaPermisos").submit(function (e) {
            e.preventDefault();
            $.ajax({
                type: $(this).attr('method'),
                url: $(this).attr('action'),
                data: $("#altaPermisos").serialize(),
                dataType: 'json',
                success: function (res) {
                    console.log(res);
                    if (res.success==true) {
                        $(".errors").html('');
                        $('#altaPermisos').trigger('reset');
                        datableP.ajax.reload();
                        $('#modalPermisos').modal('hide');   
                        
                        swal({
                          type: 'success',
                          title: 'Hecho!',
                          text: '¡Registro almacenado correctamente!',
                          buttonsStyling: false,
                          confirmButtonClass: 'btn btn-lg btn-success'
                        });
                    }
                }
                , error: function (e) {
                    pintar_errores(e.responseJSON.errors);
                }
            });
        });


        $('#name').keypress(function(event) {
            $('#slug').val(slug($(this).val()))
        });

        $("body").on("click", "#roles-table #btnMostrar", function (event) {
            event.preventDefault();
            var id = $(this).data('id');
            $('#mostrar_permisos').html('');
            $.ajax({
                url: baseuri+'roles/'+id+'/edit',
                type: 'GET',
                dataType: 'json',
                success:function(res){
                    if (res) {
                        $('#modalMostrar').modal('show');
                        $('#s_name span').html(res.name);
                        $('#s_slug span').html(res.slug);
                        $('#s_special span').html(res.special);
                        $('#s_description span').html(res.description);
                        $.each(res.permissions, function(index, val) {
                            $('#mostrar_permisos').append(' <div class="col-md-6"><label><strong class="text-info"> '+val.name+'</strong><br>'+val.description+'<span class="checkmark"></span></label></div>');                      
                        }); 
                    }
                }  
            });

        });           

        $("body").on("click", "#roles-table #btnEditar", function (event) {
            event.preventDefault();
            var id = $(this).data('id');
            $('#altaRoles').trigger('reset');
            $.ajax({
                url: baseuri+'roles/'+id+'/edit',
                type: 'GET',
                dataType: 'json',
                success:function(res){
                    $("#altaRoles").attr('action', baseuri+'roles/'+id);  
                    $("#altaRoles").attr('method', 'PUT');
                    $('#modalRoles #modal-title').html('Editar Registro');   
                    $('#name').val(res.name);
                    $('#slug').val(res.slug);
                    $('#description').val(res.description);
                    if (res.special == 'all-access') {
                        $('#special2').prop('checked', 'true')
                    }else if(res.special == 'no-access') {
                        $('#special1').prop('checked', 'true')
                    }
                    $.each(res.permissions, function(i, res) {
                        $("#permiso"+res.id).prop('checked', 'true');
                    });                     
                    $('#modalRoles').modal('show'); 
                }  
            });
        });

        $("body").on("click", "#roles-table #btnEliminar", function (event) {
            event.preventDefault();
            var id = $(this).data('id');
            swal({
              // title: '¿Seguro que desea eliminar el registro?',
              text: "¿Seguro que desea eliminar el registro?",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#0CC27E',
              cancelButtonColor: '#FF586B',
              confirmButtonText: 'Si',
              cancelButtonText: 'No',
              confirmButtonClass: 'btn btn-success mr-5',
              cancelButtonClass: 'btn btn-danger',
              buttonsStyling: false
            }).then(function () {
                $.ajax({
                    url: baseuri+'roles/'+id,
                    type: 'DELETE',
                    dataType: 'json',
                    success: function (data) {
                        if (data.success == 'true') {                               
                            swal('Eliminado', 'El registro se elimino exitosamente.', 'success');  
                            datableR.ajax.reload();
                        }
                        else {                               
                            swal('Error', 'Hubo un error al tratar de eliminar el registro!', 'error');
                            datableR.ajax.reload();
                        }
                    }
                });
              
            }, function (dismiss) {
              // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
              if (dismiss === 'cancel') {
                swal('Cancelado', 'No se realizaron cambios', 'error');
              }
            });
        });

        $('#btnCerrarModal').click(function(event) {
            event.preventDefault();
            $('#altaRoles').trigger('reset');
            $(".errors").html('');
            $('#modalRoles').modal('hide'); 
        });
    });

    function slug(str) {
      str = str.replace(/^\s+|\s+$/g, ''); // trim
      str = str.toLowerCase();
      var from = "ãàáäâẽèéëêìíïîõòóöôùúüûñç·/_,:;";
      var to   = "aaaaaeeeeeiiiiooooouuuunc------";
      for (var i=0, l=from.length ; i<l ; i++) {
        str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
      }
      str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
        .replace(/\s+/g, '-') // collapse whitespace and replace by -
        .replace(/-+/g, '-'); // collapse dashes
      return str;
    };

    function listar_permisos(permissions = null) {
        $('#rol').html('');
        $.ajax({
            url: baseuri + 'listar-permisos',
            type: 'GET',
            dataType: 'json',
            success:function(res){
                console.log(res);
                $.each(res, function(index, val) {
                    $('#permisos_listar').append(' <div class="col-md-12"><label><input name="permisos[]" id="permiso'+val.id+'" type="checkbox" value="'+val.slug+'"/><strong class="text-info"> '+val.name+'</strong><br>'+val.description+'<span class="checkmark"></span></label></div>');                      
                });    
            }
        });
    }

    function pintar_errores(errores = null){
        $(".errors").html('');
        $(".errors").parent().removeClass('has-error');
        $.each(errores, function (k, v) {
            $(".error-" + k).html(v);
            $(".error-" + k).parent().addClass('has-error');
        });
    }
</script>
@endsection
