<div class="row">
    <input id="tipo_rol" name="tipo_rol" type="hidden"/>
    <div class="col-md-6 form-group mb-3 from-group {{ $errors->has('nombre') ? ' has-error' : '' }}" id="div_nombre">
        <label for="firstName1">
            Nombre(s)
        </label>
        <input class="form-control" id="nombre" name="nombre" placeholder="Nombre(s)" type="text">
        </input>
        <span class="text-danger error-nombre errors">
        </span>
    </div>
    <div class="col-md-6 form-group mb-3 {{ $errors->has('apellidos') ? ' has-error' : '' }}" id="div_apellidos">
        <label for="lastName1">
            Apellidos
        </label>
        <input class="form-control" id="apellidos" name="apellidos" placeholder="Apellidos" type="text">
        </input>
        <span class="text-danger error-apellidos errors">
        </span>
    </div>
    <div class="col-md-4 form-group {{ $errors->has('telefono') ? ' has-error' : '' }} mb-3" id="div_telefono">
        <label for="phone">
            Teléfono
        </label>
        <input class="form-control" id="telefono" name="telefono" placeholder="Teléfono">
        </input>
        <span class="text-danger error-telefono errors">
        </span>
    </div>
    <div class="col-md-4 form-group {{ $errors->has('telefono') ? ' has-error' : '' }} mb-3" id="div_telefono_alt">
        <label for="phone">
            Teléfono Alternativo
        </label>
        <input class="form-control" id="telefono_alt" name="telefono_alt" placeholder="Teléfono Alternativo">
        </input>
        <span class="text-danger error-telefono_alt errors">
        </span>
    </div>
</div>
<div class="row">
    <div class="col-md-4 form-group {{ $errors->has('email') ? ' has-error' : '' }} mb-3" id="div_email">
        <label for="website">
            Correo Electrónico
        </label>
        <input class="form-control" id="email" name="email" placeholder="Correo Electrónico" type="email">
        </input>
        <span class="text-danger error-email errors">
        </span>
    </div>
    <div class="col-md-4 form-group {{ $errors->has('password') ? ' has-error' : '' }} mb-3" id="div_password">
        <label for="picker2">
            Contraseña
        </label>
        <input class="form-control" id="password" name="password" placeholder="**********" type="password">
        </input>
        <span class="text-danger error-password errors">
        </span>
    </div>
    <div class="col-md-4 form-group {{ $errors->has('email') ? ' has-error' : '' }} mb-3" id="div_username">
        <label for="website">
            Nombre de Usuario
        </label>
        <input class="form-control" id="username" name="username" placeholder="Nombre de Usuario" type="text">
        </input>
        <span class="text-danger error-username errors">
        </span>
    </div>
</div>
<div class="row">
    <div class="col-md-4 form-group {{ $errors->has('direccion') ? ' has-error' : '' }} mb-3" id="div_cp">
        <label for="">
            Codigo Postal
        </label>
        <input class="form-control" id="cp" maxlength="5" name="cp" onkeypress="if(this.value.length==5) return false;" placeholder="Ingresa tu código postal para buscar tu ubicación." type="number" value="{{ old('postal') }}">
            <span class="text-danger error-cp errors">
            </span>
        </input>
    </div>
    <div class="col-md-8 form-group {{ $errors->has('direccion') ? ' has-error' : '' }} mb-3" id="div_direccion">
        <label for="">
            Dirección
        </label>
        <input class="form-control" id="direccion" name="direccion" placeholder="Dirección" type="text">
        </input>
        <span class="text-danger error-direccion errors">
        </span>
    </div>
    <div class="form-group col-md-4 col-sm-8 col-xs-8" id="div_colonia">
        <label class="" for="direccion">
            Colonia:
        </label>
        <div class="input-group{{ $errors->has('codigo_postal_id') ? ' has-error' : '' }}">
            <select class="form-control" id="colonias" name="codigo_postal_id">
                <option value="">
                    Ingresa tu Código Postal..
                </option>
            </select>
        </div>
        <span class="text-danger error-codigo_postal_id errors">
        </span>
    </div>
    <div class="col-md-4" id="div_municipio">
        <label class="" for="pais">
            Municipio / Delegación:
        </label>
        <div class="input-group">
            <input class="form-control" disabled="" id="municipio" name="municipio" placeholder="" type="text">
            </input>
        </div>
    </div>
    <div class="col-md-4" id="div_estado">
        <label class="" for="provincia">
            Estado:
        </label>
        <div class="input-group">
            <input class="form-control" disabled="" id="estado" name="estado" type="text">
            </input>
        </div>
    </div>
    <div class="col-md-6 form-group {{ $errors->has('cedula') ? ' has-error' : '' }} mb-3" id="div_cedula">
        <label for="credit1">
            Cédula
        </label>
        <input class="form-control" id="cedula" name="cedula" placeholder="Cedula Profesional">
        </input>
        <span class="text-danger error-cedula errors">
        </span>
    </div>
    <div class="col-md-6 form-group {{ $errors->has('cedula') ? ' has-error' : '' }} mb-3" id="div_escuela">
        <label for="credit1">
            Escuela
        </label>
        <input class="form-control" id="escuela" name="escuela" placeholder="Escuela">
        </input>
        <span class="text-danger error-escuela errors">
        </span>
    </div>
    @if (Auth::user()->rol == 1)
    <div class="col-md-6 form-group {{ $errors->has('dentista_id') ? ' has-error' : '' }} mb-3" id="div_dentista_id">
        <label for="picker1">
            Dentista
        </label>
        <select class="form-control" id="dentista_id" name="dentista_id">
        </select>
        <span class="text-danger error-dentista_id errors">
        </span>
    </div>
    @endif
    
    {{--
    <div class="col-md-6 form-group {{ $errors->has('clinica_id') ? ' has-error' : '' }} mb-3">
        <label for="picker1">
            Clínica
        </label>
        <select class="form-control" id="clinica_id" name="clinica_id">
        </select>
        <span class="text-danger error-clinica_id errors">
        </span>
    </div>
    --}}
    <div class="col-md-6 form-group {{ $errors->has('rol') ? ' has-error' : '' }} mb-3" id="div_rol">
        <label for="picker1">
            Rol
        </label>
        <select class="form-control" id="rol" name="rol">
        </select>
        <span class="text-danger error-rol errors">
        </span>
    </div>
</div>