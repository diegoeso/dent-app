<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return redirect()->to('/login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('usuario/{rol?}', 'UserController@index')->name('usuario.index');
Route::resource('usuario', 'UserController')->except(['index', 'create']);
Route::get('listar-usuarios/{id?}', 'UserController@listar_usuarios')->name('usuario.listar_usuarios');
Route::get('listar-dentistas/{id?}', 'UserController@listar_dentistas')->name('usuario.listar_dentistas');

Route::resource('clinicas', 'ClinicaController')->except(['create']);
Route::get('listar-clinicas/{id?}', 'ClinicaController@listar_clinicas')->name('clinica.listar_clinicas');

Route::resource('roles', 'RolesController');
Route::get('listar-roles', 'RolesController@listar_roles')->name('roles.listar_roles');
Route::get('listar-permisos', 'RolesController@listar_permisos')->name('roles.listar_permisos');
Route::get('listar-roles-table', 'RolesController@listar_roles_table')->name('roles.listar_roles_table');
Route::get('listar-permisos-table', 'RolesController@listar_permisos_table')->name('roles.listar_permisos_table');
Route::post('permisos', 'RolesController@permisos')->name('roles.permisos');

Route::get('pacientes/add_view', 'PacienteController@add_view');
Route::get('pacientes/{id?}', 'PacienteController@index')->name('pacientes.index');
Route::post('pacientes/add', 'PacienteController@add');
Route::get('pacientes-listar/{id?}', 'PacienteController@listar');
Route::resource('cp', 'CodigoPostalController');
Route::get('Config/search_cp', 'CodigoPostalController@search_cp_2');

Route::resource('dentistas', 'DentistasController');

// Route::get('pacientes/view','PacienteController@view_to_edit');
Route::get('pacientes/view/{id}', 'PacienteController@view')->name('pacientes.view.data');
Route::get('paciente/add_cita', 'PacienteController@add_cita')->name('pacientes.view.add_cita');
Route::get('paciente/edit', 'PacienteController@edit')->name('pacientes.view.edit');
Route::post('pacientes/add_new_cita', 'PacienteController@add_new_cita');
Route::post('pacientes/update', 'PacienteController@update');

Route::get('tratamientos/listar', 'PacienteTratamientoController@listar');
Route::get('Tratamiento/get_all_by_id', 'TratamientoController@get_tratamientos_by_id');

Route::get('PacienteTratamiento/add_from_ajax', 'PacienteTratamientoController@add_from_ajax');
Route::post('PacienteTratamiento/add', 'PacienteTratamientoController@store');
Route::post('PacienteTratamiento/edit', 'PacienteTratamientoController@update');
Route::get('PacienteTratamiento/edit_from_ajax', 'PacienteTratamientoController@edit');
Route::get('PacienteTratamiento/delete', 'PacienteTratamientoController@destroy');

Route::get('HistorialTratamiento/add_from_ajax', 'HistorialTratamientoController@create');
Route::post('HistorialTratamiento/add', 'HistorialTratamientoController@store');
Route::post('HistorialTratamiento/edit', 'HistorialTratamientoController@update');
Route::get('HistorialTratamiento/edit_from_ajax', 'HistorialTratamientoController@edit');
Route::get('HistorialTratamiento/delete', 'HistorialTratamientoController@destroy');

// Citas
Route::resource('citas', 'CitaController');
Route::get('citas-pacientes', 'CitaController@get_pacientes');
Route::get('listar-citas/{dentista?}', 'CitaController@listar_citas')->name('citas.litar_citas');
Route::get('citas-notificacion/{id}', 'CitaController@show_cita')->name('citas.show_cita');

// imagenes
Route::resource('images', 'ImageController');

Route::get('reportes/pacientes/{val?}', 'ReportesController@report_pacient')->name('reportes.pacientes');

// Dashboard Paciente
Route::get('/home-paciente', 'HomePController@index')->name('home_paciente');
Route::get('/data-paciente', 'HomePController@data');
Route::get('get-citas', 'HomePController@get_citas');

Route::resource('cita', 'Paciente\CitaController');
Route::get('listar-citas-paciente/{total}', 'Paciente\CitaController@listar_citas');

// Route::get('/{any?}', function () {
//     return redirect()->route('home_paciente');
// })->where('any', '^(?!api\/)[\/\w\.-]*');
